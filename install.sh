#!/bin/bash
###############################################################################
# file:         install.sh                                                    #  
# description:  compiles and installs the kR^X kernel with the minimal config #  
#               for x86_64.                                                   #  
# author:       Alexander J. Gaidis <alexander_gaidis@alumni.brown.edu>       #
###############################################################################

# for returning to the start directory on error
START_DIR=$(pwd)

# setup error handling
set -eE
trap 'cd $START_DIR; echo -e "\nConfiguration FAILED; see install.log for details"' ERR

KRX_DEPENDENCIES="bc gawk gcc g++ gcc-4.7-plugin-dev libbsd-dev make"

# create log files
touch install.log

echo -n "[1/5] Installing dependencies for kR^X ... "
apt-get update &>> install.log
apt-get install -y $KRX_DEPENDENCIES &>>install.log
echo "done"

echo -n "[2/5] Configuring the kR^X kernel ... "
cd linux-3.19
cp ../config/config-3.19-amd64.krx.min ./.config
echo -e "\n\n0xffffffffc13e1fff" | make oldconfig &>>../install.log
echo "done"

echo -n "[3/5] Compiling the kR^X kernel (this may take a while) ... "
make -j`nproc` &>>../install.log
echo "done"

echo -n "[4/5] Installing the kR^X kernel modules ... "
make modules_install &>>../install.log
echo "done"

echo -n "[5/5] Installing the kR^X kernel ... "
make install &>>../install.log
echo "done"

echo -e "\nInstallation complete! Please reboot the machine."