#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/sort.h>
#include <asm/uaccess.h>

#ifdef	CONFIG_X86_64
#include <asm/desc.h>
#endif

#ifdef	CONFIG_KRX

#ifdef	CONFIG_X86_64
noinline
#endif
int krx_fixup(const struct exception_table_entry *x)
{
	int ret;
	krx_disable();
	ret = x->fixup;
	asm volatile("" : : : "memory");
	krx_enable();
	return ret;
}

#ifdef	CONFIG_X86_64
noinline
#endif
int krx_insn(const struct exception_table_entry *x)
{
	int ret;
	krx_disable();
	ret = x->insn;
	asm volatile("" : : : "memory");
	krx_enable();
	return ret;
}
#else
#define	krx_fixup(x)	x->fixup
#define krx_insn(x)	x->insn
#endif

#if defined(CONFIG_KRX) && defined(CONFIG_X86_64)
static noinline unsigned long
#else
static inline unsigned long
#endif
krx_ex_insn_addr(const struct exception_table_entry *x)
{
	unsigned long ret;
	krx_disable();
	ret = (unsigned long)&x->insn + x->insn;
	asm volatile("" : : : "memory");
	krx_enable();
	return ret;
}

#if defined(CONFIG_KRX) && defined(CONFIG_X86_64)
static noinline unsigned long
#else
static inline unsigned long
#endif
krx_ex_fixup_addr(const struct exception_table_entry *x)
{
	unsigned long ret;
	krx_disable();
	ret = (unsigned long)&x->fixup + x->fixup;
	asm volatile("" : : : "memory");
	krx_enable();
	return ret;
}

int fixup_exception(struct pt_regs *regs)
{
	const struct exception_table_entry *fixup;
	unsigned long new_ip;
	int fixup_val;
	int insn_val;

#ifdef CONFIG_PNPBIOS
	if (unlikely(SEGMENT_IS_PNP_CODE(regs->cs))) {
		extern u32 pnp_bios_fault_eip, pnp_bios_fault_esp;
		extern u32 pnp_bios_is_utter_crap;
		pnp_bios_is_utter_crap = 1;
		printk(KERN_CRIT "PNPBIOS fault.. attempting recovery.\n");
		__asm__ volatile(
			"movl %0, %%esp\n\t"
			"jmp *%1\n\t"
			: : "g" (pnp_bios_fault_esp), "g" (pnp_bios_fault_eip));
		panic("do_trap: can't hit this");
	}
#endif

	fixup = search_exception_tables(regs->ip);
	if (fixup) {
		new_ip = krx_ex_fixup_addr(fixup);
		fixup_val = krx_fixup(fixup);
		insn_val = krx_insn(fixup);

		if (fixup_val - insn_val >= 0x2ffffff0 - 4) {
			/* Special hack for uaccess_err */
			current_thread_info()->uaccess_err = 1;
			new_ip -= 0x2ffffff0;
		}
		regs->ip = new_ip;
		return 1;
	}

	return 0;
}

/* Restricted version used during very early boot */
int __init early_fixup_exception(unsigned long *ip)
{
	const struct exception_table_entry *fixup;
	unsigned long new_ip;

	fixup = search_exception_tables(*ip);
	if (fixup) {
		new_ip = krx_ex_fixup_addr(fixup);

		if (fixup->fixup - fixup->insn >= 0x2ffffff0 - 4) {
			/* uaccess handling not supported during early boot */
			return 0;
		}

		*ip = new_ip;
		return 1;
	}

	return 0;
}

/*
 * Search one exception table for an entry corresponding to the
 * given instruction address, and return the address of the entry,
 * or NULL if none is found.
 * We use a binary search, and thus we assume that the table is
 * already sorted.
 */
const struct exception_table_entry *
search_extable(const struct exception_table_entry *first,
	       const struct exception_table_entry *last,
	       unsigned long value)
{
	while (first <= last) {
		const struct exception_table_entry *mid;
		unsigned long addr;

		mid = ((last - first) >> 1) + first;
		addr = krx_ex_insn_addr(mid);
		if (addr < value)
			first = mid + 1;
		else if (addr > value)
			last = mid - 1;
		else
			return mid;
        }
        return NULL;
}

/*
 * The exception table needs to be sorted so that the binary
 * search that we use to find entries in it works properly.
 * This is used both for the kernel exception table and for
 * the exception tables of modules that get loaded.
 *
 */
static int cmp_ex(const void *a, const void *b)
{
	const struct exception_table_entry *x = a, *y = b;

	/*
	 * This value will always end up fittin in an int, because on
	 * both i386 and x86-64 the kernel symbol-reachable address
	 * space is < 2 GiB.
	 *
	 * This compare is only valid after normalization.
	 */
	return krx_insn(x) - krx_insn(y);
}

void sort_extable(struct exception_table_entry *start,
		  struct exception_table_entry *finish)
{
	struct exception_table_entry *p;
	int i;

	/* Convert all entries to being relative to the start of the section */
	i = 0;
	for (p = start; p < finish; p++) {
		krx_disable();
		p->insn += i;
		krx_enable();
		i += 4;
		krx_disable();
		p->fixup += i;
		krx_enable();
		i += 4;
	}

	krx_sort(start, finish - start, sizeof(struct exception_table_entry),
	     cmp_ex, NULL);

	/* Denormalize all entries */
	i = 0;
	for (p = start; p < finish; p++) {
		krx_disable();
		p->insn -= i;
		krx_enable();
		i += 4;
		krx_disable();
		p->fixup -= i;
		krx_enable();
		i += 4;
	}
}

#ifdef CONFIG_MODULES
static noinline void __trim_init_extable(struct module *m)
{
	/*trim the end*/
	while (m->num_exentries &&
	       within_module_init(krx_ex_insn_addr(&m->extable[m->num_exentries-1]), m))
		m->num_exentries--;
}
/*
 * If the exception table is sorted, any referring to the module init
 * will be at the beginning or the end.
 */
void trim_init_extable(struct module *m)
{
	/*trim the beginning*/
	while (m->num_exentries &&
	       within_module_init(krx_ex_insn_addr(&m->extable[0]), m)) {
		m->extable++;
		m->num_exentries--;
	}
	__trim_init_extable(m);

}
#endif /* CONFIG_MODULES */
