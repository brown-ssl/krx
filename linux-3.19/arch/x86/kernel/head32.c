/*
 *  linux/arch/i386/kernel/head32.c -- prepare to run common code
 *
 *  Copyright (C) 2000 Andrea Arcangeli <andrea@suse.de> SuSE
 *  Copyright (C) 2007 Eric Biederman <ebiederm@xmission.com>
 */

#include <linux/init.h>
#include <linux/start_kernel.h>
#include <linux/mm.h>
#include <linux/memblock.h>

#include <asm/setup.h>
#include <asm/sections.h>
#include <asm/e820.h>
#include <asm/page.h>
#include <asm/apic.h>
#include <asm/io_apic.h>
#include <asm/bios_ebda.h>
#include <asm/tlbflush.h>
#include <asm/bootparam_utils.h>

static void __init i386_default_early_setup(void)
{
	/* Initialize 32bit specific setup functions */
	x86_init.resources.reserve_resources = i386_reserve_resources;
	x86_init.mpparse.setup_ioapic_ids = setup_ioapic_ids_from_mpc;

	reserve_ebda_region();
}

#ifdef	CONFIG_KRX
#include <asm/desc.h>
static inline void __init update_user_ds(void)
{
	/* update the master (per-CPU) copy of 'gdt_page' */
	struct desc_struct user_ds = GDT_ENTRY_INIT(0xc0f2, 0,
			((unsigned long)__krx_edata - 1) >> PAGE_SHIFT);
	gdt_page.gdt[GDT_ENTRY_DEFAULT_USER_DS] = user_ds;
}
#else
static inline void update_user_ds(void) { }
#endif

asmlinkage __visible void __init i386_start_kernel(void)
{
	sanitize_boot_params(&boot_params);

	/* update the USER_DS entry */
	update_user_ds();

	/* Call the subarch specific early setup function */
	switch (boot_params.hdr.hardware_subarch) {
	case X86_SUBARCH_INTEL_MID:
		x86_intel_mid_early_setup();
		break;
	case X86_SUBARCH_CE4100:
		x86_ce4100_early_setup();
		break;
	default:
		i386_default_early_setup();
		break;
	}

	start_kernel();
}
