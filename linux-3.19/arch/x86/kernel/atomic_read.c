#include <linux/memory.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/sort.h>
#include <linux/err.h>
#include <linux/static_key.h>
#include <linux/jump_label_ratelimit.h>
#ifdef	CONFIG_X86_64
#include <asm/desc.h>
#endif

noinline int krx_atomic_read(const atomic_t *v)
{
	int counter;
	krx_disable();
	counter = ACCESS_ONCE((v)->counter);
	krx_enable();
	return counter;
}
