#include <asm/io.h>

#if defined(CONFIG_KRX) && defined(CONFIG_X86_32)
#define BUILDIOS(bwl, bw, type)						\
void outs##bwl(int port, const void *addr, unsigned long count)		\
{									\
	asm volatile("rep; outs" #bwl					\
		     : "+S"(addr), "+c"(count) : "d"(port));		\
}									\
EXPORT_SYMBOL(outs##bwl);						\
void ins##bwl(int port, void *addr, unsigned long count)		\
{									\
	asm volatile("rep; ins" #bwl					\
		     : "+D"(addr), "+c"(count) : "d"(port));		\
}									\
EXPORT_SYMBOL(ins##bwl);

BUILDIOS(b, b, char)
BUILDIOS(w, w, short)
BUILDIOS(l, , int)
#endif
