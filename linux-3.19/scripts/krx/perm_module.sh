#!/bin/bash

# the fisher-yates shuffling algorithm
function fisher_yates {
	local array=( "$@" )
	if (( "${#array[@]}" == 1 )); then
		echo "${array[@]}"
	else
		for (( i=${#array[@]}-1; i > 0; i--));
		do
			local j=$(( RANDOM % $i ))
			local tmp="${array[$i]}"
			array[$i]=${array[$j]}
			array[$j]=$tmp
		done
		echo "${array[@]}"
	fi
}

# iterate arguments to find the linker script and the object file
for var in "$@"
do
	if [[ "$var" == *.lds ]];
	then
		old_lds="$var"
	elif [[ "$var" == *.o && "$var" != *.mod.o ]];
	then
		obj="$var"
	fi
done

# get the path
obj_path=${obj%/*}
# get the filename
obj_name=${obj##*/}
# create the lds filename
lds_name=${obj_name/.o/.lds}
# create the whole lds path
new_lds="$obj_path/.$lds_name"

# get the module functions
funcs=( $($OBJDUMP -h $obj | $AWK '{print $2}' | $GREP -E "^\.text\.") )
# get the number of functions
funcs_size=${#funcs[@]}
# get the module xkeys
xkeys=( $($OBJDUMP -h $obj | $AWK '{print $2}' | $GREP -E "^\.xkeys\.") )
# get the number of keys
xkeys_size=${#xkeys[@]}

# permute the functions
rand_funcs=( $(fisher_yates "${funcs[@]}") )
# permute the keys
rand_xkeys=( $(fisher_yates "${xkeys[@]}") )

# read the linker script
IFS=$'\r\n' GLOBIGNORE='*':;
lds_lines=( $($AWK '{print $0}' $old_lds) )
# compute its length
lds_size=${#lds_lines[@]}

IFS=$' \t\n' GLOBIGNORE='*':;
# clear any residues from previous compilations
echo -n "" > $new_lds
# iterate the lines
for ((i = 0; i < $lds_size; i++))
do
	# log the file
	echo ${lds_lines[$i]} >> $new_lds
	if [[ ${lds_lines[$i]} == */DISCARD/* ]];
	then
		echo "" >> $new_lds
	fi
	# place the `.text' and `.xkeys' section
	if [[ ${lds_lines[$i]} == "SECTIONS {" ]];
	then
		# format `.text'
		echo -n "	.text		: { " >> $new_lds
		# iterate funcs
		for ((j = 0; j < $funcs_size; j++))
		do
			# add function
			echo -n "*("${rand_funcs[$j]//[[:space:]$'\n']}") " >> \
									$new_lds
		done
		# done
		echo "}" >> $new_lds

		# format `.xkeys'
		echo -n "	.xkeys		: { " >> $new_lds
		# iterate xkeys
		for ((j = 0; j < $xkeys_size; j++))
		do
			# add key
			echo -n "*("${rand_xkeys[$j]//[[:space:]$'\n']}") " >> \
									$new_lds
		done
		# done
		echo "}" >> $new_lds
	fi
done

# replace the linker script
ld_flags=${@/"$old_lds"/"$new_lds"}
# finally, link the module
$LD $ld_flags

#clean up
rm $new_lds
