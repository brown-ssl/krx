#!/bin/sh

EDATA_MARK="krx_edata"
EDATA_CONFIG_STR="CONFIG_KRX_EDATA"
AWK_PROG='{ print "0x"$1 }'

RDIR=$(dirname $0)/../..

VMLINUX=$RDIR/vmlinux
RCALL=$RDIR/rcall

OBJ_VAL=$($OBJDUMP -t $VMLINUX | $GREP $EDATA_MARK | $AWK "$AWK_PROG")
OBJ_VAL=$(printf "0x%x\n" $((OBJ_VAL-1)))

# success
[ $OBJ_VAL == $1 ] && exit 0

# fail
echo "kR^X: The value of $EDATA_CONFIG_STR is invalid ($1)."
echo "      Set $EDATA_CONFIG_STR to $OBJ_VAL and run 'make' again."
exit 1
