#include "cb_perm.h"

std::map<const char*, basic_block> entry_bbs;
unsigned long cb_mode = BB_GRAN;
unsigned int verbose = 0;

#define		ENTROPY_LIMIT		1

/* assert GPL compatibility */
int __attribute__ ((visibility("default"))) plugin_is_GPL_compatible; 

/* plugin information structure */
struct plugin_info __attribute__ ((visibility("default")))
pinfo = {
	.version	= VER,
	.help		= NULL
};

static bool
gate_krx(void)
{
	return true;
}

/* descriptor for the new code block permutatio pass */
struct rtl_opt_pass __attribute__ ((visibility("default")))
pass_code_block_perm = {{ 
	RTL_PASS,
	"code_block_perm",
	gate_krx,
	cb_perm_instrument,
	NULL,
	NULL,
	0,
	TV_REORDER_BLOCKS,
	0, 0, 0, 0, 0
}};

/* descriptor for the new xor encryption pass */
struct rtl_opt_pass __attribute__ ((visibility("default")))
pass_xor = {{ 
	RTL_PASS,
	"xor_encrypt",
	gate_krx,
	xor_instrument,
	NULL,
	NULL,
	0,
	TV_NONE,
	PROP_rtl,
	0, 0, 0, 0
}};

#if	linux && __i386__
struct rtl_opt_pass __attribute__ ((visibility("default")))
pass_clobber = {{ 
	RTL_PASS,
	"pass_clobber",
	gate_krx,
	clobber_esi,
	NULL,
	NULL,
	0,
	TV_REORDER_BLOCKS,
	0, 0, 0, 0, 0
}};
#endif

/* plugin versioning structure */
static struct plugin_gcc_version
pver = {
	.basever	= "4.7.0",
	.datestamp	= "",
	.devphase	= "",
	.revision	= "",
	.configuration_arguments = ""
};

/* 
 * Fisher-Yates/Knuth shuffle algorithm
 * https://en.wikipedia.org/wiki/Fisher–Yates_shuffle
 */

#define WARN_STR	"kR^X: Function `%s' has too few code blocks. Entropy will be low.\n"
#define FUNC_NAME	IDENTIFIER_POINTER(DECL_NAME(current_function_decl))
void permute_bbs(std::vector<basic_block> *bbs)
{
	unsigned i, j;
	basic_block tmp;

	if (bbs->size() <= ENTROPY_LIMIT) {
		/* warn */
		if (verbose)
			fprintf(stderr, WARN_STR, FUNC_NAME);
		return;
	}

	for (i = bbs->size() -1; i > 0; i--) {
		j = arc4random_uniform(i);
		tmp = bbs->at(j);
		bbs->at(j) = bbs->at(i);
		bbs->at(i) = tmp;
	}
}

/* 
 * Fisher-Yates/Knuth shuffle algorithm
 * https://en.wikipedia.org/wiki/Fisher–Yates_shuffle
 */
static void __permute_chunks(std::vector<std::vector<basic_block> *> *chunks)
{
	unsigned i, j;
	std::vector<basic_block> *tmp;

	for (i = chunks->size() - 1; i > 0; i--) {
		j = arc4random_uniform(i);
		tmp = chunks->at(j);
		chunks->at(j) = chunks->at(i);
		chunks->at(i) = tmp;
	}
}

/* 
 * Permutes the basic blocks passed in the vector argument
 * at a code chunk granularity. This means that we
 * permute chunks that end with `call' instructions
 *
 * bbs:	the basic blocks of the function
 * 	After this function returns, they are permuted
 */
static void permute_chunks(std::vector<basic_block> *bbs)
{
	std::vector<std::vector<basic_block> *> chunks;	/* holds the chunks,
							   both before and after
							   permutation */
	unsigned i, j, k;				/* iterators */
	std::vector<basic_block> *chunk;		/* holds a single code
							   chunk */

	i = 0;
	/* create the chunks */
	while (i < bbs->size()) {
		chunk = new std::vector<basic_block>();
		chunk->clear();
		chunk->push_back(bbs->at(i));
		/* 
		 * keep adding basic blocks to the chunk till we reach the end
		 * of the function or a basic block that ends with `call' 
		 */
		while (i + 1 < bbs->size() && !CALL_P(BB_END(bbs->at(i)))) {
			i++;
			chunk->push_back(bbs->at(i));
		}
		/* finished a chunk, add it to chunks and move the the next */
		chunks.push_back(chunk);
		i++;
	}
	
	/* warn if the entropy is too low */
	if (chunks.size() <= ENTROPY_LIMIT) {
		/* warn */
		if (verbose)
			fprintf(stderr, WARN_STR, FUNC_NAME);
		return;
	}
	/* permute the chunks */
	__permute_chunks(&chunks);

	/* put bbs in this order */
	k = 0;			/* iterator of bbs */
	for (i = 0; i < chunks.size(); i++) {
		/* extract current chunk */
		chunk = chunks[i];
		/* set the bbs in the right order */
		for (j = 0; j < chunk->size(); j++)
			bbs->at(k++) = chunk->at(j);
		/* clean-up */
		delete chunk;
	}
}

/*
 * helper function:
 * dispatches the argument to function
 * that permutes it with the appropriate
 * granularity (cb_mode).
 *
 * bbs:	the basic blocks of the function
 * 	After this function returns, they are permuted
 */
static void permute(std::vector<basic_block> *bbs)
{
	if (cb_mode != BB_GRAN)
		/* code chunks granularity */
		permute_chunks(bbs);
	else
		/* basic block granularity */
		permute_bbs(bbs);
}

/*
 * Identifies `call' instructions and splits the basic block right after it
 * (i.e. ends the basic block of the `call').
 */
static void split_bbs(void)
{
	basic_block bb_it;	/* basic block iterator */
	rtx insn_it;		/* instruction iterator */
	basic_block from;	/* the original basic block up to the `call'  */
	basic_block to;		/* the rest of the basic block (after `call') */

	/* iterate the basic blocks */
	FOR_EACH_BB(bb_it) {
		/* iterate the instructions inside the basic block */
		FOR_BB_INSNS(bb_it, insn_it) {
			/* 
			 * we only care about `call' instructions that
			 * are not in the end of the basic block
			 */
			if (!CALL_P(insn_it) ||	BB_END(bb_it) == insn_it)
				continue;
			/* split the block after insn_it */
			from = split_block(bb_it, insn_it)->src;
			to = single_succ(from);

			/* 
			 * bookkeeping; ensure that the new basic block is correctly
			 * connected with its predecessor
			 */
			redirect_edge_and_branch_force(single_succ_edge(from),
									to);
			if (forwarder_block_p(from))
				from->flags |= BB_FORWARDER_BLOCK;
			else
				from->flags &= ~BB_FORWARDER_BLOCK;
		}
	}
}

/*
 * Permutes the code blocks to create an unpredictable
 * function layout. Depending on the value of cb_mode
 * we either permute basic blocks or chunks of code
 * that end with `call' instructions.
 *
 * Notes:
 * 1) GCC by default does not consider `call' instructions
 * to be the end of its basic block. We do this manually.
 * 2) When we permute the code blocks at this point, GCC
 * connects them (automagically) with `jmp' instructions.
 * The only exception is the entry block. We handle this
 * with the `entry_jmp' pass.
 *
 */
static void randomize_cbs(void)
{
	basic_block bb;
	basic_block entry_bb;
	basic_block prev_bb;
	basic_block tmp_bb;
	unsigned i;
	std::vector<basic_block> bbs;
	const char *name;
	rtx insn;

	/* 
	 * if it's an empty function, warn for lack
	 * of entropy and return
	 */
	if (n_basic_blocks == NUM_FIXED_BLOCKS) {
		/* warn */
		if (verbose)
			fprintf(stderr, WARN_STR, FUNC_NAME);
		return;
	}

	/* initialize data structures for CFG layout changes */
	cfg_layout_initialize(CLEANUP_EXPENSIVE);
	/* split the basic blocks after each `call' instruction */
	split_bbs();
	/* extract the first basic block (used by the 'entry_jmp' pass) */
	entry_bb = ENTRY_BLOCK_PTR->next_bb;
	/* place all basic blocks in the temporary vector */
	FOR_EACH_BB(bb)
		bbs.push_back(bb);
	/* permute the basic blocks */
	permute(&bbs);
	/* reorder the actual basic blocks according to permutation */
	prev_bb = ENTRY_BLOCK_PTR;
	for (i = 0; i < bbs.size(); i++) {
		/* get current basic block */
		bb = bbs[i];
		/* connect it to the previous basic block */
		bb->prev_bb = prev_bb;
		/* fix the previous basic block */
		prev_bb->next_bb = bb;
		/* update the previous basic block */
		prev_bb = bb;
	}
	/* make sure the last basic block points to the EXIT_BLOCK_PTR */
	prev_bb->next_bb = EXIT_BLOCK_PTR;
	EXIT_BLOCK_PTR->prev_bb = prev_bb;

	/* 
	 * update (free then initialize) the data structures to maintain
	 * between blocks and its copies
	 */
	free_original_copy_tables();
	initialize_original_copy_tables();
	/* compact arrays */
	compact_blocks();

	/* update the 'aux' member of all basic blocks */
	FOR_EACH_BB(bb) {
		if (bb->next_bb != EXIT_BLOCK_PTR)
			bb->aux = bb->next_bb;
	}
	/* finalize the changes */
	cfg_layout_finalize();

	/* get the function name */	
	name = IDENTIFIER_POINTER(DECL_NAME(current_function_decl));
	/* make sure we can find it in the 'entry_jmp' pass */
	if (entry_bb != bbs[0])
		entry_bbs[name] = entry_bb;
}

/* 
 * code block permutation pass
 * (callback; invoked for every translation unit) 
 *
 * returns: SUCC on success, FAIL on error
 */
unsigned int __attribute__ ((visibility("default")))
cb_perm_instrument(void)
{
	randomize_cbs();
	/* return with success */
	return SUCC;
}

/*
 * argument parsing
 *
 * Parse the plugin arguments and set the corresponding
 * variables.
 *
 * plugin_info:	information regarding the plugin; provided by GCC
 *
 * returns: SUCC on success, FAIL on error
 */
static int
parse_args(const struct plugin_name_args *plugin_info)
{
	/* iterator */
	int i;

	/* argument length */
	size_t len;

	/* parse the plugin arguments (if any) */
	for (i = 0; i < plugin_info->argc; i++) {

		/* where is my getopt-like API? */
		
		/* get the length of the argument */
		len = strlen(plugin_info->argv[i].key);

		/* code block permutation mode */
		if (strncmp(plugin_info->argv[i].key,
					CBMODE_STR,
					strlen(CBMODE_STR)) == 0 &&
				len == strlen(CBMODE_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* parse the decimal option */
				cb_mode = strtoul(plugin_info->argv[i].value,
						NULL,
						BASE10);
			else {
				/* missing cb_mode */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					CBMODE_STR,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
		/* code block permutation verbosity */
		else if (strncmp(plugin_info->argv[i].key,
					VERBOSE_STR,
					strlen(VERBOSE_STR)) == 0 &&
				len == strlen(VERBOSE_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* parse the decimal option */
				verbose = strtoul(plugin_info->argv[i].value,
						NULL,
						BASE10);
			else {
				/* missing verbose */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					CBMODE_STR,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
		/* default handler */
		else {
			/* invalid argument */
			(void)fprintf(stderr,
				"%s: invalid argument %s\n",
				NAME,
				plugin_info->argv[i].key);

			/* fail */
			return FAIL;
		}
	}

	/* success */
	return SUCC;
}

/*
 * version checking
 *
 * Parse and compare two GCC versions.
 *
 * NOTE:
 * 	- The two versions are assumed to be in
 * 	the form: "MAJOR.MINOR.PATCHLEVEL", and
 * 	{MAJOR, MINOR, PATCHLEVEL} < 10.
 * 	- We invoke `strtol()' without checking
 * 	`errno' for `ERANGE'.
 *
 * lhs_ver:	the first version to compare
 * rhs_ver:	the second version to compare
 *
 * returns:	0 if the two versions are the same,
 * 		>0 if lhs_ver > rhs_ver, or <0 otherwise
 */
static int
version_check(const struct plugin_gcc_version *lhs_ver,
		const struct plugin_gcc_version *rhs_ver)
{
	/* iterator */
	const char *it	= NULL;
	
	/* base multiplier */
	size_t mult	= 1;

	/* parsed versions; integers */
	size_t lhsver	= 0;
	size_t rhsver	= 0;

	/* parse the lhs_ver version */

	/* "it" points to the end of the version string */
	it = lhs_ver->basever + strlen(lhs_ver->basever) -
		((strlen(lhs_ver->basever) > 0) ? 1 : 0);

	/* right-to-left traversal */
	while(lhs_ver->basever != it--) {
		/* found delimiter */
		if (*it == VER_DELIM) {
			/* extract the subversion */
			lhsver	+= (mult * strtol(it + 1, NULL, BASE10));
			/* update the multiplier (i.e., base) */
			mult	*= BASE10;
		}
	}
	/* extract the remainder */
	lhsver += (mult * strtol(lhs_ver->basever, NULL, BASE10));

	/* fix (e.g., 4.6 needs to be 460 and not 46) */
	while (unlikely((lhsver < BASE100) && (lhsver > 0))) lhsver *= BASE10;
	while (unlikely(lhsver > (10 * BASE100) - 1)) lhsver /= BASE10;
	
	/* parse the rhs_ver version */

	/* reset the multiplier */
	mult = 1;

	/* same as lhs_ver */
	it = rhs_ver->basever + strlen(rhs_ver->basever) -
		((strlen(rhs_ver->basever) > 0) ? 1 : 0);
	while(rhs_ver->basever != it--) {
		if (*it == VER_DELIM) {
			rhsver	+= (mult * strtol(it + 1, NULL, BASE10));
			mult	*= BASE10;
		}
	}
	rhsver += (mult * strtol(rhs_ver->basever, NULL, BASE10));
	
	while (unlikely((rhsver < BASE100) && (rhsver > 0))) rhsver *= BASE10;
	while (unlikely(rhsver > (10 * BASE100) - 1)) rhsver *= BASE10;
	
	/* return the difference of lhs_ver and rhs_ver */
	return lhsver - rhsver;	
}

/* 
 * plugin initialization (kaslr)
 *
 * Invoked right after the plugin is loaded. It does some version
 * checking and registers the RTL passes that perform
 * code instrumentation (return address protection and code diversification).
 *
 * This plugin adds two passes.
 * One performs code block permutation right 
 * after GCC has finished relocating the function basic blocks.
 * The other XOR-encrypts the return addresses using a random xkey.
 * It is invoked after most
 * of the low-level optimizations have been applied, since we do
 * not want to instrument code that will be modified or removed
 * due to subsequent optimization passes.
 *
 * NOTE: For i386 systems we add an additional pass, which clobbers
 * %esi in functions that contain inline asm which explicitly make use
 * of the %esi register. See xor.cpp for more details.
 *
 * plugin_info: information regarding the plugin; provided by GCC
 * version:     version information; provided by GCC
 *
 * returns:     SUCC on success, FAIL on error
 */
int __attribute__ ((visibility("default")))
plugin_init(struct plugin_name_args *plugin_info,
		struct plugin_gcc_version *version)
{
#if	linux && __i386__
	struct register_pass_info clobber;
#endif
	struct register_pass_info xor_rets;
	struct register_pass_info code_block_perm;

        /* version checking; the actual GCC version vs. required GCC */
        if (unlikely(version_check(version, &pver) < 0)) {
                /* incompatible GCC version */
                (void)fprintf(stderr,
                        "%s: incompatible GCC version: %s (required >= %s)\n",
                        NAME,
                        version->basever,
                        pver.basever);

                /* failed */
                goto err;
        }

        /* argument parsing */
        if (unlikely(parse_args(plugin_info) == FAIL))
                /* failed */
                goto err;

	/* provide version and help information to GCC */
	register_callback(plugin_info->base_name, PLUGIN_INFO, NULL, &pinfo);

	/* register the code block permutation pass; std boilerplate */
	code_block_perm.pass 			 = &pass_code_block_perm.pass;
	code_block_perm.ref_pass_instance_number = 0;
	code_block_perm.pos_op			 = PASS_POS_INSERT_AFTER;

	code_block_perm.reference_pass_name	 = "sched2";
	code_block_perm.pass->next		 = pass_sched2.pass.next;

	/* provide the pass information to the pass manager */
	register_callback(plugin_info->base_name,
				PLUGIN_PASS_MANAGER_SETUP,
				NULL,
				&code_block_perm);
#if	linux && __i386__
	clobber.pass 			= &pass_clobber.pass;
	clobber.ref_pass_instance_number	= 0;
	clobber.pos_op			= PASS_POS_INSERT_AFTER;

	/* chain the new pass after "vartrack"; why?... good question */
	clobber.reference_pass_name		= "expand";
	clobber.pass->next			=
				pass_expand.pass.next;
	
	/* provide the pass information to the pass manager */
	register_callback(plugin_info->base_name,
				PLUGIN_PASS_MANAGER_SETUP,
				NULL,
				&clobber);
#endif
	/* register the new pass; std boilerplate */
	xor_rets.pass 				= &pass_xor.pass;
	xor_rets.ref_pass_instance_number	= 0;
	xor_rets.pos_op			= PASS_POS_INSERT_AFTER;

#if	linux && __amd64__
	/* chain the new pass after "vartrack"; why?... good question */
	xor_rets.reference_pass_name		= "vartrack";
	xor_rets.pass->next			= 
					pass_variable_tracking.pass.next;
#elif	linux && __i386__
	/* chain the new pass after "vartrack"; why?... good question */
	xor_rets.reference_pass_name		= "vartrack";
	xor_rets.pass->next			= 
					pass_variable_tracking.pass.next;
#endif
	
	/* provide the pass information to the pass manager */
	register_callback(plugin_info->base_name,
				PLUGIN_PASS_MANAGER_SETUP,
				NULL,
				&xor_rets);

	/* exit with success */
	return SUCC;

err:
	/* exit with failure */
	return FAIL;
}
