#include "cb_perm.h"
#include <bsd/stdlib.h>
#include <string>
#include <strings.h>

#define INSN_ITER(insn_it)	basic_block bb_it; 			\
				FOR_EACH_BB(bb_it)			\
					FOR_BB_INSNS(bb_it, insn_it)
#define SET_P(insn)		(GET_CODE(insn) == SET)
#define POST_INC_P(insn)	(GET_CODE(insn) == POST_INC)
#define PRE_DEC_P(insn)		(GET_CODE(insn) == PRE_DEC)
#define PARALLEL_P(insn)	(GET_CODE(insn) == PARALLEL)
#define PLUS_P(insn)		(GET_CODE(insn) == PLUS)

#if	linux && __amd64__
#define RES_REG			R10_REG
#define MODE			DImode
#define	RET_OFFSET		(-8)
#elif	linux && __i386__
#define	RES_REG			SI_REG
#define	MODE			SImode
#define	RET_OFFSET		(-4)
#endif

/*
 * returns the instruction that follows the insn argument
 *
 * insn: the instruction of which we want to find the next instruction
 *
 * returns: the next instruction (could be NULL if insn is the last
 * 	    instruction in the code stream)
 */
rtx get_next_insn(rtx insn)
{
	rtx tmp_insn;	/* the following instruction */
	/* get the next RTX in the function */
	tmp_insn = NEXT_INSN(insn);
	/* keep getting the next RTX until we find either an instruction
	 * or we reach the end of the code stream */
	while (tmp_insn != NULL && !INSN_P(tmp_insn))
		tmp_insn = NEXT_INSN(tmp_insn);
	/* return the next instruction */
	return tmp_insn;
}

#if	linux && __i386__
/*
 * Examines if the inline asm statement @expr contains @target_str.
 *
 * expr: 		the expression to be tested
 * target_str: 		the string literal that we want to search in expr
 * test_inputs:		if 1, then examine the inputs of the asm. If they
 * 			contain %esi then return FAIL. Otherwise return SUCC
 * 			iff expr contains target str
 *
 * returns:		SUCC if true, FAIL otherwise
 */ 
long contains_asm(rtx expr, const char *target_str, unsigned long test_inputs)
{
	const char *fmt;	/* the instruction format 		     */
	unsigned long i;	/* Iterator of PARALLEL RTL expressions      */
	const char *str;	/* the string literal of the asm instruction */
	rtvec inputs;		/* the inputs of the asm instruction 	     */
	const char *input;	/* one of the inputs in the inputs rtvec     */
	short mentioned;	/* flag that shows if we encountered %esi 
				   in the inputs			     */

	/* we only care about inline asm */
	if (GET_CODE(expr) == ASM_OPERANDS) {
		/* extract string literal */
		str = XSTR(expr, 0);
		/* does it contain the target str? */
		if (strstr(str, target_str)) {
			/* no need to test more */
			if (!test_inputs)
				/* done */
				return SUCC;
			/* extract the inputs */
			inputs = XVEC(expr, 4);
			/* initialization */
			mentioned = 0;
			/* examine all inputs */
			for (i = 0; i < inputs->num_elem; i++) {
				input = XSTR(inputs->elem[i], 0);
				/* is the input %esi? */
				if (!strcmp("S", input)) {
					/* yes... */
					return FAIL;
				}
			}
			/* couldn't find it in the inputs*/
			return SUCC;
		}
		/* not found */
		return FAIL;
	}
	/* not inline asm. Recursively check the rest. */
	/* extract format */
	fmt = GET_RTX_FORMAT(GET_CODE(expr));
	/* iterate all sub-expressions*/
	for (i = 0; i < GET_RTX_LENGTH(GET_CODE(expr)); i++) {
		/* we only care about expressions */
		if (fmt[i] != 'e')
			continue;
		/* recursively check all sub-expressions */
		if (contains_asm(XEXP(expr, i), target_str, test_inputs)
								== SUCC)
			/* done */
			return SUCC;
	}
	/* didn't find it */
	return FAIL;
}

/*
 * Checks if a parallel (asm) instruction sets (modifies) %esi.
 *
 * If the instruction sets %esi then we shouldn't spill it.
 *
 * expr: The expression to be examined.
 *
 * returns SUCC if it does, FAIL if not
 */
int sets_esi(rtx expr)
{
	unsigned long i;	/* Iterator of PARALLEL RTL expressions   */
	rtx sub_expr;		/* Holds the sub-expression in PARALLEL   */
	rtx asm_op;		/* Holds the asm operand 		  */
	const char *str;	/* Holds the string literal of the asm    */

	/* examine all sub-expressions */
	for (i = 0; i < XVECLEN(expr, 0); i++) {
		sub_expr = XVECEXP(expr, 0, i);
		/* is it an asm set expression? */
		if (SET_P(sub_expr) &&
				GET_CODE(XEXP(sub_expr, 1)) == ASM_OPERANDS) {
			/* yes. Extract operand */
			asm_op = XEXP(sub_expr, 1);
			/* extract string literal */
			str = XSTR(asm_op, 1);
			/* does it set %esi? */
			if (!strcmp("=S", str))
				/* yes. We're done. */
				return SUCC;
		}
	}
	/* the expression doesn't set %esi */
	return FAIL;
}

/*
 * Some functions contain inline asm that explicitly use %esi.
 * Since it's not possible to use -ffixed-esi for those functions
 * we place them in separate files that are not compiled with this flag.
 *
 * Since %esi is not a scratch register the compiler assumes that it can be used
 * as a callee-saved register (something that is wrong since we always use %esi
 * for the return address protection scheme).
 * In this function we annotate every call instruction (either in C or in inline
 * ASM) with "clobber %esi" to force the compiler to spill %esi when calling a
 * function.
 *
 */

unsigned int __attribute__ ((visibility("default")))
clobber_esi(void)
{
	rtx insn_it;		/* instruction iterator 		  */
	unsigned long i;	/* Iterator of PARALLEL RTL expressions   */
	rtx expr;		/* Holds the sub-expression in PARALLEL   */
	bool has_asm;		/* 
				 *  1 if the current instruction is an
				 *  asm call, 0 otherwise
				 */
	rtvec new_vec;		/* holds the new asm instruction 	  */
	unsigned long len;	/* holds the length of the XVEC		  */
	rtx parallel;		/* new parallel instruction		  */
	rtx clobber;		/* clobber %esi expression		  */

	/* Does this function have %esi fixed? */
	if (fixed_regs[SI_REG])
		/* yes, nothing to do */
		return SUCC;
	/* no, iterate all instructions */
	INSN_ITER(insn_it) {
		/* we only care about calls and parallels */
		if (!INSN_P(insn_it))
			continue;
		/* is this a parallel instruction? */
		if (PARALLEL_P(PATTERN(insn_it))) {
			/* initialization */
			has_asm = 0;
			/* 
			 * if this instruction modifies %esi then we don't need
			 * to retain its value
			 */
			if (sets_esi(PATTERN(insn_it)) == SUCC)
				continue;
			/* examine every sub-expression */
			for (i = 0; i < XVECLEN(PATTERN(insn_it), 0); i++) {
				expr = XVECEXP(PATTERN(insn_it), 0, i);
				/* does it contain an asm call? */
				if (contains_asm(expr, "call", 1) == SUCC) {
					/* yes */
					has_asm = 1;
					break;
				}	
			}
			/* does it contain an asm call? */
			if (!has_asm)
				/* no. Nothing to do here */
				continue;
			/* this parallel has asm with call. Add clobber */
			len = XVECLEN(PATTERN(insn_it), 0);
			/* allocate rtvec with incremented size for the clobber
			 * esi instruction */
			new_vec = rtvec_alloc(len + 1);
			for (i = 0; i < len; i++)
				/* add all elements of PARALLEL */
				new_vec->elem[i] = XVECEXP(PATTERN(insn_it),
									0, i);
			/* generate clobber %esi */
			clobber = gen_rtx_CLOBBER(VOIDmode,
						gen_rtx_REG(SImode, SI_REG));
			/* add it at the end of the new rtvec */
			new_vec->elem[len] = clobber;
			/* generate new parallel with the new rtvec */
			parallel = gen_rtx_PARALLEL(GET_MODE(PATTERN(insn_it)),
							new_vec);
			/* replace the old parallel instruction with the new */
			replace_rtx(insn_it, PATTERN(insn_it), parallel);
		} else if (CALL_P(insn_it) && !SIBLING_CALL_P(insn_it)) {
			/* this is a call */
			/* generate clobber %esi */
			clobber = gen_rtx_CLOBBER(VOIDmode,
						gen_rtx_REG(SImode, SI_REG));
			/* emit it after the call instruction */
			emit_insn_after(clobber, insn_it);
		}
	}
	/* done */
	return SUCC;
}

/*
 * Handles marked regions that span multiple basic blocks
 *
 * Legitimate reads/writes to the protected region need to be
 * "whitelisted". We do this by annotating the beginning of such
 * regions with "krx_start:" inline assembly statements and their end
 * with "krx_end:". These statements are removed by the assembler and
 * all memory reads between them are prefixed with %ss.
 *
 * If these regions span multiple basic blocks (typically caused by code
 * block permutation) then we recursively visit all basic blocks, annotate their
 * beginning with "krx_start:" and their end with "krx_end:", unless we find the
 * end of region while looking in their instructions
 *
 * orig_insn: the instruction that marks the beginning of the whitelisted region
 * insn: typically the first instruction of the successor basic block. If this
 * 	 is the first time this called, insn == orig_insn
 * successor: 0 if this the first time it's called, 1 if it's a recursive call.
 */
void handle_markers(rtx orig_insn, rtx insn, long successor)
{
	rtx insn_it;		/* Instruction iterator 		  */
	unsigned long i;	/* Iterator of PARALLEL RTL expressions   */
	rtx expr;		/* Holds the sub-expression in PARALLEL   */
	rtx start;		/* "krx_start:" assembly expression	  */
	rtx end;		/* "krx_end:" assembly expression 	  */
	edge e;			/* Holds the successors of the current bb */
	edge_iterator ei;	/* edge iterator			  */

	/* is this a recursive call? */
	if (successor) {
		/* yes, is this an instruction? */
		if (INSN_P(insn)) {
			/* 
			 * is this a "krx_start:"?
			 * if yes, we've already visited this bb so do nothing
			 * */
			if (PARALLEL_P(PATTERN(insn))) {
				for (i = 0; i < XVECLEN(PATTERN(insn), 0); i++) {
					expr = XVECEXP(PATTERN(insn), 0, i);
					if (contains_asm(expr, "krx_start:", 0)
								      == SUCC) {
						/* already visited bb */
						return;
					}
				}
			}
		}
		/* did we end up in the same basic block we started? */
		if (BLOCK_FOR_INSN(orig_insn) == BLOCK_FOR_INSN(insn)) {
			/* should never happen */
			return;
		}
		/* 
		 * this is a new basic block.
		 * Add "krx_start:" as its first instruction
		 */
		start = gen_rtx_ASM_OPERANDS(VOIDmode,
					"krx_start:",
					"",
					0,
					rtvec_alloc(0),
					rtvec_alloc(0),
					rtvec_alloc(0),
					0);
		/* emit "krx_start:" */
		emit_insn_before(start, get_next_insn(insn));
	}

	insn_it = insn;
	/* iterate all instructions of the basic block */
	while (insn_it != BB_END(BLOCK_FOR_INSN(insn))) {
		/* is this an instruction? */
		if (!INSN_P(insn_it)) {
			/* no, nothing to do here */
			insn_it = NEXT_INSN(insn_it);
			continue;
		}
		/* inline asm instructions are always PARALLEL expressions */
		if (PARALLEL_P(PATTERN(insn_it))) {
			/* iterate all parallel sub-expressiosn */
			for (i = 0; i < XVECLEN(PATTERN(insn_it), 0); i++) {
				expr = XVECEXP(PATTERN(insn_it), 0, i);
				/* is this the end marker? */
				if (contains_asm(expr, "krx_end:", 0) == SUCC) {
					/* yes, nothing to do here */
					return;
				}
			}
		}
		/* next instruction */
		insn_it = NEXT_INSN(insn_it);
	}
	/* test last insn. Should never be a asm but to be on the safe side */
	if (INSN_P(insn_it)) {
		if (PARALLEL_P(PATTERN(insn_it))) {
			/* iterate all parallel sub-expressiosn */
			for (i = 0; i < XVECLEN(PATTERN(insn_it), 0); i++) {
				expr = XVECEXP(PATTERN(insn_it), 0, i);
				/* is this the end marker? */
				if (contains_asm(expr, "krx_end:", 0) == SUCC)
					/* yes, nothing to do here */
					return;
			}
		}
	}
	/*
	 * Reached the end of the bb and didn't find the end marker
	 * Add a "krx_end:" inline asm statement.
	 */
	end = gen_rtx_ASM_OPERANDS(VOIDmode,
					"krx_end:",
					"",
					0,
					rtvec_alloc(0),
					rtvec_alloc(0),
					rtvec_alloc(0),
					0);
	/* emit the inline asm */
	emit_insn_before(end, insn_it);

	/* iterate all successors of current basic block */
	FOR_EACH_EDGE(e, ei, BLOCK_FOR_INSN(insn)->succs) {
		/* handle the new basic block */
		handle_markers(orig_insn, BB_HEAD(e->dest), 1);
	}
}
#endif

/* 
 * list of functions that should not have their return addresses protected
 *
 * NOTE: the list *needs* to be lexicographically sorted.
 */
const char *xor_whitelist[] = {
	"krx_panic",
	"krx_phantom"
};

/*
 * check if a function is whitelisted
 *
 * Some functions must be whitelisted because they use
 * `__builtin_return_address' to produce informative error messages
 *
 * func_name: 	the name of the function that will be instrumented
 *
 * returns:	SUCC if the function belongs to the whitelised functions
 * 		FAIL otherwise
 */
static int compar_fn(const void *s1, const void *s2)
{ 
	return strcmp((const char *)s1, *((const char **)s2));
}

static int
is_whitelisted_function(const char *func_name)
{
	return (bsearch(func_name,
			xor_whitelist,
			sizeof(xor_whitelist)/sizeof(xor_whitelist[0]),
			sizeof(xor_whitelist[0]),
			compar_fn)) ? SUCC : FAIL;
}

/*
 * Return address protection through XOR encryption.
 *
 * Encrypts the return address of the function with a unique key
 * and decrypts it before return (or tail jump). It also zaps the
 * return address after the call.
 *
 * Note: It's tricky to define the keys inside the function through
 * the compiler pass (adding them through inline assembly triggers faults).
 * Therefore we emit the instructions that load the encryption key to the
 * reserved register with a "placeholder" symbol (`krx_replace_me')
 * and then use an assembler wrapper to define the keys and replace it with
 * the proper symbol. Finally, the linker wrapper randomizes both the functions
 * and the encryption keys.
 *
 */
void encrypt_return_addresses(void)
{
	rtx insn_it;		/* instruction iterator */
	rtx mov;		/* the move of the key to the
				   reserved register [mov key,%r10] */
	rtx reg;		/* the RTX of the reserved register [%r10]*/
	rtx mem;		/* the memory of the return address [(%rsp)] */
	rtx xor_insn;		/* the partial xor encryption/decryption
				   instruction */
	rtx parallel;		/* the complete encryption instruction
				   [xor %r10,(%rsp)] */
	rtx flags_reg;		/* the rflags RTX */
	rtx symbol;		/* the temporary symbol of the key (to be
				   replaced by the assembler wrapper) */
	rtx clobber_flags;	/* clobber rflags RTX */
	rtx first_insn;		/* the first instruction of the code stream */
	rtx label;		/* the label that will be the target
				   of the entry jmp */
	basic_block entry_bb;	/* the (original) entry basic block */
	const char *name;	/* the name of the function */
	rtx zero_mem;		/* the memory to be zapped after the call
				   instruction [-0x8(%rsp)] */
	rtx zero_mov;		/* the actual zapping instruction
				   [mov $0x0,-0x8(%rsp)] */
	rtx next_insn;		/* the instruction that follows the call
				   instruction */
	rtx entry_jmp;		/* the jump to the entry basic block */

	unsigned i;
	rtx expr;
	/* 
	 * NOTE: we add the initial jmp instruction
	 * of the code block permutation to ensure the jmp is
	 * placed right after encrypting the return address
	 */
	name = IDENTIFIER_POINTER(DECL_NAME(current_function_decl));
	/* reserved register (%r10) RTX */
	reg = gen_rtx_REG(MODE, RES_REG);
	/* the first instruction in the code stream */
	first_insn = BB_HEAD(ENTRY_BLOCK_PTR->next_bb);
	/* temporary symbol RTX. The assembler wrapper
	 * will replace it with the key */
	symbol = gen_rtx_SYMBOL_REF(Pmode, "krx_replace_me");
	/* the memory of the return address (%rsp) */
	mem = gen_rtx_MEM(MODE, stack_pointer_rtx);
	/* rflags register */
	flags_reg = gen_rtx_REG(CCmode, FLAGS_REG);
	/* clobber rflags, necessary for emitting the xor instruction */
	clobber_flags = gen_rtx_CLOBBER(VOIDmode, flags_reg);
	/* move the symbol (key) to the reserved register:
	 * mov symbol(%rip), %r10 */
	mov = gen_rtx_SET(MODE, reg, gen_rtx_MEM(MODE, symbol));
	/* xor the return address with the reserved register: xor %r10,(%rsp) */
	xor_insn = gen_rtx_SET(VOIDmode, mem, gen_rtx_XOR(MODE, mem, reg));
	/* finally merge the xor instruction with the clobber to form the
	 * RTX that is translated to xor %r10,(%rsp) */
	parallel = gen_rtx_PARALLEL(MODE, gen_rtvec(2, xor_insn, clobber_flags));
	/* don't emit instrumentation for whitelisted functions */
	if (is_whitelisted_function(name) == FAIL) {
		/* emit the mov instruction */
		emit_insn_before(mov, first_insn);
		/* emit the xor instruction */
		emit_insn_before(parallel, first_insn);
	}
	/* is the first basic block the entry block? */
	if (entry_bbs.find(name) != entry_bbs.end()) {
		/* no, get the entry block */
		entry_bb = entry_bbs[name];
		/* generate a new label */
		label = gen_label_rtx();
		/* iterate the instructions in the entry basic block */
		FOR_BB_INSNS(entry_bb, insn_it) {
			/* continue the iteration till we find an INSN RTX */
			if (INSN_P(insn_it))
				break;
		}
		/* place the new label before the first instruction
		 * of the entry basic block */
		emit_label_before(label, insn_it);

		/* emit the unconditional direct jump to the label */
		entry_jmp = emit_jump_insn_before_noloc(gen_jump(label), first_insn);
		/* set the label as the target of the jump */
		JUMP_LABEL(entry_jmp) = label;
		/* increase the number of uses of the label */
		LABEL_NUSES(label)++;
	}

	/* no instrumentation for whitelisted functions */
	if (is_whitelisted_function(name) == SUCC)
		return;

	/* 
	 * We want to zap the return addresses after they are used
	 * (i.e. after every `call' instruction).
	 * Since the `ret' instruction increases the `%rp',
	 * we need to account for this offset. Therefore
	 * we target -0x8(%rsp)
	*/
	zero_mem = gen_rtx_MEM(MODE, gen_rtx_PLUS(MODE,
				stack_pointer_rtx, GEN_INT(RET_OFFSET)));
	/* generate the zapping of the memory: mov $0x0,-0x8(%rsp) */
	zero_mov = gen_rtx_SET(MODE, zero_mem, GEN_INT(0));
	/* iterate the code stream */
	INSN_ITER(insn_it) {
		
		/* we only care about INSN, JUMP and CALL expressions */
		if (GET_CODE(insn_it) != INSN &&
				!JUMP_P(insn_it) &&
				!CALL_P(insn_it))
			continue;
#if	linux && __i386__
		if (PARALLEL_P(PATTERN(insn_it))) {
			for (i = 0; i < XVECLEN(PATTERN(insn_it), 0); i++) {
				expr = XVECEXP(PATTERN(insn_it), 0, i);
				if (contains_asm(expr, "krx_start:", 0) == SUCC) {
					handle_markers(insn_it, insn_it, 0);
					break;
				}	
			}
		}
#endif
		/* handle calls */
		if (CALL_P(insn_it)) {
			/* is it a tail jump? */
			if (SIBLING_CALL_P(insn_it)) {
				/* yes, we need to decrypt so that the
				 * callee with encrypt it with it's own key,
				 * so move the key to %r10 and then
				 * xor decrypt the return address memory */
				emit_insn_before(mov, insn_it);
				emit_insn_before(parallel, insn_it);
			} else {
				/* get the next instruction after the call */
				next_insn = get_next_insn(insn_it);
				/* is this call the last instruction in the
				 * code stream? */
				if (next_insn) {
					/* no, add the zapping before the next instruction */
					emit_insn_before(zero_mov, next_insn);
				}
			}
		}
		/* handle returns */
		if (ANY_RETURN_P(PATTERN(insn_it)) ||
					GET_CODE(PATTERN(insn_it)) == EH_RETURN) {
			/* 
			 * decrypt the return address, so first
			 * move the key to %r10 and then xor decrypt the
			 * return address
			 */
			emit_insn_before(mov, insn_it);
			emit_insn_before(parallel, insn_it);
		}
	}
}

/* 
 * Encryption pass
 * (callback; invoked for every translation unit) 
 *
 * Instruments the prologue and epilogue of every function
 * to encrypt the return address with a unique key.
 */
unsigned int __attribute__ ((visibility("default")))
xor_instrument(void)
{

	encrypt_return_addresses();
	/* return with success */
	return SUCC;
}
