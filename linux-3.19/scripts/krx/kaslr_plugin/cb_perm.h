#ifndef __CODEBLOCKPERM_H__
#define __CODEBLOCKPERM_H__
#include "gcc-plugin.h"
#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tree.h"
#include "tree-pass.h"
#include "basic-block.h"
#include "rtl.h"
#include "tm.h"
#include "toplev.h"
#include "gimple.h"
#include "gimple-pretty-print.h"
#include "intl.h"
#include "cfglayout.h"
#include "emit-rtl.h"
#include "plugin-version.h"
#include "config/i386/i386-protos.h"
#include "cgraph.h"
#include <bsd/stdlib.h>
#include <vector>
#include <map>
#include <set>
#include <string.h>
#include <stdlib.h>

/* plugin name and version */
#define NAME		"kaslr"
#define VER		"0.0000001alpha"

/* compiler directives for branch prediction */
#define	likely(x) 	__builtin_expect((x), 1)
#define	unlikely(x)	__builtin_expect((x), 0)

/* enable/disable values for MPX scheme */
#define DISABLE		0
#define ENABLE		1

/* macro definitions */
#define BASE10 		10		/* 10th base			*/
#define BASE16 		16		/* 10th base			*/
#define BASE100 	100		/* 100th base			*/
#define VER_DELIM	'.'		/* version delimiter		*/
#define VERBOSE_STR	"verbose"	/* `verbose' option (argument) literal */
#define CBMODE_STR	"chunk-granularity" /* granularity option (argument)
					       			literal */

extern unsigned int verbose;
extern unsigned long cb_mode;
#define	BB_GRAN		0

#if	linux && __i386__
unsigned int clobber_esi(void);
#endif
unsigned int xor_instrument(void);
unsigned int cb_perm_instrument(void);

extern std::map<const char*, basic_block> entry_bbs;

enum {
	SUCC = 0,	/* success; return value */
	FAIL = 1	/* failed; return value */
};

#endif /* __CODEBLOCKPERM_H__ */
