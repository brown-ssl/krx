#ifndef __KRX_H__
#define __KRX_H__

#include "gcc-plugin.h"
#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tree.h"
#include "tree-pass.h"
#include "basic-block.h"
#include "rtl.h"
#include "tm.h"
#include "toplev.h"
#include "gimple.h"
#include "gimple-pretty-print.h"
#include "intl.h"
#include "cfglayout.h"
#include "emit-rtl.h"
#include "plugin-version.h"
#include "config/i386/i386-protos.h"
#include "cgraph.h"
#include <bsd/stdlib.h>
#include <vector>
#include <map>
#include <set>
#include <string.h>
#include <stdlib.h>
#define DEBUG	1
/* plugin name and version */
#define NAME		"krx"
#define VER		"0.0000001alpha"

/* compiler directives for branch prediction */
#define	likely(x) 	__builtin_expect((x), 1)
#define	unlikely(x)	__builtin_expect((x), 0)

/* enable/disable values for MPX scheme */
#define DISABLE		0
#define ENABLE		1

/* optimization level values */
#define	O0		0
#define	O1		1
#define	O2		2
#define	O3		3

/* macro definitions */
#define BASE10 		10		/* 10th base			*/
#define BASE16 		16		/* 10th base			*/
#define BASE100 	100		/* 100th base			*/
#define VER_DELIM	'.'		/* version delimiter		*/
#define STUB_STR	"stub"		/* `stub' option (argument) literal */
#define STUB_DFL	"krx_panic"	/* `stub' default value		*/
#define MPX_STR		"mpx"		/* `mpx' option (argument) literal */
#define LIMIT_STR	"edata"		/* `edata' option (argument) literal */
#define OPT_LEVEL_STR	"level"		/* `level' option (argument) literal */
#ifdef	DEBUG
#define	LOG_STR		"log"		/* `log' option (argument) literal */
#endif

enum conditions {
	COND_EQ,
	COND_NE,
	COND_LT,
	COND_GE,
	COND_LE,
	COND_GT,
	COND_LTU,
	COND_GEU,
	COND_LEU,
	COND_GTU
};

typedef struct {
	rtx insn;
	rtx real_insn;
	rtx lea;
	rtx base;
	rtx index;
	rtx symbol;
	long long  offset;
	long long scale;
	bool eliminated;
	bool flags;
	int addr_mode;
} check_entry;

extern std::set<basic_block> visited;
extern std::vector<unsigned> same_lea;

enum {
	SUCC = 0,	/* success; return value */
	FAIL = 1	/* failed; return value */
};


#endif /* __KRX_H__ */
