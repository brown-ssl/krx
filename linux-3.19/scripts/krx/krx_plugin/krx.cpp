#if	linux  && __amd64__
#include "krx.h"
#include <fstream>
#include <string>
std::set<basic_block> visited;
std::vector<unsigned> same_lea;
/* function declarations */
unsigned int krx_instrument(void);
static int is_cmov(rtx);

/* macro definitions */
#define BB_ITER_BEGIN_END(insn_it, begin, end)					\
				for (insn_it = begin;				\
					insn_it &&				\
					insn_it != end;				\
					insn_it = NEXT_INSN(insn_it))
#define BB_ITER_START(insn_it, start)	rtx end_bb_insn;			\
				end_bb_insn = BB_END(BLOCK_FOR_INSN(start));	\
				for (insn_it = start;				\
					insn_it && 				\
					insn_it != NEXT_INSN(end_bb_insn);	\
					insn_it = NEXT_INSN(insn_it))
#define BB_ITER_START_REVERSE(insn_it, start)	rtx start_bb_insn;		\
				start_bb_insn = BB_HEAD(BLOCK_FOR_INSN(start));	\
				for (insn_it = PREV_INSN(start);		\
					insn_it && 				\
					insn_it != PREV_INSN(start_bb_insn);	\
					insn_it = PREV_INSN(insn_it))
#define	INSN_ITER(insn_it)	basic_block bb_it; 		\
				FOR_EACH_BB(bb_it) 		\
					FOR_BB_INSNS(bb_it, insn_it)

#define	INSN_ITER_BB(bb_it, insn_it)	FOR_EACH_BB(bb_it)	\
						FOR_BB_INSNS(bb_it, insn_it)

#define PLUS_P(rtx)	(GET_CODE(rtx) == PLUS || GET_CODE(rtx) == SS_PLUS || \
					GET_CODE(rtx) == US_PLUS)
#define MINUS_P(rtx)	(GET_CODE(rtx) == MINUS || GET_CODE(rtx) == SS_MINUS || \
					GET_CODE(rtx) == US_MINUS)
#define MULT_P(rtx)	(GET_CODE(rtx) == MULT || GET_CODE(rtx) == SS_MULT || \
					GET_CODE(rtx) == US_MULT)
#define DIV_P(rtx)	(GET_CODE(rtx) == DIV || GET_CODE(rtx) == SS_DIV || \
					GET_CODE(rtx) == US_DIV || \
					GET_CODE(rtx) == UDIV)
#define	EXTEND_P(rtx)	(GET_CODE(rtx) == SIGN_EXTEND || \
					GET_CODE(rtx) == ZERO_EXTEND)

#define ARITHM_P(rtx)	(PLUS_P(rtx) || MINUS_P(rtx) || XOR_P(rtx) \
					|| OR_P(rtx) || AND_P(rtx) \
					|| MULT_P(rtx) || DIV_P(rtx))
#define INSN_CODE_P(rtx)	(GET_CODE(rtx) == INSN)
#define EH_RETURN_P(rtx)	(GET_CODE(rtx) == EH_RETURN)
#define CONST_P(rtx)		(GET_CODE(rtx) == CONST)
#define ASHIFTRT_P(rtx)		(GET_CODE(rtx) == ASHIFTRT)
#define STRICT_LOW_PART_P(rtx)	(GET_CODE(rtx) == STRICT_LOW_PART)
#define BSWAP_P(rtx)		(GET_CODE(rtx) == BSWAP)
#define PARALLEL_P(rtx)		(GET_CODE(rtx) == PARALLEL)
#define IF_THEN_ELSE_P(rtx)	(GET_CODE(rtx) == IF_THEN_ELSE)
#define SUBREG_P(rtx)		(GET_CODE(rtx) == SUBREG)
#define COMPARE_P(rtx)		(GET_CODE(rtx) == COMPARE)
#define SET_P(rtx)		(GET_CODE(rtx) == SET)
#define ASHIFT_P(rtx)		(GET_CODE(rtx) == ASHIFT)
#define USE_P(rtx)		(GET_CODE(rtx) == USE)
#define AND_P(rtx)		(GET_CODE(rtx) == AND)
#define XOR_P(rtx)		(GET_CODE(rtx) == XOR)
#define OR_P(rtx)		(GET_CODE(rtx) == IOR)
#define CLOBBER_P(rtx)		(GET_CODE(rtx) == CLOBBER)
#define ASM_P(rtx)		(GET_CODE(rtx) == ASM_OPERANDS)
#define SYMBOL_P(rtx)		(GET_CODE(rtx) == SYMBOL_REF)
#define NE_P(rtx)		(GET_CODE(rtx) == NE)
#define EQ_P(rtx)		(GET_CODE(rtx) == EQ)
#define GE_P(rtx)		(GET_CODE(rtx) == GE)
#define GT_P(rtx)		(GET_CODE(rtx) == GT)
#define LE_P(rtx)		(GET_CODE(rtx) == LE)
#define LT_P(rtx)		(GET_CODE(rtx) == LT)
#define GEU_P(rtx)		(GET_CODE(rtx) == GEU)
#define GTU_P(rtx)		(GET_CODE(rtx) == GTU)
#define LEU_P(rtx)		(GET_CODE(rtx) == LEU)
#define LTU_P(rtx)		(GET_CODE(rtx) == LTU)
#define RES_REGNO		R10_REG
#define	RBP_REGNO		(XINT(hard_frame_pointer_rtx, 0))
#define RSP_REGNO		(XINT(stack_pointer_rtx, 0))
#define REG_NAME(reg)		(reg_names[XINT(reg, 0)])

#define MEM_ERR			-1
#define	MEM_BASE		0
#define MEM_OFF			1
#define MEM_REST		2
/* dbg config (with modules) */
/* dbg config (with modules) */
//#define TEXT_START		0xffffffffc282d000
/* deb config (with modules) */
#define TEXT_START		0xffffffffc1508000
/* min config (with modules) */
//#define TEXT_START		0xffffffffc13d7000

/* assert GPL compatibility */
int __attribute__ ((visibility("default"))) plugin_is_GPL_compatible; 

/* plugin information structure */
struct plugin_info __attribute__ ((visibility("default")))
pinfo = {
	.version	= VER,
	.help		= NULL
};

static bool
gate_krx(void)
{
	return true;
}

/* descriptor for the new pass provided by the plugin */
struct rtl_opt_pass __attribute__ ((visibility("default")))
pass_krx = {{ 
	RTL_PASS,
	"krx",
	gate_krx,
	krx_instrument,
	NULL,
	NULL,
	0,
	TV_NONE,
	PROP_rtl,
	0, 0, 0, 0
}};

/* plugin versioning structure */
static struct plugin_gcc_version
pver = {
	.basever	= "4.7.0",
	.datestamp	= "",
	.devphase	= "",
	.revision	= "",
	.configuration_arguments = ""
};

/* stub; run-time violation handler (address or symbol) */
static const char *stub		= STUB_DFL;

/* 
 * MPX knob
 * if DISABLE, use SFI scheme, else (ENABLE) use MPX scheme
 */
static unsigned long mpx	= DISABLE;

/*
 * __krx_edata address
 * holds the address of __krx_edata that will be used in the checks (SFI scheme)
 */
static unsigned long limit	= TEXT_START;

/* 
 * optimization level
 *
 * Valid values for SFI scheme:
 * O0, O1, O2, O3
 *
 * When using the MPX scheme, anything different
 * than O3 disables the optimization
 */
static unsigned long level	= O3;

#ifdef	DEBUG
/* log filename */
static char *log 		= NULL;

/* logfile */
static FILE *flog		= NULL;

/*
 * start the auditing process
 *
 * Open the logfile in append mode.
 */
static void
openlog(void)
{
	/* check if a filename for logging has been specified */
	if (log != NULL) {
		/* open the logfile in appending mode */
		if (likely((flog = fopen(log, "a")) != NULL))
			/* success */
			/* dump information regarding the translation unit */
			(void)fprintf(flog, "[/F]:%s\n",
				IDENTIFIER_POINTER(DECL_NAME(cfun->decl)));
		else
			/* failed */
			(void)fprintf(stderr,
				"Failed while trying to open %s (%s)\n",
					log, xstrerror(errno));
	}
}

/*
 * terminate the auditing process
 *
 * Close the logfile.
 */
static void
closelog(void)
{
	/* check if logging has been enabled */
	if (flog != NULL) {
		/* dump information regarding the translation unit */
		(void)fprintf(flog, "[F/]:%s\n",
				IDENTIFIER_POINTER(DECL_NAME(cfun->decl)));

		/* cleanup */
		(void)fclose(flog);
	}
}

/*
 * perform the actual logging
 *
 * Print the RTL expression of the inspected instruction; 
 * if the optional argument str is not NULL, it prepends
 * it to the instruction insn.
 *
 * insn:	the instruction to log
 * str:		the description of the instruction
 */
static void
commitlog(const rtx insn, const char *str)
{
	/* check if logging has been enabled */
	if (flog != NULL) {
		if (str != NULL)
			fprintf(flog, "%s\n", str);
		/* dump the instruction */
		print_rtl_single(flog, insn);
	}
}
#endif /* DEBUG */

/*
 * identifies the offset (if any) of a memory
 * read (and the symbol read if applicable)
 *
 * expr:		the expression that might contain the offset
 * symbol:		will hold the symbol to be read (if any)
 * offset: 		will hold the offset of the operation (if any)
 *
 * returns:		SUCC if the operation contains an
 * 			offset and/or a symbol
 * 			FAIL otherwise
 */
static int identify_offset(rtx expr, rtx *symbol, long long *offset)
{
	rtx sub_expr;	/* holds sub-expression to be examined 	*/
	rtx src;	/* the source operand of the sub-expression */
	rtx dst;	/* the destination operand of the sub-expression */


	/* simple offset */
	if (CONST_INT_P(expr)) {
		*offset = INTVAL(expr);
		goto out;
	/* simple symbol */
	} else if (SYMBOL_P(expr)) {
		*symbol = expr;
		*offset = 0;
		goto out;
	/* symbol + offset */
	} else if (CONST_P(expr)) {
		/* extract operand */
		sub_expr = XEXP(expr, 0);
		/* if it has an offset, this is always a PLUS */
		if (!PLUS_P(sub_expr))
			/* no offset */
			goto err;
		/* 
		 * there should be one SYMBOL and one const_int
		 * without specific order
		 */
		/* extract source and destination operands */
		dst = XEXP(sub_expr, 0);
		src = XEXP(sub_expr, 1);
		/* symbol@dst, constant@src*/
		if (SYMBOL_P(dst) && CONST_INT_P(src)) {
			*symbol = dst;
			*offset = INTVAL(src);
			goto out;
		/* symbol@src, constant@dst */
		} else if (SYMBOL_P(src) && CONST_INT_P(dst)) {
			*symbol = src;
			*offset = INTVAL(dst);
			goto out;
		} else {
			/* no offset */
			goto err;
		}
	/* everything else is not an offset */
	} else
err:
		/* not an offset */
		return FAIL;
out:
	/* offset (and symbol) found */
	return SUCC;
}

/*
 * extracts the information from a MEM_REST memory expression.
 *
 * NOTE: This case is very complicated because there are dozens of different
 * (valid) RTL interpretations and we need to handle all of them.
 *
 * What follows is the list of all the valid representations.
 *
 * Wthout symbol:
 * (plus (mult (index) (scale)) (base)) : (base, index, scale (!= 1))
 * (plus (plus (base) (index)) (offset)): offset(base, index, 1)
 * (plus (plus (mult (index) (scale)) (base)) (offset)):
 * 				offset(base, index, scale (!= 1))
 * (plus (mult (index) (scale)) (offset)) : offset(,index, scale (!=1))
 * (mult (index) (scale)): (, index, scale(!=1))
 * (plus (base) (index)) : (base, index, 1)
 * 
 * With symbol:
 * (plus (mult (index) (scale)) (symbol_ref)) :
 * 					symbol(,index, scale)
 * (plus (mult (index) (scale)) (const (plus (symbol) (offset)))) :
 * 			symbol+offset(,index, scale)
 * (plus (mult (base) (2)) (symbol)) : symbol(base, base, 1)
 * (plus (plus (base) (index)) (symbol)): symbol (base, index, 1)
 * (plus (plus (base) (index)) (const (plus (symbol) (offset)))) :
 * 			symbol+offset(base, symbol, 1)
 * (plus (plus (mult (index) (scale)) (base)) (symbol)) :
 * 			symbol (base, index, scale)
 * (plus (plus (mult (index) (scale)) (base))
 * 				(const (plus (symbol) (offset)))) :
 *				symbol+offset(base, index, scale)
 * (plus (base) (symbol)) : symbol(base)
 * (plus (base) (const (plus (symbol) (offset)))) :
 * 			symbol+offset(base)
 *
 *
 * mem_expr:	the memory read expression 
 * base:	will hold the base register of the operation (if any)
 * index:	will hold the index register of the operation (if any)
 * scale:	will hold the scale factor of the operation (if any)
 * offset:	will hold the offset of the operation (if any)
 * symbol:	will hold the symbol being read (if any)
 *
 */
static void extract_mem_rest_info(rtx mem_expr, rtx *base, rtx *index,
							long long *scale,
							long long *offset,
							rtx *symbol)
{
	rtx sub_expr;	/* holds sub-expression to be examined 	*/
	rtx src;	/* the source operand of the sub-expression */
	rtx dst;	/* the destination operand of the sub-expression */

	/* extract operand */
	sub_expr = XEXP(mem_expr, 0);
	/* extract source and destination operands */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);

	/* no offset & no base */
	if (MULT_P(sub_expr)) {
		/* extract source and destination */
		src = XEXP(sub_expr, 1);
		dst = XEXP(sub_expr, 0);
		/* save index */
		*index = dst;
		/* save scale */
		*scale = INTVAL(src);
		/* done */
		return;
	}
	/* everything else should start with PLUS */
	else if (!PLUS_P(sub_expr))
		goto error;
	/* check if the second operand contains the offset/symbol */
	if (identify_offset(src, symbol, offset)) {
		/* the offset is at the source */
		if (PLUS_P(dst)) {
			/* extract source and destination operands of PLUS */
			src = XEXP(dst, 1);
			dst = XEXP(dst, 0);
			/* offset(base, index, 1) */
			if (REG_P(dst) && REG_P(src)) {
				/* save base */
				*base = dst;
				/* save index */
				*index = src;
				/* save scale */
				*scale = 1;
				/* done */
				return;
			/* offset(base, index, scale) */
			} else if (MULT_P(dst) && REG_P(src)) {
				/* save base */
				*base = src;
				/* extract the source and destination
				   operands of MULT */
				src = XEXP(dst, 1);
				dst = XEXP(dst, 0);
				/* sanity check */
				if (!REG_P(dst) || !CONST_INT_P(src))
					goto error;
				/* save index */
				*index = dst;
				/* save scale */
				*scale = INTVAL(src);
				/* done */
				return;
			/* everything else is an error */
			} else
				goto error;
		/* symbol(base) */
		} else if (REG_P(dst)) {
			/* save base */
			*base = dst;
			/* done */
			return;
		/* everything else must be MULT */
		} else if (!MULT_P(dst))
			goto error;
		/* extract source and destination operands of MULT */
		src = XEXP(dst, 1);
		dst = XEXP(dst, 0);
		/* sanity check */
		if (!REG_P(dst) || !CONST_INT_P(src))
			goto error;
		/* save index */
		*index = dst;
		/* save scale */
		*scale = INTVAL(src);
		/* done */
		return;
	/* handle weird restructuring of
	   offset(base, index, 1) */
	} else if (PLUS_P(dst)) {
		/* check if the second operand contains the offset/symbol */
		if (identify_offset(XEXP(dst, 1), symbol, offset)) {
			/* sanity check */
			if (!REG_P(src))
				goto error;
			/* save index */
			*index = src;
			/* extract source and destination operands of PLUS */
			src = XEXP(dst, 1);
			dst = XEXP(dst, 0);
			/* sanity check */
			if (!REG_P(dst))
				goto error;
			/* save base */
			*base = dst;
			/* save scale */
			*scale = 1;
			/* done */
			return;
		}
	/* (base, index, 1) */
	} else if (REG_P(dst) && REG_P(src)) {
		/* save base */
		*base = dst;
		/* save index */
		*index = src;
		/* save scale */
		*scale = 1;
		/* done */
		return;
	/* (base, index, scale) */
	} else if (MULT_P(dst) && REG_P(src)) {
		/* save base */
		*base = src;
		/* extract source and destination operands of MULT */
		src = XEXP(dst, 1);
		dst = XEXP(dst, 0);
		/* sanity check */
		if (REG_P(dst) && CONST_INT_P(src)) {
			/* save index */
			*index = dst;
			/* save scale */
			*scale = INTVAL(src);
			/* done */
			return;
		}
	}
error:
	/* anything else is error */
#ifdef	DEBUG
	commitlog(mem_expr, "Invalid mem_rest");
#endif
	return;
}

/*
 * examines if the expression contains a symbol
 * (recursive function).
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if it contains a symbol
 * 		FAIL otherwise
 */
static int contains_symbol(rtx expr)
{
	rtx sub_expr;		/* holds sub-expression to be examined 	*/
	const char *fmt; 	/* the format of the expression 	*/
	RTX_CODE code;		/* the code of the expression 		*/
	unsigned short i;	/* index to be used to traverse the format */

	/* is expr a symbol? */
	if (SYMBOL_P(expr))
		/* Yes, done */
		return SUCC;

	/* get code */
	code = GET_CODE(expr);
	/* get the format */
	fmt = GET_RTX_FORMAT(code);
	/* traverse the format */
	for (i = 0; i < GET_RTX_LENGTH(code); i++) {
		/* extract sub-expression */
		sub_expr = XEXP(expr, i);
		/* 
		 * we only care about expressions ('e', 'u')
		 * or strings/symbols ('s', 'S')
		 */
		if (fmt[i] != 'e' && fmt[i] != 's' &&
				fmt[i] != 'S' && fmt[i] != 'u')
			/* next in the format */
			continue;
		/* recursive call */
		if (contains_symbol(sub_expr) == SUCC)
			/* found a SYMBOL */
			return SUCC;
	}
	/* didn't find a SYMBOL */
	return FAIL;
}

/*
 * examines if the addressing mode of the memory read is (reg).
 *
 * mem_expr:	the memory read expression
 *
 * returns:	SUCC if it is of this mode
 * 		FAIL otherwise
 */
static int is_mem_base(rtx mem_expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined 	*/

	/* extract the sub-expression */
	sub_expr = XEXP(mem_expr, 0);
	/* 
	 * the length needs to be 3 and the 
	 * sub_expression needs to be a register
	 */
	if (GET_RTX_LENGTH(GET_CODE(sub_expr)) == 3 &&
				REG_P(sub_expr))
		/* MEM_BASE */
		return SUCC;
	/* MEM_OFF or MEM_REST */
	return FAIL;
}

/*
 * examines if the addressing mode of the memory read is offset(reg)
 *
 * NOTE: Some symbols are accessed through offset(reg).
 * Since we can't do the O2 optimization (the offset is known
 * only at link time), we do not identify them as MEM_OFF.
 *
 * mem_expr:	the memory read expression
 *
 * returns:	SUCC if it is of this mode
 * 		FAIL otherwise
 */
static int is_mem_offset(rtx mem_expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined 	*/
	rtx src;	/* the source operand of the sub-expression */
	rtx dst;	/* the destination operand of the sub-expression */

	sub_expr = XEXP(mem_expr, 0);
	/* 
	 * the length needs to be 2 and the
	 * sub-expression needs to be a PLUS
	 */
	if (GET_RTX_LENGTH(GET_CODE(sub_expr)) != 2		||
			!PLUS_P(sub_expr))
		goto err;
	/* extract the source and the destination operand */
	src = XEXP(sub_expr, 1);
	dst = XEXP(sub_expr, 0);
	if (!REG_P(dst) || !CONSTANT_P(src))
		goto err;
	/* make sure this is not a symbol reference */
	if (contains_symbol(sub_expr) == SUCC)
		goto err;
	/* MEM_OFF */
	return SUCC;
err:
	/* MEM_BASE or MEM_REST */
	return FAIL;
}

/*
 * identifies the addressing mode of the memory read
 *		MEM_BASE:	(reg)
 *		MEM_OFF:	offset(reg)
 *		MEM_REST:	offset(base, index, scale)
 *
  *
 * mem_expr:	the RTL expression of the memory read
 *
 * returns:	the mode of the memory read
 */
static int get_mem_read_mode(rtx mem_expr)
{
	/* MEM_BASE */
	if (is_mem_base(mem_expr) == SUCC)
		return MEM_BASE;
	/* MEM_OFF */
	else if (is_mem_offset(mem_expr) == SUCC)
		return MEM_OFF;
	/* MEM_REST */
	else
		return MEM_REST;
}
/*
 * identify simple memory reads (`mov', `movzwl', `movswl' etc).
 *
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction. We know this is not a PARALLEL instruction
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a memory read instruction
 * 		FAIL otherwise
 */
static int __is_mem_read(rtx expr)
{
	rtx src;	/* holds the source operand;
			   this will hold the memory read */

	/* all simple memory reads are SET */
	if (SET_P(expr)) {
		/* extract the source operand */
		src = XEXP(expr, 1);

		/* handle movzwl, movswl */
		while (EXTEND_P(src))
			/* extract operand */
			src = XEXP(src, 0);
		/* is it a memory operand? */
		if (MEM_P(src)) {
			/* memory read */
			return SUCC;
		}
	}
	/* not memory read */
	return FAIL;
}

/*
 * identify simple memory reads (`mov', `movzwl', `movswl' etc).
 *
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined. May be PARALLEL
 *
 * returns:	SUCC if the expression contains a memory read instruction
 * 		FAIL otherwise
 */
static int is_mem_read(rtx expr)
{
	int i;		/* Iterator of PARALLEL RTL expressions */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/


	if (PARALLEL_P(expr)) {
		/* examine the sub-expressions */
		for (i = 0; i < XVECLEN(expr, 0); i++) {
			/* extract sub-expression */
			sub_expr = XVECEXP(expr, 0, i);
			/* is it a memory read? */
			if (__is_mem_read(sub_expr) == SUCC)
				/* Yes */
				goto out;
		}
		/* no memory read */
		goto err;
	}
	/* is it a memory read? */
	if (__is_mem_read(expr) == SUCC)
		/* Yes */
		goto out;
err:
	/* no memory read */
	return FAIL;
out:
	/* memory read */
	return SUCC;
}


static rtx __extract_mem_read(rtx expr)
{
	rtx src;	/* holds the source operand;
			   this will hold the memory read */

	/* extract the source operand */
	src = XEXP(expr, 1);

	/* handle movzwl, movswl */
	while (EXTEND_P(src))
		/* extract operand */
		src = XEXP(src, 0);
	/* memory read */
	return src;
}

static rtx extract_mem_read(rtx expr, rtx *insn)
{
	int i;		/* Iterator of PARALLEL RTL expressions */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/


	if (PARALLEL_P(expr)) {
		/* examine the sub-expressions */
		for (i = 0; i < XVECLEN(expr, 0); i++) {
			/* extract sub-expression */
			sub_expr = XVECEXP(expr, 0, i);
			/* is it a memory read? */
			if (__is_mem_read(sub_expr) == SUCC) {
				/* place the range check right
				   before the sub-expression */
				*insn = sub_expr;
				/* extract the memory expression */
				return __extract_mem_read(sub_expr);
			}	
		}
		/* paranoia */
		return NULL;
	}
	/* extract the memory expression */
	return __extract_mem_read(expr);
}

/*
 * extract the memory expression of an
 * arithmetic memory read instruction
 *
 *
 * expr:	the arithmetic memory
 * 		read instruction
 *
 * returns:	the memory expression (memory read)
 */
static rtx __extract_arithmetic_mem_read(rtx expr)
{
	rtx src;	/* holds the source operand;
			   this will hold the memory read */

	/* extract source operand */
	src = XEXP(expr, 1);

	/* handle extend expression */
	while (EXTEND_P(src))
		/* extract the operand of EXTEND */
		src = XEXP(src, 0);

	/* handle 3 operand MULT form */
	if (MULT_P(src)) {
		/* extract source operand */
		src = XEXP(src, 1);
		/* is this operand memory? */
		if (MEM_P(src))
			/* Yes, this is a memory read */
			return src;
	}

	/* generic case */
	if (ARITHM_P(src)) {
		/* extract source operand */
		src = XEXP(src, 1);
		/* is this operand memory? */
		if (MEM_P(src)) {
			/* Yes, this is a memory read */
			return src;
		}
	}
	/* paranoia */
	return NULL;
}

/*
 * identify arithmetic memory reads.
 * Arithmetic instructions are:
 * PLUS, SUB, MUL, DIV, OR, XOR, AND
 *
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction. We know that expr is NOT a PARALLEL.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents an arithmetic instruction
 * 		FAIL otherwise
 */
static int __is_arithmetic_mem_read(rtx expr)
{
	rtx src;	/* holds the source operand;
			   this will hold the memory read */

	/* all arithmetic instructions are SET */
	if (!SET_P(expr))
		goto err;
	/* extract source operand */
	src = XEXP(expr, 1);
	/* ensure we extract the real operand */
	while (EXTEND_P(src))
		/* extract the operand of EXTEND */
		src = XEXP(src, 0);
	/* special case: 3 operand MULT form */
	if (MULT_P(src)) {
		/* extract source operand */
		src = XEXP(src, 1);
		/* is this operand memory? */
		if (MEM_P(src))
			/* Yes, this is a memory read */
			goto out;
	}

	/* generic case */
	if (ARITHM_P(src)) {
		/* extract source operand */
		src = XEXP(src, 1);
		/* is this operand memory? */
		if (MEM_P(src)) {
			/* Yes, this is a memory read */
			goto out;
		}
	}
err:
	/* not arithmetic memory read */
	return FAIL;
out:
	/* identified memory read */
	return SUCC;
}

/*
 * extract the memory read of a cc read instruction
 *
 * returns:	the cc_read memory read
 */
static rtx extract_cc_read(rtx expr)
{
	return	XEXP(XEXP(XEXP(XVECEXP(expr, 0, 0), 1), 0), 1);
}

/*
 * identify cc read instructions.
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction. We also know that expr is PARALLEL.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a cc read instruction
 * 		FAIL otherwise
 */
static int is_cc_read(rtx expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined */
	rtx src;	/* always holds the source operand
			   of an RTL expression */
	rtx dst;	/* always holds the destination operand
			   of an RTL expression */

	/* ensure PARALLEL contains 2 sub-expressions */
	if (XVECLEN(expr, 0) != 2)
		goto err;

	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET((reg) (COMPARE((PLUS((reg) (MEM)))))) ||
	   SET((reg) (COMPARE((OR((reg) (MEM)))))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination operands */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination is REG */
	if (!REG_P(dst))
		goto err;
	/* ensure the source is COMPARE */
	if (!COMPARE_P(src))
		goto err;
	/* extract operand */
	dst = XEXP(src, 0);
	/* ensure the operand is PLUS or OR */
	if (ARITHM_P(dst)) {
		/* extract source */
		src = XEXP(dst, 1);
		/* ensure source is memory */
		if (MEM_P(src))
			return SUCC;
	}
err:
	return FAIL;
}

static rtx extract_arithmetic_mem_read(rtx expr)
{
	int i;		/* Iterator of PARALLEL RTL expressions */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/

	if (PARALLEL_P(expr)) {
		/* special case */
		if (is_cc_read(expr) == SUCC)
			/* identified */
			return extract_cc_read(expr);
		/* iterate the PARALLEL vector */
		for (i = 0; i < XVECLEN(expr, 0); i++) {
			/* extract sub-expression */
			sub_expr = XVECEXP(expr, 0, i);
			/* is it an arithmetic memory read? */
			if (__is_arithmetic_mem_read(sub_expr) == SUCC)
				/* Yes */
				return __extract_arithmetic_mem_read(sub_expr);
		}
	} else {
		/* is it an arithmetic memory read? */
		if (__is_arithmetic_mem_read(expr) == SUCC)
			/* Yes */
			return __extract_arithmetic_mem_read(sub_expr);
	}
	/* paranoia */
	return NULL;
}
/*
 * identify arithmetic memory reads.
 * Arithmetic instructions are:
 * PLUS, SUB, MUL, DIV, OR, XOR, AND
 *
 * We also identify here arithmetic instructions that are emitted
 * for setting the appropriate value of RFLAGS (typically `or' and `add'
 * instructions). We call these instances "cc reads".
 *
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents an arithmetic instruction
 * 		FAIL otherwise
 */
static int is_arithmetic_mem_read(rtx expr)
{
	int i;		/* Iterator of PARALLEL RTL expressions */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/

	if (PARALLEL_P(expr)) {
		/* special case */
		if (is_cc_read(expr) == SUCC)
			/* identified */
			goto out;
		/* iterate the PARALLEL vector */
		for (i = 0; i < XVECLEN(expr, 0); i++) {
			/* extract sub-expression */
			sub_expr = XVECEXP(expr, 0, i);
			/* is it an arithmetic memory read? */
			if (__is_arithmetic_mem_read(sub_expr) == SUCC)
				/* Yes */
				goto out;
		}
	} else {
		/* is it an arithmetic memory read? */
		if (__is_arithmetic_mem_read(expr) == SUCC)
			/* Yes */
			goto out;
	}
	/* not an arithmetic memory read */
	return FAIL;
out:
	/* identified arithmetic memory read */
	return SUCC;
}

/*
 * extract the memory read of the `leave' instruction
 *
 * returns:	the `leave' memory read
 */
static rtx extract_leave_mem_read(rtx expr)
{
	return XEXP(XVECEXP(expr, 0, 1), 1);
}

/*
 * identify `leave' instructions.
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a `leave' instruction
 * 		FAIL otherwise
 */
static int is_leave(rtx expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined */
	rtx src;	/* always holds the source operand
			   of an RTL expression */
	rtx dst;	/* always holds the destination operand
			   of an RTL expression */
	
	/* `leave' are always PARALLEL */
	if (!PARALLEL_P(expr))
		goto err;
	/* ensure PARALLEL contains 3 sub-expressions */
	if (XVECLEN(expr, 0) != 3)
		goto err;

	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET((sp) (PLUS((rbp) (8)))) */
	if (!SET_P(sub_expr) || !REG_P(XEXP(sub_expr, 0)))
		goto err;
	/* extract source and destination operand of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination is RSP */
	if (!REG_P(dst) || XINT(dst, 0) != RSP_REGNO)
		goto err;
	/* ensure source is a PLUS */
	if (!PLUS_P(src))
		goto err;
	/* extract source and destination operand of PLUS */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* ensure destination is RBP */
	if (!REG_P(dst) || XINT(dst, 0) != RBP_REGNO)
		goto err;
	/* ensure source is 0x8 */
	if (!CONSTANT_P(src) || XINT(src, 0) != 8)
		goto err;

	/* extract second sub-expression */
	sub_expr = XVECEXP(expr, 0, 1);
	/* SET((bp) MEM((bp))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination operand of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination is RBP */
	if (!REG_P(dst) || XINT(dst, 0) != RBP_REGNO)
		goto err;
	/* ensure source is MEM */
	if (!MEM_P(src))
		goto err;
	/* extract operand */
	dst = XEXP(src, 0);
	/* ensure operand is RBP */	
	if (!REG_P(dst) || XINT(dst, 0) != RBP_REGNO)
		goto err;

	/* extract third sub-expression */
	sub_expr = XVECEXP(expr, 0, 2);
	/* clobber MEM */
	if (!CLOBBER_P(sub_expr))
		goto err;
	/* extract operand */
	dst = XEXP(sub_expr, 0);
	/* ensure operand is MEM */
	if (!MEM_P(dst))
		goto err;
	/* finally... */
	return SUCC;
err:
	return FAIL;
}


/*
 * extract the memory read of the `movz' instruction
 *
 * returns:	the `movz' memory read
 */
static rtx extract_movz_mem_read(rtx expr)
{
	return XEXP(XEXP(XVECEXP(expr, 0, 0), 1), 0);
}

/*
 * identify `movz' instructions.
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a `movz' instruction
 * 		FAIL otherwise
 */
static int is_movz(rtx expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined */
	rtx src;	/* always holds the source operand
			   of an RTL expression*/
	rtx dst;	/* always holds the destination operand
			   of an RTL expression*/
	
	/* `movz' are always PARALLEL */
	if (!PARALLEL_P(expr))
		goto err;
	/* ensure PARALLEL contains 2 sub-expressions */
	if (XVECLEN(expr, 0) != 2)
		goto err;
	
	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET((reg) (AND(MEM(reg) (0xffffff)))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination operands */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination operand is a register */
	if (!REG_P(dst))
		goto err;
	/* ensure source operand is an AND expression */
	if (!AND_P(src))
		goto err;
	/* extract source and destination operands of AND expression */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* ensure destination operand is a memory expression */
	if (!MEM_P(dst))
		goto err;
	/* ensure source operand is 0xffffff*/
	if (!CONSTANT_P(src) 					||
			(XINT(src, 0) != 0xffff 		&&
			XINT(src, 0) != 0xff))
		goto err;
	/* extract second sub-expression */
	sub_expr = XVECEXP(expr, 0, 1);
	/* CLOBBER(FLAGS_REG) */
	if (!CLOBBER_P(sub_expr))
		goto err;
	/* extract operand */
	dst = XEXP(sub_expr, 0);
	/* ensure the operand is FLAGS_REG */
	if (REGNO(dst) != FLAGS_REG)
		goto err;

	/* finally... */
	return SUCC;
err:
	return FAIL;
}

/*
 * returns the next INSN instruction
 * This is used to find the instruction
 * that follows a `rep movs' instruction, because
 * we place the range check AFTER the `rep movs'
 * for postmortem detection.
 *
 * NOTE: Since this can't be the end of a BB, we don't
 * check for this case.
 *
 * insn:	the `rep movs' instruction
 * returns:	the first INSN instruction after insn
 */
static rtx get_next_insn(rtx insn)
{
	rtx next_insn;	/* the next instruction after insn */

	/* start from the RTL expression right after insn */
	next_insn = NEXT_INSN(insn);

	/* 
	 * move to the next instruction until we reach
	 * an INSN, CALL, RETURN etc 
	 */
	while (!INSN_CODE_P(next_insn)			&&
			!JUMP_P(next_insn)		&&
			!EH_RETURN_P(next_insn)		&&
			!ANY_RETURN_P(next_insn)	&&
			!CALL_P(next_insn))
		/* next RTL expression */
		next_insn = NEXT_INSN(next_insn);
	/* found it */
	return next_insn;
}

/*
 * extract the memory read of the `rep movs' instruction
 *
 * NOTE: this is always going to be a (%rsi).
 * We extract it from the `rep movs' expression to
 * obtain the correct size.
 *
 * returns:	the `rep movs' memory read
 */
static rtx extract_rep_movs_mem_read(rtx expr)
{
	return XEXP(XVECEXP(expr, 0, 3), 1);
}

/*
 * identify `rep movs' instructions.
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a `rep movs' instruction
 * 		FAIL otherwise
 */
static int is_rep_movs(rtx expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined */
	rtx src;	/* always holds the source operand
			   of an RTL expression*/
	rtx dst;	/* always holds the destination operand
			   of an RTL expression*/

	/* `rep movs' are always PARALLEL */
	if (!PARALLEL_P(expr))
		goto err;
	/* ensure PARALLEL contains 5 sub-expressions */
	if (XVECLEN(expr, 0) != 5)
		goto err;

	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET((cx) (0)) */
	if (!SET_P(sub_expr))
	       goto err;
	/* extract source and destination operand */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure that src = %.cx and dst = 0 */
	if (!REG_P(dst) 			||
			(REGNO(dst) != CX_REG)	||
			!CONSTANT_P(src)	||
			XINT(src, 0) != 0)
		goto err;

	/* extract second sub-expression */
	sub_expr = XVECEXP(expr, 0, 1);
	/*
	 * SET((di) (PLUS((di) (cx)))) ||
	 * SET((di) (PLUS((ASHIFT((cx) (1|2|3)) (di)))))
	 */
	if (!SET_P(sub_expr))
		goto err;       
	/* extract source and destination operand of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination operand is REG(di) */
	if (!REG_P(dst) || (REGNO(dst) != DI_REG))
		goto err;

	/* ensure source operand is PLUS */
	if (!PLUS_P(src))
		goto err;

	/* extract source and destination operand of PLUS */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* examine if source operand is ASHIFT */
	if (ASHIFT_P(dst)) {
		/* ensure source operand of PLUS is %.di */
		if (!REG_P(src) || (REGNO(src) != DI_REG))
			goto err;
		/* extract source and destination operand of ASHIFT */
		src = XEXP(dst, 1);
		dst = XEXP(dst, 0);
		/* ensure destination operand of ASHIFT is %.cx*/
		if (!REG_P(dst) || (REGNO(dst) != CX_REG))
			goto err;
		/* ensure source operand is (1|2|3) */
		if (!CONSTANT_P(src) ||
				(XINT(src, 0) != 1 && XINT(src, 0) != 2 &&
				 XINT(src, 0) != 3))
			goto err;
	} else {
		/* source operand is REG(cx) */
		if (!REG_P(src) || (REGNO(src) != CX_REG))
			goto err;
		/* ensure destination operand of PLUS is %.di */
		if (!REG_P(dst) || (REGNO(dst) != DI_REG))
			goto err;
	}

	/* extract third sub-expression */
	sub_expr = XVECEXP(expr, 0, 2);
	/*
	 * SET((si) (PLUS((si) (cx)))) ||
	 * SET((si) (PLUS((ASHIFT((cx) (1|2|3)) (si)))))
	 */
	if (!SET_P(sub_expr))
		goto err;       
	/* extract source and destination operand of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination operand is REG(di) */
	if (!REG_P(dst) || (REGNO(dst) != SI_REG))
		goto err;
	/* ensure source operand is PLUS */
	if (!PLUS_P(src))
		goto err;
	/* extract source and destination operand of PLUS */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* examine if source operand is ASHIFT */
	if (ASHIFT_P(dst)) {
		/* ensure source operand of PLUS is %.di */
		if (!REG_P(src) || (REGNO(src) != SI_REG))
			goto err;
		/* extract source and destination operand of ASHIFT */
		src = XEXP(dst, 1);
		dst = XEXP(dst, 0);
		/* ensure destination operand of ASHIFT is %.cx*/
		if (!REG_P(dst) || (REGNO(dst) != CX_REG))
			goto err;
		/* ensure source operand is (1|2|3) */
		if (!CONSTANT_P(src) ||
				(XINT(src, 0) != 1 && XINT(src, 0) != 2 &&
				 XINT(src, 0) != 3))
			goto err;
	} else {
		/* source operand is REG(cx) */
		if (!REG_P(src) || (REGNO(src) != CX_REG))
			goto err;
		/* ensure destination operand of PLUS is %.di */
		if (!REG_P(dst) || (REGNO(dst) != SI_REG))
			goto err;
	}

	/* extract fourth sub-expression */
	sub_expr = XVECEXP(expr, 0, 3);
	/* SET((MEM(di)) (MEM(si))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination operand of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure both operands are MEM */
	if (!MEM_P(dst) || !MEM_P(src))
		goto err;
	/* extract source and destination registers */
	dst = XEXP(dst, 0);
	src = XEXP(src, 0);
	/* ensure destination register is %.di */
	if (REGNO(dst) != DI_REG)
		goto err;
	/* ensure source register is %.si */
	if (REGNO(src) != SI_REG)
		goto err;

	/* extract fifth sub-expression */
	sub_expr = XVECEXP(expr, 0, 4);
	/* USE(cx) */
	if (!USE_P(sub_expr))
	       goto err;
	/* extract register */
	dst = XEXP(sub_expr, 0);
	/* ensure the register is %.cx */
	if (!REG_P(dst) || (REGNO(dst) != CX_REG))
		goto err;

	/* finally */
	return SUCC;
err:
	return FAIL;
}

/*
 * extract the memory read of the `movs' instruction
 *
 * NOTE: this is always going to be a (%rsi).
 * We extract it from the `movs' expression to
 * obtain the correct size.
 *
 * returns:	the `movs' memory read
 */
static rtx extract_movs_mem_read(rtx expr)
{
	return XEXP(XVECEXP(expr, 0, 0), 1);
}

/*
 * identify `movs' instructions.
 * NOTE: We assume that we have extracted the PATTERN 
 * from the instruction.
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if the expression represents a `movs' instruction
 * 		FAIL otherwise
 */
static int is_movs(rtx expr)
{
	rtx sub_expr;	/* holds sub-expression to be examined */
	rtx src;	/* always holds the source operand
			   of an RTL expression*/
	rtx dst;	/* always holds the destination operand
			   of an RTL expression*/

	/* `movs' are always PARALLEL */
	if (!PARALLEL_P(expr))
		goto err;
	/* ensure PARALLEL contains 3 sub-expressions */
	if (XVECLEN(expr, 0) != 3)
		goto err;

	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET ((mem(di)) (mem(si))) */
	if (!SET_P(sub_expr) ||
	    !MEM_P(XEXP(sub_expr, 0)) ||
	    !MEM_P(XEXP(sub_expr, 1)))
		goto err;
	/* extract source and destination registers */
	src = XEXP(XEXP(sub_expr, 1), 0);
	dst = XEXP(XEXP(sub_expr, 0), 0);
	/* ensure source = %.si and destination = %.di */
	if (!REG_P(src) || !REG_P(dst) ||
			(REGNO(src) != SI_REG) ||
			(REGNO(dst) != DI_REG))
		goto err;

	/* extract second sub-expression */
	sub_expr = XVECEXP(expr, 0, 1);
	/* SET((di) PLUS((di)(1|2|4|8))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination of SET expression */
	src = XEXP(sub_expr, 1);
	dst = XEXP(sub_expr, 0);
	/* ensure destination is %.di */
	if (!REG_P(dst) || (REGNO(dst) != DI_REG))
		goto err;
	/* ensure src is a PLUS expression */
	if (!PLUS_P(src))
		goto err;
	/* extract source and destination of the PLUS expression */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* ensure destination is %.di and source is a (1|2|4|8) */
	if (!REG_P(dst) ||
			(REGNO(dst) != DI_REG) ||
			!CONSTANT_P(src) ||
			(XINT(src, 0) != 1 && XINT(src, 0) != 2 &&
			XINT(src, 0) != 4 && XINT(src, 0) != 8))
		goto err;
	/* extract third sub-expression */
	sub_expr = XVECEXP(expr, 0, 2);
	/* SET((si) PLUS((si)(1|2|4|8))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination of SET expression */
	src = XEXP(sub_expr, 1);
	dst = XEXP(sub_expr, 0);
	/* ensure destination is %.si */
	if (!REG_P(dst) || (REGNO(dst) != SI_REG))
		goto err;
	/* ensure src is a PLUS expression */
	if (!PLUS_P(src))
		goto err;
	/* extract source and destination of the PLUS expression */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* ensure destination is %.di and source is a (1|2|4|8) */
	if (!REG_P(dst) ||
			(REGNO(dst) != SI_REG) ||
			!CONSTANT_P(src) ||
			(XINT(src, 0) != 1 && XINT(src, 0) != 2 &&
			XINT(src, 0) != 4 && XINT(src, 0) != 8))
		goto err;

	/* At last... */
	return SUCC;
err:
	return FAIL;
}

/*
 * identifies safe memory reads that should not be instrumented.
 * Specifically:
 * a) pop instructions (%rsp-based)
 * b) generic %rsp-based instructions
 * b) symbol reads (%rip-based)
 * c) constant reads
 *
 * mem_expr:	the RTL expression of the memory read
 * addr_mode:	the addressing mode of the RTL expression
 *
 * returns:	SUCC if mem_expr is a safe read operation
 * 		FAIL otherwise
 */
static int is_safe_read(rtx mem_expr, int addr_mode)
{
	rtx sub_expr;			/* the sub-expression of the
					   memory operation */
	rtx reg;			/* the base register of the
					   memory read */
	enum rtx_class __class;	/* the class of the sub-expression */

	long long tmp;

	/* extract sub-expression */
	sub_expr = XEXP(mem_expr, 0);
	/* extract class of sub-expression */
	__class = GET_RTX_CLASS(GET_CODE(sub_expr));
	/* pop instructions (%rsp-based) */
	if (__class == RTX_AUTOINC			||
			/* symbols (%rip-based) */
			SYMBOL_P(sub_expr)		||
			/* constants */
			__class == RTX_CONST_OBJ)
		/* safe read */
		return SUCC;
	/* generic %rsp-based memory read */
	if (addr_mode == MEM_BASE && REGNO(sub_expr) == RSP_REGNO)
		/* safe read */
		return SUCC;
	if (addr_mode == MEM_OFF) {
		tmp = INTVAL(XEXP(sub_expr, 1));
		/* extract register */
		reg = XEXP(sub_expr, 0);
		if (REGNO(reg) == RSP_REGNO) {
			/* safe read */
			return SUCC;
		}
	}
	/* not a safe read */
	return FAIL;
}

/*
 * extract the appropriate information depending on the addressing mode
 *
 * mem_expr: 	the RTL expression that represents the memory read
 * addr_mode:	the addressing mode of the memory read
 * base:	will hold the base register of the operation
 * index:	will hold the index register of the operation (if applicable)
 * scale:	will hold the scale factor of the operation (if applicable)
 * offset:	will hold the offset of the operation (if applicable)
 * lea:		will hold the expression that represents
 * 		the effective address (if applicable)
 */
static void
extract_info(rtx mem_expr, int addr_mode, rtx *base, rtx *index,
				long long *scale, long long *offset,
			       	rtx *symbol)
{
	/* MEM_BASE [(%base)] */
	if (addr_mode == MEM_BASE)
		/* only %base */
		*base = XEXP(mem_expr, 0);
	/* MEM_OFF [offset(%base)] */
	else if (addr_mode == MEM_OFF) { 
		/* %base */
		*base = XEXP(XEXP(mem_expr, 0), 0);
		/* offset */
		*offset = INTVAL(XEXP(XEXP(mem_expr, 0), 1));
	} else {
		/* extract all other info [offset(%base, %index, scale)] */
		extract_mem_rest_info(mem_expr, base, index,
					scale, offset, symbol);
	}
}

static check_entry *
prepare_check_entry(rtx mem_expr, int addr_mode, int clobber_flags,
					rtx instr_insn, rtx curr_insn)
{
	check_entry *entry;		/* holds the range check info */
	rtx base = NULL;		/* holds the base register 
					   of the memory read */
	rtx index = NULL;		/* holds the index register
					   of the memory read (if applicable) */
	long long scale;		/* holds the scale factor
					   of the memory read (if applicable) */
	long long offset;		/* holds the offset
					   of the memory read (if applicable) */
	rtx symbol = NULL;		/* holds the symbol
					   of the memory read (if applicable) */

	/* allocate space for range check */
	entry = (check_entry *)xmalloc(sizeof(check_entry));

	/* extract the info from the memory read expression */
	extract_info(mem_expr, addr_mode, &base, &index, &scale,
							&offset, &symbol);
#ifdef	DEBUG
	commitlog(mem_expr, "Found memory move");
#endif
	/* store know information */
	entry->insn = instr_insn;
	entry->real_insn = curr_insn;
	entry->addr_mode = addr_mode;
	entry->base = base;
	/* store lea expression */
	entry->lea = copy_rtx(mem_expr);
	/* 
	 * if it's an arithmetic memory read,
	 * we don't need to spill the RFLAGS (-O1)
	 */
	if (level >= O1 && clobber_flags) {
		entry->flags = false;
	} else {
		entry->flags = true;
	}
	/* used for the check elimination optimization (-O3) */
	entry->eliminated = false;

	/* offset */
	entry->offset = 0;
	/* index */
	entry->index = NULL;
	/* scale */
	entry->scale = 0;
	/* symbol (if any) */
	entry->symbol = NULL;
	/* save addressing mode specific info */
	if (addr_mode == MEM_OFF)
		/* offset */
		entry->offset = offset;
	else if (addr_mode == MEM_REST) {
		/* offset */
		entry->offset = offset;
		/* index */
		entry->index = index;
		/* scale */
		entry->scale = scale;
		/* symbol (if any) */
		entry->symbol = symbol;
	}
	/* return the pointer */
	return entry;
}
/*
 * Iterates on all the instructions of the current function
 * and identifies memory reads that should be instrumented.
 * These cases are added to checks.
 *
 * checks:	vector that contains the memory reads that
 * 		should be instrumented.
 */
static void locate_mem_reads(std::vector<check_entry *> *checks)
{
	basic_block bb_it;	/* basic block iterator */
	rtx insn_it;		/* instruction iterator */
	rtx insn; 		/* the instruction before
				   which we place the range check */
	rtx mem_expr;		/* holds the RTL expression
				   that corresponds to the memory read */
	rtx expr;		/* the RTL expression of each instruction
				   in the code stream */
	int clobber_flags;	/* if 1 the operation clobbers the flags
				   thus we should not spill the RFLAGS
				   register (-O1) */
	int addr_mode;		/* holds the addressing mode of the
				   memory read */
	check_entry *entry;	/* holds the range check entry that 
				   contains the necessary info for
				   the instrumentation */
	/* traverse all the instructions of the function */
	INSN_ITER_BB(bb_it, insn_it) {
#ifdef	DEBUG
		commitlog(insn_it, "-----------------------");
#endif
		/* memory read instructions have INSN code */
		if (!INSN_CODE_P(insn_it))
			/* next instruction */
			continue;
		/* initialize memory expression */
		mem_expr = NULL;
		/* by default the instruction is not modifying RFLAGS */
		clobber_flags = 0;
		/* by default we add the instrumentation before
		   the current instruction */
		insn = insn_it;
		/* extract the RTL expression of the instruction */
		expr = PATTERN(insn_it);

		/* movs */
		if (is_movs(expr) == SUCC) {
			/* extract memory read */
			mem_expr = extract_movs_mem_read(expr);
		}
		/* rep movs */
		else if (is_rep_movs(expr) == SUCC) {
			/* extract memory read */
			mem_expr = extract_rep_movs_mem_read(expr);
			/* instrument AFTER rep movs (postmortem detection) */
			insn = get_next_insn(insn_it);
		/* movz */
		} else if (is_movz(expr) == SUCC) {
			/* extract memory read */
			mem_expr = extract_movz_mem_read(expr);
		}
		/* leave */
		else if (is_leave(expr) == SUCC) {
			/* extract memory read */
			mem_expr = extract_leave_mem_read(expr);
		}
		/* arithmetic instructions
		   (`add', `sub', `xor', `'mul, `div' etc) */
		else if (is_arithmetic_mem_read(expr) == SUCC) {
			/* mark it as an arithmetic memory read */
			clobber_flags = 1;
			/* extract memory read */
			mem_expr = extract_arithmetic_mem_read(expr);
		}
		/* simple memory reads (`mov' etc) */
		else if (is_mem_read(expr) == SUCC)
			/* 
			 * extract memory read 
			 * NOTE: we pass a reference to insn
			 * because occasionally the memory read is
			 * inside a PARALLEL. In this case
			 * we place the range check there to thightly
			 * couple the check with the memory read.
			 */
			mem_expr = extract_mem_read(expr, &insn);
		else
			/* not a memory read */
			continue;

		/* extract the addressing mode of the memory read */
		addr_mode = get_mem_read_mode(mem_expr);

		/* eliminate safe reads */
		if (is_safe_read(mem_expr, addr_mode) == SUCC)
			/* should not instrument */
			continue;

		/* this is a memory read that should be instrumented */
		entry = prepare_check_entry(mem_expr, addr_mode,
					clobber_flags, insn, insn_it);
		/* add it to the vector */
		checks->push_back(entry);
	}
}

/*
 * emits a `popf' instruction
 *
 * insn:	the instruction before which
 * 		we will emit the `popf'
 */
static void emit_popf(rtx insn)
{
	rtx popf;		/* the popf expression */
	int insn_line;		/* the line of the instruction;
				   debugging info */

	/* the popf will have the same line as insn */
	insn_line = expand_location(RTL_LOCATION(insn)).line;
	/* generate the popf expression */
	popf = gen_rtx_ASM_OPERANDS(VOIDmode,
					"popfq",
					"",
					0,
					rtvec_alloc(0),
					rtvec_alloc(0),
					rtvec_alloc(0),
					insn_line);
	/* emit `popf' */
	emit_insn_before(popf, insn);
}

/*
 * emits a `pushf' instruction
 *
 * insn:	the instruction before which
 * 		we will emit the `pushf'
 */
static void emit_pushf(rtx insn)
{
	rtx pushf;		/* the pushf expression */
	int insn_line;		/* the line of the instruction;
				   debugging info */

	/* the pushf will have the same line as insn */
	insn_line = expand_location(RTL_LOCATION(insn)).line;
	/* generate the pushf expression */
	pushf = gen_rtx_ASM_OPERANDS(VOIDmode,
					"pushfq",
					"",
					0,
					rtvec_alloc(0),
					rtvec_alloc(0),
					rtvec_alloc(0),
					insn_line);
	/* emit `pushf' */
	emit_insn_before(pushf, insn);
}

/*
 * emits a `lea' instruction before insn
 *
 * insn:	the memory read expression to be instrumented
 * lea:		the expression of which we want to obtain the
 * 		effective address. 
 *
 * returns:	the reserved register that will hold the
 * 		effective address (%r10)
 */
rtx emit_lea(rtx insn, rtx lea)
{
	rtx reg;		/* the reserved register (%r10) */
	rtx lea_insn;		/* the lea expression 		*/

	/* generate the reserved register */
	reg = gen_rtx_REG(DImode, RES_REGNO);
	/* generate the `lea' expression */
	lea_insn = gen_rtx_SET(DImode, reg, XEXP(lea, 0));
	/* emit the instruction */
	emit_insn_before(lea_insn, insn);
	/* return the reserved register */
	return reg;
}

/*
 * emits the MPX range check before insn.
 * NOTE: in this version of GCC its not possible to produce MPX instructions
 * through RTL expressions. Therefore, instead of emitting a single `bndcu'
 * instruction, we also emit a `lea' instruction and use `%r10' in inline
 * assembly `bndcu'.
 *
 * insn:		the memory read expression to be instrumented
 * lea:			the expression to be used at the `lea' instruction
 */
static void emit_mpx_check(rtx insn, rtx lea)
{
	rtx bndcu;		/* the bndcu expression */
	int insn_line;		/* the line of the instruction;
				   debugging info */
	/* emit `lea' instruction */
	emit_lea(insn, lea);

	/* `bndcu' will have the same line as insn */
	insn_line = expand_location(RTL_LOCATION(insn)).line;
	/* generate the `bndcu' expression */
	bndcu = gen_rtx_ASM_OPERANDS(VOIDmode,
					"bndcu %%r10,%%bnd0",
					"",
					0,
					rtvec_alloc(0),
					rtvec_alloc(0),
					rtvec_alloc(0),
					insn_line);

	/* emit the `bndcu' instruction */
	emit_insn_before(bndcu, insn);
}

/*
 * emits the range check before insn.
 * if the addressing mode is MEM_REST (or we operate at < -O2),
 * we emit a `lea' instruction before the `cmp'/`ja' sequence
 *
 *
 * insn:		the memory read expression to be instrumented
 * addr_mode:		the addressing mode of the memory read
 * reg:			the base register of the memory read
 * lea:			the expression to be used at the `lea' instruction
 * 			(MEM_REST, < -O2)
 * offset:		the offset of the memory read
 * label:		the target label of the `ja' instruction (points to
 * 			the violation handler)
 */
static void
emit_sfi_check(rtx insn, int addr_mode, rtx reg, rtx lea,
						long long offset, rtx label)
{
	basic_block from;		/* basic block that contains the code
					   before the insn 		     */
	basic_block to;			/* basic block that contains the
				   	   instrumetation 		     */
	rtx flags_reg;			/* condition code register 	     */
	rtx ja;				/* conditional jump expression 	     */
	rtx cond;			/* the condition of the `cmp'
				   	   instruction 			     */
	rtx cmp;			/* the `cmp' expression		     */
	enum machine_mode cmpmode;	/* the comparison mode used in `cmp' */
	rtx limit_rtx;			/* holds thr RTL expression that
					   represents the limit of the
					   effective address
					   (i.e. __krx_edata)		     */
	unsigned long check_limit;	/* the limit of the check 	     */

	check_limit = limit;
	if (level < O2 || addr_mode == MEM_REST) {
		reg = emit_lea(insn, lea);
		/* create the RTL expression for the limit */
		limit_rtx = GEN_INT(check_limit);
	}
	/* -O2/-O3 */
	else {
		if (addr_mode == MEM_OFF) {
			/* create the RTL expression for the limit */
			check_limit -= offset;
		}
		limit_rtx = GEN_INT(check_limit);
	}
	/* split basic block */
	from = split_block(BLOCK_FOR_INSN(insn), insn)->src;
	to = single_succ(from);

	/* 
	 * bookkeeping; ensure that the new basic block is correctly
	 * connected with its predecessor
	 */
	redirect_edge_and_branch_force(single_succ_edge(from), to);
	if (forwarder_block_p(from))
		from->flags |= BB_FORWARDER_BLOCK;
	else
		from->flags &= ~BB_FORWARDER_BLOCK;

	/* extract comparison mode */
	cmpmode = SELECT_CC_MODE(GTU, reg, limit_rtx);
	/* generate the RFLAGS register */
	flags_reg = gen_rtx_REG(cmpmode, FLAGS_REG);
	
	/* generate `cmp' expression */
	cmp = gen_rtx_SET(GET_MODE(flags_reg), flags_reg,
				gen_rtx_COMPARE(cmpmode, reg, limit_rtx));
	/* emit cmp instruction */
	emit_insn_before(cmp, insn);
	
	/* generate the condition of the `ja' */
	cond = gen_rtx_IF_THEN_ELSE(VOIDmode, 
					gen_rtx_GTU(VOIDmode, flags_reg,
								const0_rtx),
					gen_rtx_LABEL_REF(VOIDmode, label),
					pc_rtx);
	/* generate the `ja' expression */
	ja = emit_jump_insn_before(gen_rtx_SET(VOIDmode, pc_rtx, cond),	insn);
	/* set the label as the target of the conditional jump */
	JUMP_LABEL(ja) = label;
	/* increase the number of uses of the label */
	LABEL_NUSES(label)++;
}

/*
 * frees the malloc()'d space for the check entries
 *
 * checks:	the vector that holds the range check entries
 *
 */
static void free_checks(std::vector<check_entry *> *checks)
{
	int i;			/* counter to iterate the vector*/
	check_entry *curr;	/* will hold the current entry */
	
	/* iterate vector */
	for (i = 0; i < checks->size(); i++) {
		/* get the pointer */
		curr = checks->at(i);
		/* free space */
		free(curr);
	}
}

/*
 * emits the actual instrumentation for all the check entries
 * in the checks vector.
 *
 *
 * checks: 	The vector that holds all the memory
 *		reads that should be instrumented.
 */
static void emit_checks(std::vector<check_entry *> *checks)
{
	int i;			/* iterator */
	check_entry *curr;	/* the check to be emitted */
	basic_block new_bb;	/* the basic block that holds the
				   call to the violation handler */
	rtx call_stub;		/* call to violation handler */
	rtx vsaddr;		/* violation handler */
	rtx ret;		/* return instruction after
				   violation handler call */
	rtx label;		/* the target label (beginning
				   of new basic block) */

	/* do we have any checks to emit? */
	if (checks->size() == 0)
		/* No */
		return;
	/* no need to add a call to stub if MPX is enabled */
	if (mpx == DISABLE) {
		/* run-time violation handler; stub */
		vsaddr = gen_rtx_SYMBOL_REF(Pmode, stub);
		SYMBOL_REF_FLAGS(vsaddr) |=
				(SYMBOL_FLAG_FUNCTION | SYMBOL_FLAG_EXTERNAL);
		/* emit return instruction */
		call_stub = emit_insn(gen_rtx_CALL(Pmode,
							gen_rtx_MEM(QImode, vsaddr),
							const0_rtx));

		/* create new basic block with the call
		   at the end of the function */
		new_bb = create_basic_block(call_stub, NULL, EXIT_BLOCK_PTR->prev_bb);
		/* emit the target label (beginning of new basic block) */
		label = block_label(new_bb);
	}
	/* iterate checks */
	for (i = 0; i < checks->size(); i++) {
		/* get current check */
		curr = checks->at(i);

		/* is this check eliminated? */
		if (curr->eliminated)
			/* next check */
			continue;
		/* do we have to spill RFLAGS? */
		if (curr->flags && mpx == DISABLE)
			/* emit pushf */
			emit_pushf(curr->insn);
		if (mpx == DISABLE)
			/* emit SFI instrumentation */
			emit_sfi_check(curr->insn, curr->addr_mode, curr->base,
					curr->lea, curr->offset, label);
		else
			/* emit MPX instrumentation */
			emit_mpx_check(curr->insn, curr->lea);
		/* do we have to fill RFLAGS? */
		if (curr->flags && mpx == DISABLE)
			/* emit popf */
			emit_popf(curr->insn);
	}
}

/*
 * updates (keeps max) the offset of checks->at(pred) and checks->at(elim)
 *
 * checks:		the vector that holds the range check entries
 * pred:		the index of the predecessor check
 * 			(this check will survive)
 * elim:		the index of the check to be eliminated
 */
static void
update_offset(std::vector<check_entry *> *checks, unsigned pred, unsigned elim)
{
	/* MEM_BASE and MEM_OFF */
	if (checks->at(pred)->addr_mode != MEM_REST) {
		/* does elim have greater offset? */
		if (checks->at(pred)->offset < checks->at(elim)->offset)
			/* keep maximum offset */
			checks->at(pred)->offset = checks->at(elim)->offset;
		/* update addressing mode if necessary */
		if (checks->at(pred)->offset != 0)
			checks->at(pred)->addr_mode = MEM_OFF;
	/* MEM_REST */
	} else {
		/* does elim have greater offset? */
		if (checks->at(pred)->offset < checks->at(elim)->offset) {
			/* update lea */
			checks->at(pred)->lea = checks->at(elim)->lea;
			/* update offset */
			checks->at(pred)->offset = checks->at(elim)->offset;
		}
	}
}


/*
 * updates (keeps max) the offset of all the checks in the same_lea vector.
 *
 * same_lea holds the checks that share the same (%base, %index, scale)
 * tuple as the check that will be eliminated (i.e. checks->at(id)).
 *
 * checks:		the vector that holds the range check entries
 * id:			the index of the check to be eliminated
 */
static void
update_offsets(std::vector<check_entry *> *checks, unsigned id)
{
	unsigned i;	/* iterator*/

	/* traverse all entries in same_lea */
	for (i = 0; i < same_lea.size(); i++)
		/* update the offset of the check */
		update_offset(checks, same_lea[i], id);
}


/*
 * examines if two (MEM_REST) checks can be coalesced.
 * To do so, they need to be exactly the same, except for the offset.
 * Specifically they should share the same (%base, %index, scale) tuple.
 *
 * NOTE: We call this function if at least one of these checks is MEM_REST.
 * Internally we verify that *both* need to be of the same addressing mode.
 *
 * check1, check2:		the two check to be compared
 *
 * returns:		SUCC if the two checks can be coalesced
 * 			FAIL otherwise
 */
static int can_eliminate_rest(check_entry *check1, check_entry *check2)
{
	check_entry *base_index;
	check_entry *no_base;

	/* ensure both checks are MEM_REST */
	if (check1->addr_mode != MEM_REST ||
				check2->addr_mode != MEM_REST)
		/* can't be coalesced */
		return FAIL;

	/* handle corner-cases (due to element reordering) first */
	/* (index, index, 1) == (,index, 2) */
	if (check1->scale != check2->scale) {
		if (check1->scale > 2 || check2->scale > 2)
			/* can't be coalesced */
			return FAIL;
		/* scale can only be 1, 2, 4, 8 */
		if (check1->scale == 1) {
			/* check2 should not have base */
			if (check2->base != NULL)
				/* can't be coalesced */
				return FAIL;
			/* the base and index of check1
			   should be the same */
			if (!rtx_equal_p(check1->base, check1->index))
				/* can't be coalesced */
				return FAIL;
			/* the index of check1 and check2
			   should be the same */
			if (!rtx_equal_p(check1->index, check2->index))
				/* can't be coalesced */
				return FAIL;
		} else {
			/* check1 should not have base */
			if (check1->base != NULL)
				/* can't be coalesced */
				return FAIL;
			/* the base and index of check2
			   should be the same */
			if (!rtx_equal_p(check2->base, check2->index))
				/* can't be coalesced */
				return FAIL;
			/* the index of check1 and check2
			   should be the same */
			if (!rtx_equal_p(check1->index, check2->index))
				/* can't be coalesced */
				return FAIL;
		}
		/* can be coalesced */
		return SUCC;
	/* (base, index, 1) == (index, base, 1) */
	} else if (!rtx_equal_p(check1->base, check2->base)) {
		/* verify that scale is 1 for both */
		if (check1->scale != 1 || check2->scale != 1)
			/* can't be coalesced */
			return FAIL;
		/* the base of check1 should be the same as
		   the index of check2 and vice versa */
		if (!rtx_equal_p(check1->base, check2->index) ||
				!rtx_equal_p(check1->index, check2->base))
			/* can't be coalesced */
			return FAIL;
		/* can be coalesced */
		return SUCC;
	}
	/* check for the same index and same symbol (no corner-case) 
	   due to the checks above, we know that they have the same
	   base and scale */
	/* index */
	if (!rtx_equal_p(check1->index, check2->index))
		/* can't be coalesced */
		return FAIL;
	/* symbol */
	if (!rtx_equal_p(check1->symbol, check2->symbol))
		/* can't be coalesced */
		return FAIL;
	/* can be coalesced */
	return SUCC;
}

/*
 * examines if a check (specified by checks->at(i)) should be eliminated (-O3)
 *
 * How do we eliminate a check:
 * Starting from the instruction to be instrumented, we examine the
 * instructions upwards until the end/start of the bb.
 * 1) If we find an instruction that modifies any of the registers
 * used by the check (%base, %index), then we must retain the check.
 * 2) If we find an instruction that is about to be instrumented, which uses
 * the same set of registers and scale - i.e. that check and the check we
 * want to eliminate share the same (%base, %index, scale) tuple - we
 * update that check (i.e. keep the maximum offset among those two).
 * We then eliminate the examined check.
 * 3) If we don't find any instruction that falls in these categories,
 * we (recursively) check the predecessors.
 *
 * checks:		the vector that holds the range check entries
 * i:			the index of the currently examined check
 * insn_indices:	the map that contains the mapping between
 * 			instructions and checks index
 * bb:			the currently examined basic block
 * predecessor:		if 1, we examine a predecessor of the basic block
 * 			of the currently examined range check
 * 			if 0, we are in the same basic block of the
 * 			currently examined range check for the *first*
 * 			time
 *
 * returns:		SUCC if we should eliminate the range check
 * 			FAIL otherwise
 */
static int eliminate_check(std::vector<check_entry *> *checks, unsigned i,
					std::map<rtx, unsigned> *insn_indices,
					basic_block bb, int predecessor)
{
	rtx insn_it;
	rtx start_insn;
	check_entry *curr_check, *it_check;
	std::map<rtx, unsigned>::iterator it;
	edge e;
	edge_iterator ei;

	/* have we visited this basic block before? */
	if (visited.find(bb) != visited.end())
		/* loop detected */
		return SUCC;

	/* get the check under examination */
	curr_check = checks->at(i);
	/* is this a predecessor? */
	if (predecessor) {
		/* start examining from the end of the block */
		start_insn = BB_END(bb);
		/* add the basic block to the list of visited blocks */
		visited.insert(bb);
	} else
		/* 
		 * We're in the original basic block. 
		 * Start from the instruction to be instrumented */
		start_insn = curr_check->real_insn;
	
	/* intra-basic block: iterate the instructions in reverse order */
	BB_ITER_START_REVERSE(insn_it, start_insn) {
		/* ignore anything that's not an instruction */
		if (!INSN_P(insn_it))
			continue;

		/* does this check use a %base? */
		if (curr_check->base != NULL) {
			/* Yes, check if this instruction modifies it */
			if (reg_set_p(curr_check->base, insn_it))
				/* %base modified. Keep check */
				return FAIL;
		}
		/* does this check use a %index */
		if (curr_check->addr_mode == MEM_REST	&&
				curr_check->index != NULL) {
			/* Yes, check if this instruction modifies it */
			if (reg_set_p(curr_check->index, insn_it))
				/* %index modified. Keep check */
				return FAIL;
		}
		/* get the mapping of this instruction */
		it = insn_indices->find(insn_it);
		/* is this instruction marked for instrumentation? */
		if (it != insn_indices->end()) {
			/* reached the same instruction? */
			if (i == it->second)
				/* Don't add to same_lea */
				return SUCC;
			/* extract the check of the
			   iterated instruction */
			it_check = checks->at(it->second);
			/* do curr_check and it_check match? */
			/* MEM_REST */
			if (curr_check->addr_mode == MEM_REST ||
					it_check->addr_mode == MEM_REST) {
				/* can we eliminate curr_check? */
				if (can_eliminate_rest(curr_check, it_check)
					       				== FAIL)
					/* No. Next instruction */
					continue;
			/* MEM_BASE and MEM_OFF */
			} else {
				/* 
				 * They must have the same base.
				 * Other than that, anything goes.
				 */
				if (!rtx_equal_p(curr_check->base,
								it_check->base))
					/* Next instruction */
					continue;
			}
			/* 
			 * They match.
			 * Add new_check in same_lea and return SUCC
			 */
			same_lea.push_back(it->second);
			return SUCC;
		}
	}
	/*
	 * Reached the head of the bb and there's no
	 * suitable check or instruction that sets the
	 * register(s).
	 *
	 * Inter-basic block: Do DFS in bb predecessors.
	 * If *all* of them have a suitable check,
	 * then we can update the offset of *all* of them and
	 * remove this check since it's protected by them.
	 */
	FOR_EACH_EDGE(e, ei, bb->preds) {
		/* first bb. Probably register(s) are arguments */
		if (e->src == ENTRY_BLOCK_PTR)
			/* can't eliminate */
			return FAIL;
		/* recursively check predecessor */
		if (!eliminate_check(checks, i, insn_indices, e->src, 1)
									== SUCC)
			/* can't eliminate */
			return FAIL;
	}
	/* *every* predecessor returned SUCC */
	return SUCC;
}

/*
 * identifies which range checks should be eliminated.
 *
 * checks:		the vector that holds all the range
 * 			checks entries
 * insn_indices:	the map that contains the mapping between
 * 			instructions and checks index
 */
static void eliminate_checks(std::vector<check_entry *> *checks,
				std::map<rtx, unsigned> *insn_indices)
{
	unsigned i;		/* index */
	basic_block bb;		/* holds the basic block of the 
				   instruction that we currently examine */

	/* iterate all range checks */
	for (i = 0; i < checks->size(); i++) {
		/* clean-up the set of
		   visited basic blocks */
		visited.clear();
		/* clean-up the vector of
		   checks that have the same
		   (%base, %index, scale) */
		same_lea.clear();
		/* get the basic block of the
		   instruction to be instrumented */
		bb = BLOCK_FOR_INSN(checks->at(i)->real_insn);
		/* can we eliminate this check? */
		if (eliminate_check(checks, i, insn_indices, bb, 0) == SUCC) {
			/* update offset (keep maximum) */
			update_offsets(checks, i);
			/* mark the check as eliminated */
			checks->at(i)->eliminated = true;
			/* erase entry from the map */
			insn_indices->erase(insn_indices->find(
						checks->at(i)->real_insn));
		}
	}
}

/*
 * creates a mapping between instructions and check entries.
 * Specifically, we associate the index of the vector that
 * holds the range checks information with the rtl expression
 * of the instruction to be instrumented.
 *
 * NOTE: this step facilitates the check elimination (-O3)
 * optimization.  
 * 
 * checks:		the vector that holds all the range
 * 			checks entries
 * insn_indices:	the map to be filled
 */
static void fill_insn_indices(std::vector<check_entry *> *checks,
				std::map<rtx, unsigned> *insn_indices)
{
	unsigned i;			/* index */
	check_entry *check;		/* the range check information entry */

	/* iterate the vector */
	for (i = 0; i < checks->size(); i++) {
		/* get current range check entry */
		check = checks->at(i);
		/* create mapping */
		insn_indices->insert(std::pair<rtx, unsigned>(check->real_insn, i));
	}
}


/*
 * examine if an expression is a convert instruction
 *
 * NOTE: Such instructions do not affect the RFLAGS
 * value even though they contain CLOBBER(flags) expressions
 * (brainf*ck);
 *
 * expr:	the expr to examine
 * returns:	SUCC if it is a convert instruction
 * 		FAIL otherwise
 */
static int is_cltd(rtx expr)
{
	unsigned i;	/* iterator 				*/
	rtx src;	/* source operand 			*/
	rtx dst;	/* destination operand 			*/
	rtx sub_expr;	/* holds sub-expression to be examined 	*/

	/* they are always PARALLEL */
	if (!PARALLEL_P(expr))
		goto err;
	/* ensure PARALLEL contains 3 sub-expressions */
	if (XVECLEN(expr, 0) != 2)
		goto err;

	/* extract first sub-expression */
	sub_expr = XVECEXP(expr, 0, 0);
	/* SET((reg) (ASHIFT((reg) (constant)))) */
	if (!SET_P(sub_expr))
		goto err;
	/* extract source and destination operands of SET */
	dst = XEXP(sub_expr, 0);
	src = XEXP(sub_expr, 1);
	/* ensure destination is a register */
	if (!REG_P(dst))
		goto err;
	/* ensure source is ASHIFT */
	if (!ASHIFT_P(src))
		goto err;
	/* extract source and destination operands of ASHIFT */
	dst = XEXP(src, 0);
	src = XEXP(src, 1);
	/* ensure destination is register and source is constant */
	if (!REG_P(dst) || !CONSTANT_P(src))
		goto err;

	/* extract second sub-expression */
	sub_expr = XVECEXP(expr, 0, 1);
	/* CLOBBER(flags) */
	/* extract operand */
	dst = XEXP(expr, 0);
	if (!CLOBBER_P(sub_expr) || REGNO(dst) != FLAGS_REG)
		goto err;
	/* cltd */
	return SUCC;
err:
	/* !cltd */
	return FAIL;
}

/*
 * examines if an expression explicitly modifies or clobbers the
 * RFLAGS.
 *
 * expr:	the expression to be examined
 * returns:	SUCC if the expression modifies the value of RFLAGS
 * 		FAIL otherwise
 */
static int __sets_flags(rtx expr)
{
	rtx src;	/* source operand 	*/
	rtx dst;	/* destination operand 	*/
	/* explicit overwrite of RFLAGS value
	   (SET((flags) (COMPARE(*)))) */
	if (SET_P(expr)) {
		/* extract source and destination operands */
		dst = XEXP(expr, 0);
		src = XEXP(expr, 1);
		if (REG_P(dst) && REGNO(dst) == FLAGS_REG && COMPARE_P(src))
			return SUCC;
	/* explicitly clobber RFLAGS (CLOBBER(flags)) */
	} else if (CLOBBER_P(expr)) {
	       /* extract operand */
		dst = XEXP(expr, 0);
		if (REG_P(dst) && REGNO(dst) == FLAGS_REG)
			return SUCC;
	}
	/* all other cases */
	return FAIL;

}

/*
 * examines if an instructions sets the value of RFLAGS.
 * Such instructions are `return', `call', `cmp', `test', `bit'
 * and arithmetic operations 
 *
 * insn:	the instruction to be examined
 * returns:	SUCC if the instruction sets the value of
 * 		RFLAGS
 * 		FAIL otherwise
 */
static int sets_flags(rtx insn)
{
	rtx expr;	/* holds the expression of the instruction
			   (after applying PATTERN) */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/
	unsigned i;	/* iterator */

	/* return */
	if (JUMP_P(insn)) {
		/* extract the expression */
		expr = PATTERN(insn);
		/* is it any return instruction? */
		if (ANY_RETURN_P(expr) || EH_RETURN_P(expr))
			/* Yes */
			goto succ;
	}

	/* call */
	if (CALL_P(insn))
		goto succ;

	/* all other cases must be INSN */
	if (!INSN_CODE_P(insn))
		goto fail;

	/* extract the expression */
	expr = PATTERN(insn);
	/* 
	 * identify corner cases that don't affect the rflags
	 * even though they contain a clobber statement
	 */
	if (is_cltd(expr) == SUCC || is_movz(expr) == SUCC)
		goto fail;

	/* handle PARALLEL */
	if (PARALLEL_P(expr)) {
		/* iterate sub-expressions */
		for (i = 0; i < XVECLEN(expr, 0); i++) {
			/* extract sub-expression */
			sub_expr = XVECEXP(expr, 0, i);
			/* 
			 * identify instruction that were emitted to only 
			 * set the value of RFLAGS
			 * (`cmp', `test', "cc reads" etc)
			 * or that clobber the value of RFLAGS
			 * (arithmetic operations etc)
			 */
			if (__sets_flags(sub_expr) == SUCC)
				goto succ;
		}
	} else {
		/* 
		 * identify instruction that were emitted to only 
		 * set the value of RFLAGS
		 * (`cmp', `test', "cc reads" etc)
		 * or that clobber the value of RFLAGS
		 * (arithmetic operations etc)
		 */
		if (__sets_flags(expr) == SUCC)
			goto succ;
	}
fail:
	return FAIL;
succ:
	return SUCC;
}

/*
 * examine if an expression is a SETcc instruction
 *
 * expr:	the expression to examine
 * returns:	SUCC if it is a SETcc instruction
 * 		FAIL otherwise
 *
 */
static int is_set(rtx expr)
{
	rtx src;	/* source operand 	*/
	rtx dst;	/* destination operand 	*/

	/* SETcc should always be a SET (brainf*ck ^2) */
	if (!SET_P(expr))
		goto fail;

	/* extract source operand */
	src = XEXP(expr, 1);
	/* ensure the source is a valid condition */
	if (GET_CODE(src) >= NE && GET_CODE(src) <= LTU) {
		/* extract the destination of the condition */
		dst = XEXP(src, 0);
		/* ensure that destination is the flags register */
		if (REG_P(dst) && REGNO(dst) == FLAGS_REG)
			return SUCC;
	}
fail:
	return FAIL;
}

/*
 * examine if an expression is a CMOVcc instruction
 *
 * expr:	the expression to examine
 * returns:	SUCC if it is a CMOVcc instruction
 * 		FAIL otherwise
 *
 */
static int is_cmov(rtx expr)
{
	rtx src;	/* source operand; needs to be IF_THEN_ELSE */
	rtx dst;	/* destination operand; needs to be a
			   register (other than RFLAGS) */

	/* the expression needs to be a SET */
	if (!SET_P(expr))
		/* not CMOV */
		goto out;

	/* extract the operands */
	dst = XEXP(expr, 0);
	src = XEXP(expr, 1);

	/* ensure that the source operand is IF_THEN_ELSE */
	if (!IF_THEN_ELSE_P(src))
		/* not CMOV */
		goto out;

	/* handle zero/sign extensions of the dst operand/register */
	while (dst && EXTEND_P(dst))
		/* extract sub-expression */
		dst = XEXP(expr, 0);

	/* dst == REG */	
	if (REG_P(dst)) {
		/* ensure it's not the RFLAGS */
		if (REGNO(dst) == FLAGS_REG)
			/* not CMOV */
			goto out;
	}
	/* dst == SUBREG */
	else if (SUBREG_P(dst)) {
		/* ensure it's not the RFLAGS */
	       if (REGNO(SUBREG_REG(dst)) == FLAGS_REG)
			/* not CMOV */
			goto out;
	}
	else
		/* not CMOV */
		goto out;

	/* verified CMOV */
	return SUCC;
out:
	/* not CMOV */
	return FAIL;
}


/*
 * identifies if the given expression is a `setcc' or `cmovcc' instruction
 *
 * expr:	the expression to be examined
 *
 * returns:	SUCC if it is a `setcc' or `cmovcc' instruction
 * 		FAIL otherwise
 */
static int __depends_on_flags(rtx expr)
{
	if ((is_set(expr) == SUCC) || (is_cmov(expr) == SUCC))
		return SUCC;
	else
		return FAIL;
}
/*
 * examines if an instructions depends on the value of RFLAGS.
 * Such instructions are conditional jumps (`jcc'),
 * conditional movs (`cmovcc') and `setcc'
 *
 * insn:	the instruction to be examined
 * returns:	SUCC if the instruction depends on the value of
 * 		RFLAGS
 * 		FAIL otherwise
 */
static int depends_on_flags(rtx insn)
{
	rtx expr;	/* holds the expression of the instruction
			   (after applying PATTERN) */
	rtx sub_expr;	/* holds sub-expression to be examined 	*/
	unsigned i;	/* iterator */

	/* jcc */
	if (JUMP_P(insn)) {
		/* extract the expression */
		expr = PATTERN(insn);
		/* conditional jumps are always SET */
		if (!SET_P(expr))
		       goto fail;
		/* the source operand of the SET
		   needs to be IF_THEN_ELSE */
		sub_expr = XEXP(expr, 1);
		if (IF_THEN_ELSE_P(sub_expr))
			/* jcc */
			goto succ;
		else
			/* anything else is not jcc */
			goto fail;
	/* cmovcc, setcc */
	} else if (INSN_CODE_P(insn)) {
		/* extract the expression */
		expr = PATTERN(insn);
		/* handle PARALLEL */
		if (PARALLEL_P(expr)) {
			/* examine each sub-expression individually */
			for (i = 0; i < XVECLEN(expr, 0); i++) {
				/* extract sub-expression */
				sub_expr = XVECEXP(expr, 0, i);
				if (__depends_on_flags(sub_expr) == SUCC)
					goto succ;
			}
			/* none depends on flags */
			goto fail;
		} else {
			if (__depends_on_flags(expr) == SUCC)
				goto succ;
			else
				goto fail;
		}
	/* we don't care about other codes */
	} else
fail:
		return FAIL;
	
succ:
	return SUCC;
}

/*
 * performs liveness analysis on the RFLAGS register.
 * Essentially it iterates the instructions that follow
 * the instruction to be instrumented. If an instruction
 * depends on the flags, we need to retain the pushf/popf
 * pair. If we find an instruction that overwrites the
 * RFLAGS value  we can (safely) eliminate the pair.
 *
 * NOTE: In most cases we will find the instruction we
 * look for in the same basic block. However if the basic
 * block we examine ends with a direct `jmp' we recursively
 * examine the successor basic block.
 *
 * insn:	the instruction to be instrumented
 *
 * returns:	SUCC if the we can safely eliminate the pushf/popf
 *		pair
 *		FAIL otherwise
 */
static int eliminate_push_pop(rtx insn)
{
	rtx insn_it;		/* instruction iterator 	*/
	basic_block bb;		/* the basic block we examine 	*/
	
	/* get the current basic block */
	bb = BLOCK_FOR_INSN(insn);
	/* iterate the basic block instructions,
	   starting from insn */
	BB_ITER_START(insn_it, insn) {
		/* does it depend on flags? (e.g. `jcc', `cmov') */
		if (depends_on_flags(insn_it) == SUCC)
			/* Yes, we should retain pushf/popf */
			goto fail;
		/* 
		 * does it overwrite the flags?
		 * (e.g. arithmetic operations, `cmp', `test',
		 * `set')
		 */
		else if (sets_flags(insn_it) == SUCC)
			/* Yes, we should eliminate pushf/popf */
			return SUCC;

		/* did we reach the end of this basic block? */
		if (insn_it == BB_END(bb)) {
			/* does the basic block end with a simple `jmp' */
			if (!single_succ_p(bb)) {
				continue;
			}
			/* get the successor basic block */
			bb = single_succ(bb);
			/* decide based on the successor */
			return eliminate_push_pop(BB_HEAD(bb));
		}
	}
fail:
	return FAIL;
}

/*
 * eliminates all unnecessary pushf/popf pairs from the
 * range checks, by applying liveness analysis
 *
 * checks:	the vector that holds all memory
 * 		reads to be instrumented
 */ 
static void eliminate_push_pops(std::vector<check_entry *> *checks)
{
	check_entry *entry;	/* the current entry to be examined */
	unsigned i;		/* iterator */

	/* iterate all check entries */
	for (i = 0; i < checks->size(); i++) {
		/* perform liveness analysis */
		if (eliminate_push_pop(checks->at(i)->real_insn) == SUCC) {
			/* eliminate pushf/popf */
			checks->at(i)->flags = false;
		}
	}
}

/*
 * returns the appropriate condition for rewriting CMOV instructions
 *
 * cond:	the condition in the IF_THEN_ELSE RTL construct
 * comp:	if true, we return the complementary condition
 * 		if false, we return the same condition
 *
 * returns: 	the condition that should be used in the jcc/mov construct
 */
static
int get_cond(rtx cond, int comp)
{
	/* cmovne (mem),%reg */
	if ((NE_P(cond) && comp) || (EQ_P(cond) && !comp))
		/* je 1f; mov (mem),%reg; 1: */
		return COND_EQ;
	/* cmove (mem),%reg */
	else if ((NE_P(cond) && !comp) || (EQ_P(cond) && comp))
		/* jne 1f; mov (mem),%reg; 1: */
		return COND_NE;
	/* cmovge (mem),%reg */
	else if ((GE_P(cond) && comp) || (LT_P(cond) && !comp))
		/* jl 1f; mov (mem),%reg; 1: */
		return COND_LT;
	/* cmovl (mem),%reg */
	else if ((GE_P(cond) && !comp) || (LT_P(cond) && comp))
		/* jge 1f; mov (mem),%reg; 1: */
		return COND_GE;
	/* cmovg (mem),%reg */
	else if ((GT_P(cond) && comp) || (LE_P(cond) && !comp))
		/* jle 1f; mov (mem),%reg; 1: */
		return COND_LE;
	/* cmovle (mem),%reg */
	else if ((GT_P(cond) && !comp) || (LE_P(cond) && comp))
		/* jg 1f; mov (mem),%reg; 1: */
		return COND_GT;
	/* cmovae (mem),%reg */
	else if ((GEU_P(cond) && comp) || (LTU_P(cond) && !comp))
		/* jb 1f; mov (mem),%reg; 1: */
		return COND_LTU;
	/* cmovb (mem),%reg */
	else if ((GEU_P(cond) && !comp) || (LTU_P(cond) && comp))
		/* jae 1f; mov (mem),%reg; 1: */
		return COND_GEU;
	/* cmova (mem),%reg */
	else if ((GTU_P(cond) && comp) || (LEU_P(cond) && !comp))
		/* jbe 1f; mov (mem),%reg; 1: */
		return COND_LEU;
	/* cmovbe (mem),%reg */
	else if ((GTU_P(cond) && !comp) || (LEU_P(cond) && comp))
		/* ja 1f; mov (mem),%reg; 1: */
		return COND_GTU;
	/* paranoia */
	return -1;
}

/*
 * create the conditional jump that will be used in the
 * jcc/mov construct that will replace the CMOVcc instruction
 *
 * cond_val:	the condition of the jcc
 * flags_reg:	the RFLAGS register
 * label:	the label that will be the target of the condtional jump
 *
 * returns:	the RTX that represents the conditional jump
 *
 */
static
rtx get_jcc(int cond_val, rtx flags_reg, rtx label)
{
	rtx cond;	/* the condition of the jcc */

	/* differentiate between different conditions */
	switch (cond_val) {
	/* je */
	case COND_EQ:
		cond = gen_rtx_EQ(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jne */
	case COND_NE:
		cond = gen_rtx_NE(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jl */
	case COND_LT:
		cond = gen_rtx_LT(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jge */
	case COND_GE:
		cond = gen_rtx_GE(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jle */
	case COND_LE:
		cond = gen_rtx_LE(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jg */
	case COND_GT:
		cond = gen_rtx_GT(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jb */
	case COND_LTU:
		cond = gen_rtx_LTU(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jae */
	case COND_GEU:
		cond = gen_rtx_GEU(VOIDmode, flags_reg, const0_rtx);
		break;
	/* jbe */
	case COND_LEU:
		cond = gen_rtx_LEU(VOIDmode, flags_reg, const0_rtx);
		break;
	/* ja */
	case COND_GTU:
		cond = gen_rtx_GTU(VOIDmode, flags_reg, const0_rtx);
		break;
	default:
		/* paranoia*/
		return NULL;
	}
	/* return the RTX representation of the jcc that targets label */
	return gen_rtx_IF_THEN_ELSE(VOIDmode,
					cond,
					gen_rtx_LABEL_REF(VOIDmode, label),
					pc_rtx);
}

/*
 * rewrite the CMOVcc instruction as a jcc/mov construct
 * to facilitate instrumentation.
 *
 * insn:	the CMOVcc instruction to be rewritten
 * dst:		the destination operand; always a register
 * cond:	the condition of the jcc instruction
 * mem:		the MEMORY rtx that represents the memory read
 * reverse:	a flag that indicates if we need to use the reverse
 * 		condition of the CMOVcc or we should keep the same
 *
 */
void rewrite_cmov(rtx insn, rtx dst, int cond, rtx mem)
{
	basic_block from;	/* basic block that contains the code
				   before the cmov instruction 		*/
	basic_block to;		/* basic block that contains the cmov
				   instruction 				*/
	rtx label;		/* label expression 			*/
	rtx flags_reg;		/* condition code register 		*/
	rtx jcc;		/* conditional jump expression 		*/
	rtx mov;		/* mov expression			*/
	rtx jcc_insn;		/* conditional jump instruction		*/

	/* split the basic block before the CMOV instruction */
	from = split_block(BLOCK_FOR_INSN(insn), insn)->src;
	to = single_succ(from);

	/* 
	 * bookkeeping; ensure that the new basic block is correctly
	 * connected with its predecessor
	 */
	redirect_edge_and_branch_force(single_succ_edge(from), to);
	if (forwarder_block_p(from))
		from->flags |= BB_FORWARDER_BLOCK;
	else
		from->flags &= ~BB_FORWARDER_BLOCK;

	/* create label */
	label = gen_label_rtx();
	/* extract the flags register */
	flags_reg = gen_rtx_REG(CCmode, FLAGS_REG);

	/* for every cond, create a jcc with the appropriate condition */
	jcc = get_jcc(cond, flags_reg, label);
	/* emit the conditional jump before the insn */
	jcc_insn = emit_jump_insn_before(gen_rtx_SET(VOIDmode, pc_rtx, jcc),
						insn);
	/* set the label as the target of the conditional jump */
	JUMP_LABEL(jcc_insn) = label;
	/* increase the number of uses of the label */
	LABEL_NUSES(label)++;
	/* emit the label after the insn */
	emit_label_after(label, insn);
	/* create mov instruction (memory read) */
	mov = gen_rtx_SET(VOIDmode, dst, mem);
	/* emit the mov instruction before the insn */
	emit_insn_before(mov, insn);
	/* remove the insn (CMOV) */
	remove_insn(insn);
}

/*
 * iterate all instructions looking for CMOV
 * instructions that perform memory reads and rewrite them
 * with jcc/mov constructs.
 *
 * NOTE: CMOVcc instructions are IF_THEN_ELSE RTL constructs.
 * 	 If the memory read happens in the THEN branch,
 * 	 the jcc we emit needs to have the complementary condition.
 *
 */
void rewrite_cmov_reads(void)
{
	rtx insn_it;		/* instruction iterator */
	basic_block bb_it;	/* basic block iterator */
	rtx expr;		/* subexpression of the instruction */
	rtx src;		/* source operand */
	rtx dst;		/* destination operand */
	rtx mem;		/* the memory subexpression */
	rtx org_cond;		/* the condition of the cmov */
	int cond;		/* the condition that should be
				   used in the jcc/mov constructs */

	/* iterate all instructions */
	INSN_ITER_BB(bb_it, insn_it) {
		/* cmov instructions have INSN code */
		if (!INSN_CODE_P(insn_it))
			/* next instruction */
			continue;

		/* obtain subexpression */
		expr = PATTERN(insn_it);
		/* check if it is a CMOVcc instruction */
		if (is_cmov(expr) == SUCC) {
			/* extract operands */
			dst = XEXP(expr, 0);
			src = XEXP(expr, 1);

			/* extract the condition */
			org_cond = XEXP(src, 0);

			/* check if the THEN expression is a memory read */
			if (MEM_P(XEXP(src, 1))) {
				/* get the condition for the jcc */
				cond = get_cond(org_cond, 1);
				/* extract the memory subexpression */
				mem = XEXP(src, 1);
			/* check if the ELSE expression is a memory read */
			} else if (MEM_P(XEXP(src, 2))) {
				/* get the condition for the jcc */
				cond = get_cond(org_cond, 0);
				/* extract the memory subexpression */
				mem = XEXP(src, 2);
			}
			/* this CMOVcc contains no memory read; nothing to do */
			else
				/* next instruction */
				continue;
			/* 
			 * test if the memory read or
			 * the destination are rsp-based
			 */
			if (reg_mentioned_p(stack_pointer_rtx, mem)
					|| dst == stack_pointer_rtx)
				/* won't instrument it; next instruction */
				continue;
#ifdef	DEBUG
			commitlog(insn_it, "Rewriting cmov:");
#endif
			/* rewrite CMOVcc */
			rewrite_cmov(insn_it, dst, cond, mem);
		}
	}
}

/* 
 * list of functions that should not be instrumented
 *
 * NOTE: the list *needs* to be lexicographically sorted.
 */
const char *krx_whitelist[] = {
	"krx_addr_val",
	"krx_atomic_read",
	"krx_bug_addr",
	"krx_bug_file",
	"krx_bug_flags",
	"krx_bug_line",
	"krx_can_boost",
	"krx_ex_fixup_addr",
	"krx_ex_insn_addr",
	"krx_fixup",
	"krx_generic_swap",
	"krx_get_addr",
	"krx_get_entry_code",
	"krx_get_entry_target",
	"krx_get_insn_val",
	"krx_get_key_entries",
	"krx_get_key_next",
	"krx_get_next",
	"krx_get_next_char",
	"krx_get_next_int",
	"krx_get_next_short",
	"krx_get_next_unsigned_short",
	"krx_get_opcode",
	"krx_get_tp_funcs",
	"krx_get_tp_name",
	"krx_get_tp_regfunc",
	"krx_get_tp_unregfunc",
	"krx_insn",
	"krx_iter_key",
	"krx_jump_entry_key",
	"krx_jump_label_cmp",
	"krx_jump_label_code",
	"krx_memcmp",
	"krx__memcpy",
	"krx_memcpy",
	"krx_peek_nbyte_next",
	"krx_peek_next",
	"krx_read_kprobe_opcode",
	"krx_recover_probed_insn",
	"krx_resume_execution",
	"krx_skip_prefixes",
	"krx_static_key_count",
	"krx_write_memcpy"
};

/*
 * check if a function is whitelisted
 *
 * Some functions must be whitelisted because they
 * actually read from the code section.
 * These functions in most cases belong to the
 * (dynamic) ftrace, kProbes and debugging subsystems
 * and the jump label optimization.
 *
 * NOTE: all whitelisted functions are either functions that
 * 	 are used *exclusively* to read from the code section
 * 	 or copies of the original functions that we manually
 * 	 created that are used *only* for this purpose.
 *
 * func_name: 	the name of the function that will be instrumented
 *
 * returns:	SUCC if the function belongs to the whitelised functions
 * 		FAIL otherwise
 */
static int compar_fn(const void *s1, const void *s2)
{ 
	return strcmp((const char *)s1, *((const char **)s2));
}

static int
is_whitelisted_function(const char *func_name)
{
	return (bsearch(func_name,
			krx_whitelist,
			sizeof(krx_whitelist)/sizeof(krx_whitelist[0]),
			sizeof(krx_whitelist[0]),
			compar_fn)) ? SUCC : FAIL;
}

/* 
 * krx pass
 * (callback; invoked for every translation unit) 
 *
 * Instruments every memory read with range checks,
 * using the selected optimization level.
 * If the MPX scheme is enabled, then only 1 optimization
 * exists, so any value other than O3 disables it.
 *
 * returns: SUCC on success, FAIL on error
 */
unsigned int __attribute__ ((visibility("default")))
krx_instrument(void)
{
	std::vector<check_entry *> checks;
	std::map<rtx, unsigned> insn_indices;

	/* check if function is whitelisted; if yes, return */
	if (unlikely(is_whitelisted_function(
			IDENTIFIER_POINTER(DECL_NAME(cfun->decl))) == SUCC))
		goto out;

#ifdef	DEBUG /* auditing */
	openlog();
#endif /* DEBUG */
	/* convert all cmov mem reads to jcc/mov constructs */
	rewrite_cmov_reads();
	/* get all instrumentation points */
	locate_mem_reads(&checks);
	/* examine optimization level */
	if (level == O3) {
		/* produce a hashmap that holds insn->index information */
		fill_insn_indices(&checks, &insn_indices);
		eliminate_checks(&checks, &insn_indices);
	}
	if (level >= O1 && mpx == DISABLE)
		/* liveness analysis on RFLAGS */
		eliminate_push_pops(&checks);
	/* emit the checks */
	emit_checks(&checks);
	/* clean-up the vector */
	free_checks(&checks);

#ifdef	DEBUG /* auditing */
	closelog();
#endif /* DEBUG */

out:
	/* return with success */
	return SUCC;
}

/*
 * argument parsing
 *
 * Parse the plugin arguments and set the corresponding
 * variables (i.e., `stub', `level', `mpx', `log').
 *
 * plugin_info:	information regarding the plugin; provided by GCC
 *
 * returns: SUCC on success, FAIL on error
 */
static int
parse_args(const struct plugin_name_args *plugin_info)
{
	/* iterator */
	int i;

	/* argument length */
	size_t len;

	/* parse the plugin arguments (if any) */
	for (i = 0; i < plugin_info->argc; i++) {

		/* where is my getopt-like API? */
		
		/* get the length of the argument */
		len = strlen(plugin_info->argv[i].key);

		/* stub */
		if (strncmp(plugin_info->argv[i].key,
					STUB_STR,
					strlen(STUB_STR)) == 0 &&
				len == strlen(STUB_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* stub can be an address or a symbol name */
				stub = plugin_info->argv[i].value;
			else {
				/* missing stub */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					NAME,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
		/* MPX */
		else if (strncmp(plugin_info->argv[i].key,
					MPX_STR,
					strlen(MPX_STR)) == 0 &&
				len == strlen(MPX_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* parse the decimal option */
				mpx = strtoul(plugin_info->argv[i].value,
						NULL,
						BASE10);
			else {
				/* missing mpx */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					NAME,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
		/* limit */
		else if (strncmp(plugin_info->argv[i].key,
					LIMIT_STR,
					strlen(LIMIT_STR)) == 0 &&
				len == strlen(LIMIT_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* parse the hex option */
				limit = strtoul(plugin_info->argv[i].value,
						NULL,
						BASE16);
			else {
				/* missing limit */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					NAME,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
		/* level */
		else if (strncmp(plugin_info->argv[i].key,
					OPT_LEVEL_STR,
					strlen(OPT_LEVEL_STR)) == 0 &&
				len == strlen(OPT_LEVEL_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0)) {
				/* parse the decimal value */
				level = strtoul(plugin_info->argv[i].value,
						NULL,
						BASE10);
				if (level > O3) {
					(void)fprintf(stderr,
						"%s: wrong value for argument %s\n",
						NAME,
						plugin_info->argv[i].key);
	
					/* fail */
					return FAIL;
				}
			}
			else {
				/* missing log filename */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					NAME,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
#ifdef	DEBUG
		/* log */
		else if (strncmp(plugin_info->argv[i].key,
					LOG_STR,
					strlen(LOG_STR)) == 0 &&
				len == strlen(LOG_STR)) {
			if ((plugin_info->argv[i].value != NULL) &&
				(strlen(plugin_info->argv[i].value) != 0))
				/* set the log filename */
				log = plugin_info->argv[i].value;
			else {
				/* missing log filename */
				(void)fprintf(stderr,
					"%s: missing option for argument %s\n",
					NAME,
					plugin_info->argv[i].key);

				/* fail */
				return FAIL;
			}
		}
#endif
		/* default handler */
		else {
			/* invalid argument */
			(void)fprintf(stderr,
				"%s: invalid argument %s\n",
				NAME,
				plugin_info->argv[i].key);

			/* fail */
			return FAIL;
		}
	}

	/* success */
	return SUCC;
}

/*
 * version checking
 *
 * Parse and compare two GCC versions.
 *
 * NOTE:
 * 	- The two versions are assumed to be in
 * 	the form: "MAJOR.MINOR.PATCHLEVEL", and
 * 	{MAJOR, MINOR, PATCHLEVEL} < 10.
 * 	- We invoke `strtol()' without checking
 * 	`errno' for `ERANGE'.
 *
 * lhs_ver:	the first version to compare
 * rhs_ver:	the second version to compare
 *
 * returns:	0 if the two versions are the same,
 * 		>0 if lhs_ver > rhs_ver, or <0 otherwise
 */
static int
version_check(const struct plugin_gcc_version *lhs_ver,
		const struct plugin_gcc_version *rhs_ver)
{
	/* iterator */
	const char *it	= NULL;
	
	/* base multiplier */
	size_t mult	= 1;

	/* parsed versions; integers */
	size_t lhsver	= 0;
	size_t rhsver	= 0;

	/* parse the lhs_ver version */

	/* "it" points to the end of the version string */
	it = lhs_ver->basever + strlen(lhs_ver->basever) -
		((strlen(lhs_ver->basever) > 0) ? 1 : 0);

	/* right-to-left traversal */
	while(lhs_ver->basever != it--) {
		/* found delimiter */
		if (*it == VER_DELIM) {
			/* extract the subversion */
			lhsver	+= (mult * strtol(it + 1, NULL, BASE10));
			/* update the multiplier (i.e., base) */
			mult	*= BASE10;
		}
	}
	/* extract the remainder */
	lhsver += (mult * strtol(lhs_ver->basever, NULL, BASE10));

	/* fix (e.g., 4.6 needs to be 460 and not 46) */
	while (unlikely((lhsver < BASE100) && (lhsver > 0))) lhsver *= BASE10;
	while (unlikely(lhsver > (10 * BASE100) - 1)) lhsver /= BASE10;
	
	/* parse the rhs_ver version */

	/* reset the multiplier */
	mult = 1;

	/* same as lhs_ver */
	it = rhs_ver->basever + strlen(rhs_ver->basever) -
		((strlen(rhs_ver->basever) > 0) ? 1 : 0);
	while(rhs_ver->basever != it--) {
		if (*it == VER_DELIM) {
			rhsver	+= (mult * strtol(it + 1, NULL, BASE10));
			mult	*= BASE10;
		}
	}
	rhsver += (mult * strtol(rhs_ver->basever, NULL, BASE10));
	
	while (unlikely((rhsver < BASE100) && (rhsver > 0))) rhsver *= BASE10;
	while (unlikely(rhsver > (10 * BASE100) - 1)) rhsver *= BASE10;
	
	/* return the difference of lhs_ver and rhs_ver */
	return lhsver - rhsver;	
}

/* 
 * plugin initialization (kR^X)
 *
 * Invoked right after the plugin is loaded. It does some version
 * checking and registers the `krx' RTL pass that performs
 * code instrumentation.
 *
 * NOTE: The new pass (i.e., `krx') is invoked after most
 * of the low-level optimizations have been applied, since we do
 * not want to instrument code that will be modified or removed
 * due to subsequent optimization passes.
 *
 * plugin_info: information regarding the plugin; provided by GCC
 * version:     version information; provided by GCC
 *
 * returns:     SUCC on success, FAIL on error
 */
int __attribute__ ((visibility("default")))
plugin_init(struct plugin_name_args *plugin_info,
		struct plugin_gcc_version *version)
{
	/* pass control structure */
	struct register_pass_info pass_info;
        /* version checking; the actual GCC version vs. required GCC */
        if (unlikely(version_check(version, &pver) < 0)) {
                /* incompatible GCC version */
                (void)fprintf(stderr,
                        "%s: incompatible GCC version: %s (required >= %s)\n",
                        NAME,
                        version->basever,
                        pver.basever);

                /* failed */
                goto err;
        }

        /* argument parsing */
        if (unlikely(parse_args(plugin_info) == FAIL))
                /* failed */
                goto err;

	/* provide version and help information to GCC */
	register_callback(plugin_info->base_name, PLUGIN_INFO, NULL, &pinfo);

	/* register the new pass; std boilerplate */
	pass_info.pass 				= &pass_krx.pass;
	pass_info.ref_pass_instance_number	= 0;
	pass_info.pos_op			= PASS_POS_INSERT_AFTER;

	/* chain the new pass after "vartrack"; why?... good question */
	pass_info.reference_pass_name		= "vartrack";
	pass_info.pass->next			=
					pass_variable_tracking.pass.next;
	
	/* provide the pass information to the pass manager */
	register_callback(plugin_info->base_name,
				PLUGIN_PASS_MANAGER_SETUP,
				NULL,
				&pass_info);
	/* exit with success */
	return SUCC;
err:
	/* exit with failure */
	return FAIL;
}
#endif
