#!/bin/bash

# the fisher-yates shuffling algorithm
function fisher_yates {
	local array=( "$@" )
	if (( "${#array[@]}" == 1 )); then
		echo "${array[@]}"
	else
		for (( i=${#array[@]}-1; i > 0; i--));
		do
			local j=$(( RANDOM % $i ))
			local tmp="${array[$i]}"
			array[$i]=${array[$j]}
			array[$j]=$tmp
		done
		echo "${array[@]}"
	fi
}

# get the module functions
funcs=( $($OBJDUMP -h $1 | $AWK '{print $2}' | $GREP -E "^\.text\.") )
# get the module xkeys
xkeys=( $($OBJDUMP -h $1 | $AWK '{print $2}' | $GREP -E "^\.xkeys\.") )

# read the linker script
IFS=$'\r\n' GLOBIGNORE='*':;
lds_lines=( $($AWK '{print $0}' $KBUILD_LDS) )
# compute its length
lds_size=${#lds_lines[@]}

IFS=$' \t\n' GLOBIGNORE='*':;
# clear any residues from previous compilations
echo -n "" > $KBUILD_KRX_LDS
# iterate the lines
for ((i = 0; i < $lds_size; i++))
do
	# identify the location that we should permute the functions
	if [[ ${lds_lines[$i]} == *\*\(.text\)* ]];
	then
		# we have the right line.
		# prepare everything to add the
		# (permuted) function sections
		line=${lds_lines[$i]}
		while [ -n "${line}" ];
		do
			# delete until the first occurrence of *(
			del_beg=${line#*\*(}
			# delete after the first of occurrence of )
			sec_name=${del_beg%%)*}
			# got the text sections. Add them to
			# the functions so that they can be permuted
			funcs+=($sec_name)
			#delete up until the first )
			line=${del_beg#*\)}
		done
		# prepared everything.
		# get the number of functions
		funcs_size=${#funcs[@]}
		# permute the functions
		rand_funcs=( $(fisher_yates "${funcs[@]}") )
		# emit the functions
		echo -n ". = ALIGN(8); " >> $KBUILD_KRX_LDS
		for ((j = 0; j < $funcs_size; j++))
		do
			# add function
			echo -n "*("${rand_funcs[$j]//[[:space:]$'\n']}") " >> \
								$KBUILD_KRX_LDS
		done
		# done
		echo "" >> $KBUILD_KRX_LDS
	elif [[ ${lds_lines[$i]} == *\*\(.xkeys\)* ]];
	then
		# *(.xkeys) is a placeholder.

		# get the number of keys
		xkeys_size=${#xkeys[@]}
		# permute the keys
		rand_xkeys=( $(fisher_yates "${xkeys[@]}") )

		# emit the keys
		echo -n ". = ALIGN(8); " >> $KBUILD_KRX_LDS
		for ((j = 0; j < $xkeys_size; j++))
		do
			# add key
			echo -n "*("${rand_xkeys[$j]//[[:space:]$'\n']}") " >> \
								$KBUILD_KRX_LDS
		done
		# done
		echo "" >> $KBUILD_KRX_LDS
	else
		echo ${lds_lines[$i]} >> $KBUILD_KRX_LDS
	fi
done
