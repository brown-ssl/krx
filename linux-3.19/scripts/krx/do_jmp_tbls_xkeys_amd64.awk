#!/usr/bin/awk -f

BEGIN {
	# the caller set the value of `outname'
	jmp			= 0
	previous_section	= ""
	section			= ".text"
	# erase file from previous compilation
	print "" > outname
	srand(int(seed));
	PREC			= "double"
}

{
	if (jmp == 1) {
		if (match($0, /^[ \t]*\.section[ \t]+\.rodata/)) {
			line = $0
			gsub(/\.rodata/, ".jmp_tbls, \"a\"", line)
			print line >> outname
			next;
		}

		jmp = 0
	}

	if (match($0, /^[ \t]*jmp[ \t]*\*\.L.+\(,/)) {
		jmp = 1
	}

	if (match($0, /^[ \t]*\.section[ \t]+(.+),[ \t]*\".+/, arr)) {
		previous_section = section
		section		 = arr[1];
	} else if (match($0, /^[ \t]*\.section[ \t]+(.+)$/, arr)) {
		previous_section = section
		section		 = arr[1];
	} else if (match($0, /^[ \t]*\.text[ \t]*$/)) {
		previous_section = section
		section		 = ".text"
	} else if (match($0, /^[ \t]*\.previous[ \t]*/)) {
		section		 = previous_section
	}

	if (match($0, /^[ \t]\.type[ \t]+(.+),[ \t]*@function$/, arr)) {
		func_name = arr[1]

		if (match(func_name, /^[[:alpha:]]+\.[[:digit:]]+$/) && !match(func_name, /\.isra\./)) {
			func_name = substr(func_name, 0, index(func_name, ".") - 1)
		}

		name		= ".xkey." inname "_" func_name
		sec_name	= ".xkeys." inname "_" func_name

		print "\t.section " sec_name ",\"a\",@progbits" >> outname
		print "\t.align 8" >> outname
		print "\t" name ":" >> outname
		printf "\t.quad 0x" >> outname

		printf	"%x%x%x%x%x%x%x%x",
			int(rand()*256),
			int(rand()*256),
			int(rand()*256),
			int(rand()*256),
			int(rand()*256),
			int(rand()*256),
			int(rand()*256),
			int(rand()*256) >> outname

		printf "\n" >> outname
		print "\t.section " section >> outname
	}

	if (match($0, /krx_replace_me/)) {
		line = gensub(/krx_replace_me/, name, "g")
		print line >> outname
		next
	}

	# log line from file
	print $0 >> outname
}
