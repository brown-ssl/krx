#include <linux/jump_label.h>
#include <asm/desc.h>

#ifdef	CONFIG_KRX
#ifdef	CONFIG_X86_64
noinline
#endif
int krx_static_key_count(struct static_key *key)
{
	int ret;
	krx_disable();
	ret = atomic_read(&key->enabled);
	krx_enable();
	return ret;
}

#ifdef	CONFIG_X86_64
noinline
#endif
int static_key_count(struct static_key *key)
{
	return atomic_read(&key->enabled);
}
EXPORT_SYMBOL(static_key_count);

#if	defined(CC_HAVE_ASM_GOTO) && defined(CONFIG_JUMP_LABEL)
#ifdef	CONFIG_X86_64
noinline
#endif
struct jump_entry *krx_get_key_entries(struct static_key *key)
{
	struct jump_entry *entries;
	krx_disable();
	entries = key->entries;
	asm volatile ("" : : : "memory");
	krx_enable();
	return entries;
}
#endif
#endif
