#ifndef _LINUX_SORT_H
#define _LINUX_SORT_H

#include <linux/types.h>

void sort(void *base, size_t num, size_t size,
	  int (*cmp)(const void *, const void *),
	  void (*swap)(void *, void *, int));
#ifdef	CONFIG_KRX
void krx_sort(void *base, size_t num, size_t size,
	  int (*cmp)(const void *, const void *),
	  void (*swap)(void *, void *, int));
#else
#define krx_sort(base, num, size, cmp, swap)	sort(base, num, size, cmp, swap)
#endif
#endif
