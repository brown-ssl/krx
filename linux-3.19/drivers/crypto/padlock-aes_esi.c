#include <crypto/algapi.h>
#include <crypto/aes.h>
#include <crypto/padlock.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/percpu.h>
#include <linux/smp.h>
#include <linux/slab.h>
#include <asm/cpu_device_id.h>
#include <asm/byteorder.h>
#include <asm/processor.h>
#include <asm/i387.h>

#if defined(CONFIG_KRX) && defined(CONFIG_X86_32)
extern unsigned int ecb_fetch_blocks;
extern unsigned int cbc_fetch_blocks;

/* Control word. */
struct cword {
	unsigned int __attribute__ ((__packed__))
		rounds:4,
		algo:3,
		keygen:1,
		interm:1,
		encdec:1,
		ksize:2;
} __attribute__ ((__aligned__(PADLOCK_ALIGNMENT)));

extern void ecb_crypt(const u8 *in, u8 *out, u32 *key,
			     struct cword *cword, int count);
extern u8 *cbc_crypt(const u8 *in, u8 *out, u32 *key,
			    u8 *iv, struct cword *cword, int count);

/*
 * While the padlock instructions don't use FP/SSE registers, they
 * generate a spurious DNA fault when cr0.ts is '1'. These instructions
 * should be used only inside the irq_ts_save/restore() context
 */

void rep_xcrypt_ecb(const u8 *input, u8 *output, void *key,
				  struct cword *control_word, int count)
{
	asm volatile (".byte 0xf3,0x0f,0xa7,0xc8"	/* rep xcryptecb */
		      : "+S"(input), "+D"(output)
		      : "d"(control_word), "b"(key), "c"(count));
}

u8 *rep_xcrypt_cbc(const u8 *input, u8 *output, void *key,
				 u8 *iv, struct cword *control_word, int count)
{
	asm volatile (".byte 0xf3,0x0f,0xa7,0xd0"	/* rep xcryptcbc */
		      : "+S" (input), "+D" (output), "+a" (iv)
		      : "d" (control_word), "b" (key), "c" (count));
	return iv;
}

void padlock_xcrypt_ecb(const u8 *input, u8 *output, void *key,
                                      void *control_word, u32 count)
{
        u32 initial = count & (ecb_fetch_blocks - 1);

        if (count < ecb_fetch_blocks) {
                ecb_crypt(input, output, key, control_word, count);
                return;
        }

        if (initial)
                asm volatile (".byte 0xf3,0x0f,0xa7,0xc8"       /* rep xcryptecb */
                              : "+S"(input), "+D"(output)
                              : "d"(control_word), "b"(key), "c"(initial));

        asm volatile (".byte 0xf3,0x0f,0xa7,0xc8"       /* rep xcryptecb */
                      : "+S"(input), "+D"(output)
                      : "d"(control_word), "b"(key), "c"(count - initial));
}

u8 *padlock_xcrypt_cbc(const u8 *input, u8 *output, void *key,
                                     u8 *iv, void *control_word, u32 count)
{
        u32 initial = count & (cbc_fetch_blocks - 1);

        if (count < cbc_fetch_blocks)
                return cbc_crypt(input, output, key, iv, control_word, count);

        if (initial)
                asm volatile (".byte 0xf3,0x0f,0xa7,0xd0"       /* rep xcryptcbc */
                              : "+S" (input), "+D" (output), "+a" (iv)
                              : "d" (control_word), "b" (key), "c" (initial));

        asm volatile (".byte 0xf3,0x0f,0xa7,0xd0"       /* rep xcryptcbc */
                      : "+S" (input), "+D" (output), "+a" (iv)
                      : "d" (control_word), "b" (key), "c" (count-initial));
        return iv;
}
#endif
