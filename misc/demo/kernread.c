#include <linux/debugfs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/uaccess.h>

/* buffer size for holding a virtual address */
#if	defined(__i386__)	/* x86 */
#define ADDR_SZ	16		/* 16 bytes */
#elif	defined(__x86_64__)	/* x86-64 */
#define ADDR_SZ	32		/* 32 bytes */
#endif


/* kernel-mapped data pointer */
unsigned long *dptr;


/*
 * writing to the `data_ptr` file overwrites the data pointer to `dptr` with an
 * arbitrary value set from user space
 */
static ssize_t
write_data(struct file *f, const char __user *buf, size_t count, loff_t *off)
{
	/* address buffer */
	char addr[ADDR_SZ];

	/* cleanup */
	memset(addr, 0, ADDR_SZ);

	/* copy the buffer (user specified address) to kernel space */
	if (copy_from_user(addr, 
			buf, 
			(count < ADDR_SZ - 1) ? count : ADDR_SZ - 1) != 0) {
		/* failed :( */
		printk(KERN_ERR 
			"kernread: overwriting the data pointer failed\n");
		return -EINVAL;
	}

	/* overwrite the data pointer */
	dptr 		= (void *)simple_strtoul(addr, NULL, 16);
	f->private_data = dptr;

	printk(KERN_DEBUG
		"kernread: overwriting data pointer with 0x%p\n", dptr);

	/* done! :) */
	return count;
}

/*
 * reading the `data_ptr` file returns to user space what `dptr` points to in
 * kernel space
 */
static ssize_t
read_data(struct file *f, char __user *buf, size_t count, loff_t *off)
{
	unsigned long tval = *dptr;	/* simulate the memory leak */

	/* copy count bytes from kernel space to buf (user space) */
	if (copy_to_user(buf, &tval, (count <= sizeof(tval) ? 
					count : 
					sizeof(tval))) != 0) {
		/* failed */
		printk(KERN_ERR 
			"kernread: failure reading %zd bytes from "
			"kernel space address %p\n", count, dptr);
		return -EINVAL;
	}

	printk(KERN_DEBUG 
		"kernread: success reading %zd bytes from "
		"kernel space address %p\n", count, dptr);

	/* done */
	return count;
}


/* handles to the files we create */
static struct dentry *kernread_root = NULL;
static struct dentry *data_ptr = NULL;

/* structs telling the kernel how to handle writes and reads to our files */
static const struct file_operations data_fops = {
	.write = write_data,
	.read = read_data,
};

/* module cleanup; remove files and the directory */
static void
cleanup_debugfs(void)
{
	if (data_ptr != NULL)
		debugfs_remove(data_ptr);
	if (kernread_root != NULL)
		debugfs_remove(kernread_root);
}

/* module loading callback */
static int
kernread_init(void)
{
	/* initialize the data pointer */
	dptr = NULL;

	/* create the kernread directory in debugfs root directory */
	kernread_root = debugfs_create_dir("kernread", NULL);

	/* failed; debugfs support might be missing */
	if (kernread_root == NULL) {
		printk(KERN_ERR "kernread: creating root directory failed\n");
		return -ENODEV;
	}

	/* create the files with the appropriate `fops` struct and perms */
	data_ptr = debugfs_create_file("data_ptr",
					0666, /*rw-rw-rw-*/
					kernread_root,
					NULL,
					&data_fops);

	/* error handling */
	if (data_ptr == NULL)
		goto out_err;

	printk(KERN_INFO "kernread: module loaded\n");

	/* SUCCESS! */
	return 0;

out_err: /* cleanup after errors */
	printk(KERN_ERR "kernread: creating files in root directory failed\n");
	cleanup_debugfs();
	return -ENODEV;
}

/* module unloading callback */
static void
kernread_exit(void)
{
	cleanup_debugfs();
}

/* register module load/unload callbacks */
module_init(kernread_init);
module_exit(kernread_exit);

/* extremely necessary information */
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Alexander J. Gaidis <alexander_gaidis@alumni.brown.edu>, Vasileios P. Kemerlis <vpk@cs.brown.edu>");
MODULE_DESCRIPTION("Simulates an arbitrary memory disclosure vulnerability via debugfs");
MODULE_VERSION("1");
