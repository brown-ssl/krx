<div align="center">

# kR^X

#### Comprehensive Kernel Protection against Just-In-Time Code Reuse

</div>

kR^X is a kernel hardening scheme based on execute-only memory and code diversification to combat: (a) return oriented programming (ROP)/jump oriented programming (JOP) and similar code reuse attacks, (b) direct just-in-time return oriented programming (JIT-ROP), and (c) indirect JIT-ROP. We study a previously unexplored point in the design space, where a hypervisor or a super-privileged component is not required. Implemented mostly as a set of `GCC` plugins, kR^X is readily applicable to the x86-64 Linux kernel and can benefit from hardware support (e.g., MPX on modern Intel CPUs) to optimize performance. In full protection mode, kR^X incurs a low runtime overhead of 4.04%, which drops to 2.32% when MPX is available.

# Publication

<!-- TODO: might want to just link to Marios' website -->
The full paper published in EuroSys can be found [here](misc/krx.pdf).

The talk given at Black Hat can be found [here](https://www.youtube.com/watch?v=L-3eCmZ8s3A).

# Overview

<!-- TODO: Provide an overview of kR^X beyond what is mentioned in the abstract above -->

# Distribution

The kR^X bundle contains the following files.

<details>
<summary><code>install.sh</code> Script</summary>

`install.sh` is a Bash script meant to expedite installation of the minimal amd64 build.

</details>

<details>
<summary><code>config</code> Directory</summary>

`config` contains eight configuration files for the Linux kernel v3.19 that were used for implementing and testing kR^X.

<div align="center">

| Architecture | Configuration File           | Description |
| :----------: | :--------------------------: | :---------- |
| x86  | `config-3.19-i386.krx.def` <br> `config-3.19-i386.krx.min` <br> `config-3.19-i386.krx.deb` <br> `config-3.19-i386.krx.dbg` |  default build <br> minimal build <br> full build <br> debug; boot-time tests  |
| x86_64  | `config-3.19-amd64.krx.def` <br> `config-3.19-amd64.krx.min` <br> `config-3.19-amd64.krx.deb` <br> `config-3.19-amd64.krx.dbg` |  default build <br> minimal build <br> full build <br> debug; boot-time tests  |

##### Table 1. Linux kernel configuration files for kR^X. 

</div>

We recommend starting with `config-3.19-amd64.krx.min` due its quicker compilation times.

</details>

<details>
<summary><code>linux-3.19</code> Directory</summary>

`linux-3.19` contains the patched Linux kernel v3.19 source code as well as the necessary `GCC` plugins for compiling the kernel.

The patch sets up the new kernel address space layout, `kR^X-KAS`; the `%bnd0` register for the MPX protection scheme; and the segmentation protection scheme, `kR^X-SEG`, for x86.

The `GCC` plugins can be found in `linux-3.19/scripts/krx/`. `krx_plugin` allows the kernel to be compiled with the `kR^X-SFI` or `kR^X-MPX` protection schemes. `kaslr_plugin` enables fine-grained randomization, at both the function and basic block level, as well as return address protections.

</details>

<details>
<summary><code>docs</code> Directory</summary>

`docs` contains documentation for installing Debian and kR^X, troubleshooting, and conducting a small test of the kR^X system.

</details>

<details>
<summary><code>misc</code> Directory</summary>

The `misc` directory contains extras to ease installation and demonstrate kR^X's capabilities. Specifically, it contains:

* `binutils-2.25.tar.gz`, the gzip compressed archive of `binutils` v2.25 source code
* the `demo` directory, comprising code for a simple kR^X test [further described here](docs/demo.md)

</details>

<br>

<details>
<summary>The elements in this dropdown are outlined in the publication but are not included in this distribution.</summary>

<br>

<details>
<summary>Phantom Code Blocks</summary>

Phantom blocks are blocks of code containing exception-triggering instructions within a function that contribute to its entropy pool for fine-grained KASLR. 

</details>

<details>
<summary>Entropy Adjustment</summary>

Tuning fine-grained KASLR's entropy determines how many blocks a function is split into, adding phantom blocks when necessary.

</details>

<details>
<summary>Return Address Decoys</summary>

As an alternative to return address encryption, two return addresses can be pushed to the stack where guessing the incorrect one can result in an exception.

</details>

<details>
<summary><code>xkey</code> Replenishment at Initialization</summary>

`xkey`s, used in return address encryption, and their addresses should be re-randomized during initialization, however this distribution re-randomizes at recompilation.

</details>

</details>


# Installation

Installation of the kR^X kernel should be carried out on a [Debian GNU/Linux v7 system](docs/installing-deb.md). We recommend starting with the minimal amd64 build with the `kR^X-SFI` scheme enabled, which can be installed with the following commands after `cd`ing into the repository:

```Bash
sudo ./install.sh
sudo reboot
```

Now that the kR^X kernel has been installed, we recommend following a small [demo of kR^X's capabilities](docs/demo.md).

Installing the kR^X kernel for other architectures and with other protection schemes is described at length [here](docs/installing-krx.md).

Troubleshooting tips and help setting up the base Debian system can be found [here](docs/troubleshooting.md).

# License
<!-- TODO: include license in repo -->
Gnu General Public License
