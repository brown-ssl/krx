<div align="center">

# Installing kR^X

#### How to Install a Custom kR^X Kernel

</div>

After [installation of the base Debian system](installing-deb.md) additional packages (and their dependencies) need to be installed before compiling the kR^X kernel.

# Dependency Installation

Table 1 lists packages that can be installed with `apt`.

<div align="center">

| Package              | Version | Description                         |
| :------------------- | :-----: | :---------------------------------- |
| `bc`                 | 1.06.95 | The GNU bc arbitrary precision calculator language |
| `gawk`               | 4.0.1   | GNU awk, a pattern scanning and processing language |
| `gcc`                | 4.7.2   | GNU C compiler                      |
| `g++`                | 4.7.2   | GNU C++ compiler                    |
| `gcc-4.7-plugin-dev` | 4.7.2   | Files for GNU GCC plugin development. |
| `libbsd-dev`         | 0.4.2   | utility functions from BSD systems - development files |
| `make`               | 3.81    | An utility for Directing compilation. |
##### Table 1. Additional package requirements for compiling the kR^X kernel.

</div>

To run the MPX protection scheme, `binutils` v2.25 is necessary to compile the kernel and must be built from source. Table 2 lists additional packages needed to install `binutils` from source.

<div align="center">

| Package              | Version | Description                         |
| :------------------- | :-----: | :---------------------------------- |
| `bison`              | 2.5     | YACC-compatible parser generator    |
| `flex`               | 2.5.35  | A fast lexical analyzer generator   |
| `m4`                 | 1.4.16  | A macro processing language         |
| `texinfo`            | 4.13    | Documentation system for on-line information and printed output |
##### Table 2. Additional package requirements for building `binutils` from source.

</div>

Code Block 1 details the shell commands needed to install `binutils` from source.

```Bash
user@deb:~/krx$ tar xfz binutils-2.25.tar.gz -C ~   # untar the source
user@deb:~$ cd ../binutils-2.25
user@deb:~/binutils-2.25$ mkdir build        # make a dedicated build directory
user@deb:~/binutils-2.25$ cd build
user@deb:~/binutils-2.25/build$ ../configure --prefix=/usr       \    # prepare binutils for compilation 
                                             --enable-gold       \
                                             --enable-ld=default \
                                             --enable-plugins    \
                                             --enable-shared     \
                                             --disable-werror    \
                                             --enable-64-bit-bfd \
                                             --with-system-zlib  \
                                             --with-sysroot=/
user@deb:~/binutils-2.25/build$ make tooldir=/usr               # compile the package
user@deb:~/binutils-2.25/build$ make -k check                   # test compilation results
user@deb:~/binutils-2.25/build$ sudo make tooldir=/usr install  # install the package
```
<div align="center">

##### Code Block 1. Shell commands to compile and install `binutils` from source.

</div>

# Installing the Kernel

With all dependencies met, the compilation and installation of the kR^X kernel can commence. Code Block 2 walks through the necessary shell commands.

```Bash
user@deb:~/krx/linux-3.19$ cp ../config/<config> ./.config  # select a configuration file
user@deb:~/krx/linux-3.19$ make oldconfig                   # configure the kernel
user@deb:~/krx/linux-3.19$ make -j`nproc`                   # build the kernel
user@deb:~/krx/linux-3.19$ sudo make modules_install        # install the modules
user@deb:~/krx/linux-3.19$ sudo make install                # install the kernel
user@deb:~/krx/linux-3.19$ sudo reboot
# ... log back in ...
user@deb:~$ uname -r
3.19.0-krx        
```
<div align="center">

##### Code Block 2. Shell commands to compile and install the kR^X kernel.

</div>

For help with errors, please refer to our [troubleshooting guide](troubleshooting.md).