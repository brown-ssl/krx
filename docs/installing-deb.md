<div align="center">

# Installing Debian

#### How to Install Debian

</div>

This section describes how to install the [Debian GNU/Linux v7](https://www.debian.org/releases/wheezy/debian-installer/) operating system from scratch for building and installing the kR^X kernel.

After booting the machine with the removable media containing the Debian image inserted, the *Debian Gnu/Linux installer boot menu* will appear. From this menu, go through the options according to the structure outlined below. Each leaf of the tree indicates an option that the user should select in a given menu. Not everything needs to match perfectly, such as the language, locale, and keyboard layout.

```
Boot menu
|-- Advanced options
|   |-- Expert install
|   |   |-- Choose language
|   |   |   |-- English - English
|   |   |   |-- United States
|   |   |   |-- United States - en_US.UTF-8
|   |   |   |-- deselect all locales
|   |   |-- Configure the keyboard
|   |   |   |-- American English
|   |   |-- Detect and mount CD-ROM
|   |   |-- Load installer components from CD
|   |   |   |-- deselect all installer components
|   |   |-- Detect network hardware
|   |   |-- Configure the network
|   |   |   |-- Yes
|   |   |   |-- link detection time: 3 seconds
|   |   |   |-- hostname: krx
|   |   |   |-- domain name: *blank*
|   |   |-- Setup users and passwords
|   |   |   |-- Yes
|   |   |   |-- No
|   |   |   |-- full name: *name*
|   |   |   |-- username: *username*
|   |   |   |-- password: *password*
|   |   |   |-- confirm password
|   |   |-- Configure the clock
|   |   |   |-- Yes
|   |   |   |-- NTP server: 0.debian.pool.ntp.org
|   |   |   |-- Continue
|   |   |   |-- Eastern
|   |   |-- Detect disks
|   |   |-- Partition disks
|   |   |   |-- Guided - use entire disk
|   |   |   |-- SCSI3 (0,0,0) (sda) - VMWare, VMWare Virtual S
|   |   |   |-- All files in one partition (recommended for new users)
|   |   |   |-- Finish partitioning and write changes to disk
|   |   |   |-- Yes
|   |   |-- Install the base system
|   |   |   |-- linux-image-amd64
|   |   |   |-- generic: include all available drivers
|   |   |-- Configure the package manager
|   |   |   |-- No
|   |   |   |-- deselect all services
|   |   |-- Select and install software
|   |   |   |-- deselect all packages
|   |   |   |-- No
|   |   |   |-- Yes
|   |   |   |-- deselect all software
|   |   |-- Install the GRUB boot loader on a hard disk
|   |   |   |-- Yes
|   |   |-- Finish the installation
|   |   |   |-- Yes
```
<div align="center">

##### Code Block 1. A walkthrough of the custom installation process for a the Debian v7 operating system.

</div>

Once the installation is complete, the system should be shutdown and the removable media can be ejected.

For a straight-forward installation of kR^X refer to [the Installation section of the README](../README.md).

For advanced setup and installation of kR^X refer to [the relevant documentation](installing-krx.md).