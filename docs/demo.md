<div align="center">

# kR^X Demo

#### A Simple Example to Illustrate kR^X's Capabilities

</div>

Once the kR^X kernel has been successfully installed, a small test using the `kernread` Linux kernel module (source at [`misc/demo/kernread.c`](../misc/demo/kernread.c)) can be conducted. The module utilizes `debugfs(8)` to simulate an arbitrary memory disclosure vulnerability. Thus, before loading the module, `debugfs` must be mounted and permissioned.

```Bash
user@krx:~$ sudo mount -t debugfs none /sys/kernel/debug
user@krx:~$ sudo chmod +x /sys/kernel/debug
```
<div align="center">

##### Code Block 1. Mounting and permissioning `debugfs`.

</div>

From here, the module can be built and loaded.

```Bash
user@krx:~/krx$ make
user@krx:~/krx$ sudo insmod kernread.ko
```
<div align="center">

##### Code Block 2. Compiling and inserting the `kernread` Linux kernel module.

</div>

Inserting the module creates a file, `/sys/kernel/debug/kernread/data_ptr`. Writing an address to this file will overwrite a data pointer in kernel space and reading from this file discloses memory at the data pointer simulating a vulnerability. With kR^X active, attempting to read the kernel's `.text` section will result in a kernel panic.

```Bash
user@krx:~/krx$ grep "T prepare_creds" /proc/kallsyms | cut -d' ' -f1
ffffffffc167d874
user@krx:~/krx$ echo -n "0xffffffffc167d874" > /sys/kernel/debug/kernread/data_ptr
user@krx:~/krx$ xxd -p -l8 /sys/kernel/debug/kernread/data_ptr
# kernel panic
```
<div align="center">

##### Code Block 3. kR^X blocking an attempt to read the kernel's `.text` section.

</div>

This test will work for both 32 bit and 64 bit machines across all three protection schemes (`kR^X-SFI`, `kR^X-MPX`, and `kR^X-SEG`).