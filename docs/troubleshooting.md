<div align="center">

# Troubleshooting

#### Tips and Tricks for Installing and Working with the kR^X Kernel

</div>

<details>
<summary>Package Repositories</summary>

While Debian v7 ("wheezy") is no longer supported by the Debian Project, package repositories can still be found in the Debian archive. To access them, add the following line to `/etc/apt/sources.list` and then run `apt-get update`.

```
deb http://archive.debian.org/debian wheezy main non-free contrib
```

</details>

<details>
<summary>Space Requirements</summary>

If kernel compilation is failing with messages that look like:

```
ld: final link failed: No space left on device
```

or

```
write error: No space left on device
```

the machine most likely doesn't have enough disk space to compile the kernel. For the minimal install (`*.min` configuration file) we found that 20GB was sufficient and for the regular install (`*.deb` configuration file) we found that 25GB of disk space was sufficient.

</details>

<details>
<summary>Invalid <code>CONFIG_KRX_EDATA</code></summary>

When running `make`, the compilation may halt with the following error that tells the user the default end-of-data won't work.

```
kR^X: The value of CONFIG_KRX_EDATA is invalid (0xffffffffc1523000).
      Set CONFIG_KRX_EDATA to 0xffffffffc13e1fff and run 'make' again.
make: *** [all] Error 1
```
<div align="center">

##### Code Block 1. Invalid `CONFIG_KRX_EDATA` error message.

</div>

To fix this, simply go into the `.config` file, change `CONFIG_KRX_EDATA` to be the recommended `0xffffffffc13e1fff` from `0xffffffffc1523000` and then run `make` again.

</details>

<details>
<summary>Enabling MPX</summary>

In the event that MPX support isn't passed through from the host to the guest VMWare virtual machine, add `cpuid.enableMPX = "TRUE"` to the VM's `.vmx` file.

</details>

<details>
<summary>Full Package Listing</summary>

This is a listing of all the packages on the amd64 Debian v7 system after custom installation and installation of several packages required for building the kR^X kernel. In total, there were 229 packages present before kR^X compilation began.

<div align="center">

| Name                               | Version                          | Description                                                          |
| :--------------------------------- | :------------------------------- | :------------------------------------------------------------------- |
| acpi                               | 1.6-1                            | displays information on ACPI devices
| acpi-support-base                  | 0.140-5+deb7u3                   | scripts for handling base ACPI events such as the power button
| acpid                              | 1:2.0.16-1+deb7u1                | Advanced Configuration and Power Interface event daemon
| adduser                            | 3.113+nmu3                       | add and remove users and groups
| apt                                | 0.9.7.9+deb7u7                   | commandline package manager
| apt-utils                          | 0.9.7.9+deb7u7                   | package managment related utility programs
| aptitude                           | 0.6.8.2-1                        | terminal-based package manager
| aptitude-common                    | 0.6.8.2-1                        | architecture indepedent files for the aptitude package manager
| base-files                         | 7.1wheezy11                      | Debian base system miscellaneous files
| base-passwd                        | 3.5.26                           | Debian base system master password and group files
| bash                               | 4.2+dfsg-0.1+deb7u3              | GNU Bourne Again SHell
| bc                                 | 1.06.95-2+b1                     | The GNU bc arbitrary precision calculator language
| binutils                           | 2.25                             | GNU assembler, linker and binary utilities
| bison                              | 1:2.5.dfsg-2.1                   | YACC-compatible parser generator
| bsdmainutils                       | 9.0.3                            | collection of more utilities from FreeBSD
| bsdutils                           | 1:2.20.1-5.3                     | Basic utilities from 4.4BSD-Lite
| busybox                            | 1:1.20.0-7                       | Tiny utilities for small and embedded systems
| console-setup                      | 1.88                             | console font and keymap setup program
| console-setup-linux                | 1.88                             | Linux specific part of console-setup
| coreutils                          | 8.13-3.5                         | GNU core utilities
| cpio                               | 2.11+dfsg-0.1+deb7u2             | GNU cpio -- a program to manage archives of files
| cpp                                | 4:4.7.2-1                        | GNU C preprocessor (cpp)
| cpp-4.7                            | 4.7.2-5                          | GNU C preprocessor
| cron                               | 3.0pl1-124                       | process scheduling daemon
| dash                               | 0.5.7-3                          | POSIX-compliant shell
| debconf                            | 1.5.49                           | Debian configuration management system
| debconf-i18n                       | 1.5.49                           | full internationalization support for debconf
| debian-archive-keyring             | 2014.3~deb7u1                    | GnuPG archive keys of the Debian archive
| debianutils                        | 4.3.2                            | Miscellaneous utilities specific to Debian
| diffutils                          | 1:3.2-6                          | File comparison utilities
| discover                           | 2.1.2-5.2                        | hardware identification system
| discover-data                      | 2.2010.10.18                     | Data lists for Discover hardware detection system
| dmidecode                          | 2.11-9                           | SMBIOS/DMI table decoder
| dmsetup                            | 2:1.02.74-8                      | Linux Kernel Device Mapper userspace library
| dpkg                               | 1.16.18                          | Debian package management system
| e2fslibs:amd64                     | 1.42.5-1.1+deb7u1                | ext2/ext3/ext4 file system libraries
| e2fsprogs                          | 1.42.5-1.1+deb7u1                | ext2/ext3/ext4 file system utilities
| eject                              | 2.1.5+deb1+cvs20081104-13        | ejects CDs and operates CD-Changers under Linux
| findutils                          | 4.4.2-4                          | utilities for finding files--find, xargs
| flex                               | 2.5.35-10.1                      | A fast lexical analyzer generator.
| g++                                | 4:4.7.2-1                        | GNU C++ compiler
| g++-4.7                            | 4.7.2-5                          | GNU C++ compiler
| gawk                               | 1:4.0.1+dfsg-2.1                 | GNU awk, a pattern scanning and processing language
| gcc                                | 4:4.7.2-1                        | GNU C compiler
| gcc-4.7                            | 4.7.2-5                          | GNU C compiler
| gcc-4.7-base:amd64                 | 4.7.2-5                          | GCC, the GNU Compiler Collection (base package)
| gcc-4.7-plugin-dev                 | 4.7.2-5                          | Files for GNU GCC plugin development.
| gettext-base                       | 0.18.1.1-9                       | GNU Internationalization utilities for the base system
| gnupg                              | 1.4.12-7+deb7u7                  | GNU privacy guard - a free PGP replacement
| gpgv                               | 1.4.12-7+deb7u7                  | GNU privacy guard - signature verification tool
| grep                               | 2.12-2                           | GNU grep, egrep and fgrep
| groff-base                         | 1.21-9                           | GNU troff text-formatting system (base system components)
| grub-common                        | 1.99-27+deb7u3                   | GRand Unified Bootloader (common files)
| grub-pc                            | 1.99-27+deb7u3                   | GRand Unified Bootloader, version 2 (PC/BIOS version)
| grub-pc-bin                        | 1.99-27+deb7u3                   | GRand Unified Bootloader, version 2 (PC/BIOS binaries)
| grub2-common                       | 1.99-27+deb7u3                   | GRand Unified Bootloader (common files for version 2)
| gzip                               | 1.5-1.1                          | GNU compression utilities
| hostname                           | 3.11                             | utility to set/show the host name or domain name
| ifupdown                           | 0.7.8                            | high level tools to configure network interfaces
| info                               | 4.13a.dfsg.1-10                  | Standalone GNU Info documentation browser
| initramfs-tools                    | 0.109.1                          | generic modular initramfs generator
| initscripts                        | 2.88dsf-41+deb7u1                | scripts for initializing and shutting down the system
| insserv                            | 1.14.0-5                         | boot sequence organizer using LSB init.d script dependency information
| install-info                       | 4.13a.dfsg.1-10                  | Manage installed documentation in info format
| installation-report                | 2.49                             | system installation report
| iproute                            | 20120521-3+b3                    | networking and traffic control tools
| iptables                           | 1.4.14-3.1                       | administration tools for packet filtering and NAT
| iputils-ping                       | 3:20101006-1+b1                  | Tools to test the reachability of network hosts
| isc-dhcp-client                    | 4.2.2.dfsg.1-5+deb70u8           | ISC DHCP client
| isc-dhcp-common                    | 4.2.2.dfsg.1-5+deb70u8           | common files used by all the isc-dhcp* packages
| kbd                                | 1.15.3-9                         | Linux console font and keytable utilities
| keyboard-configuration             | 1.88                             | system-wide keyboard preferences
| klibc-utils                        | 2.0.1-3.1                        | small utilities built with klibc for early boot
| kmod                               | 9-3                              | tools for managing Linux kernel modules
| krb5-locales                       | 1.10.1+dfsg-5+deb7u7             | Internationalization support for MIT Kerberos
| laptop-detect                      | 0.13.7                           | attempt to detect a laptop
| libacl1:amd64                      | 2.2.51-8                         | Access control list shared library
| libapt-inst1.5:amd64               | 0.9.7.9+deb7u7                   | deb package format runtime library
| libapt-pkg4.12:amd64               | 0.9.7.9+deb7u7                   | package managment runtime library
| libasprintf0c2:amd64               | 0.18.1.1-9                       | GNU library to use fprintf and friends in C++
| libattr1:amd64                     | 1:2.4.46-8                       | Extended attribute shared library
| libbison-dev:amd64                 | 1:2.5.dfsg-2.1                   | YACC-compatible parser generator - development library
| libblkid1:amd64                    | 2.20.1-5.3                       | block device id library
| libboost-iostreams1.49.0           | 1.49.0-3.2                       | Boost.Iostreams Library
| libbsd-dev                         | 0.4.2-1                          | utility functions from BSD systems - development files
| libbsd0:amd64                      | 0.4.2-1                          | utility functions from BSD systems - shared library
| libbz2-1.0:amd64                   | 1.0.6-4                          | high-quality block-sorting file compressor library - runtime
| libc-bin                           | 2.13-38+deb7u10                  | Embedded GNU C Library: Binaries
| libc-dev-bin                       | 2.13-38+deb7u10                  | Embedded GNU C Library: Development binaries
| libc6:amd64                        | 2.13-38+deb7u10                  | Embedded GNU C Library: Shared libraries
| libc6-dev:amd64                    | 2.13-38+deb7u10                  | Embedded GNU C Library: Development Libraries and Header Files
| libcomerr2:amd64                   | 1.42.5-1.1+deb7u1                | common error description library
| libcwidget3                        | 0.5.16-3.4                       | high-level terminal interface library for C++ (runtime files)
| libdb5.1:amd64                     | 5.1.29-5                         | Berkeley v5.1 Database Libraries [runtime]
| libdevmapper1.02.1:amd64           | 2:1.02.74-8                      | Linux Kernel Device Mapper userspace library
| libdiscover2                       | 2.1.2-5.2                        | hardware identification library
| libedit2:amd64                     | 2.11-20080614-5                  | BSD editline and history libraries
| libept1.4.12                       | 1.0.9                            | High-level library for managing Debian package information
| libexpat1:amd64                    | 2.1.0-1+deb7u2                   | XML parsing C library - runtime library
| libfreetype6:amd64                 | 2.4.9-1.1+deb7u3                 | FreeType 2 font engine, shared library files
| libfuse2:amd64                     | 2.9.0-2+deb7u2                   | Filesystem in Userspace (library)
| libgcc1:amd64                      | 1:4.7.2-5                        | GCC support library
| libgcrypt11:amd64                  | 1.5.0-5+deb7u4                   | LGPL Crypto library - runtime library
| libgdbm3:amd64                     | 1.8.3-11                         | GNU dbm database routines (runtime version)
| libgmp-dev:amd64                   | 2:5.0.5+dfsg-2                   | Multiprecision arithmetic library developers tools
| libgmp10:amd64                     | 2:5.0.5+dfsg-2                   | Multiprecision arithmetic library
| libgmpxx4ldbl:amd64                | 2:5.0.5+dfsg-2                   | Multiprecision arithmetic library (C++ bindings)
| libgnutls26:amd64                  | 2.12.20-8+deb7u5                 | GNU TLS library - runtime library
| libgomp1:amd64                     | 4.7.2-5                          | GCC OpenMP (GOMP) support library
| libgpg-error0:amd64                | 1.10-3.1                         | library for common error values and messages in GnuPG components
| libgssapi-krb5-2:amd64             | 1.10.1+dfsg-5+deb7u7             | MIT Kerberos runtime libraries - krb5 GSS-API Mechanism
| libidn11:amd64                     | 1.25-2                           | GNU Libidn library, implementation of IETF IDN specifications
| libitm1:amd64                      | 4.7.2-5                          | GNU Transactional Memory Library
| libk5crypto3:amd64                 | 1.10.1+dfsg-5+deb7u7             | MIT Kerberos runtime libraries - Crypto Library
| libkeyutils1:amd64                 | 1.5.5-3+deb7u1                   | Linux Key Management Utilities (library)
| libklibc                           | 2.0.1-3.1                        | minimal libc subset for use with initramfs
| libkmod2:amd64                     | 9-3                              | libkmod shared library
| libkrb5-3:amd64                    | 1.10.1+dfsg-5+deb7u7             | MIT Kerberos runtime libraries
| libkrb5support0:amd64              | 1.10.1+dfsg-5+deb7u7             | MIT Kerberos runtime libraries - Support library
| liblocale-gettext-perl             | 1.05-7+b1                        | module using libc functions for internationalization in Perl
| liblzma5:amd64                     | 5.1.1alpha+20120614-2            | XZ-format compression library
| libmount1                          | 2.20.1-5.3                       | block device id library
| libmpc2:amd64                      | 0.9-4                            | multiple precision complex floating-point library
| libmpfr4:amd64                     | 3.1.0-5                          | multiple precision floating-point computation
| libncurses5:amd64                  | 5.9-10                           | shared libraries for terminal handling
| libncursesw5:amd64                 | 5.9-10                           | shared libraries for terminal handling (wide character support)
| libnewt0.52                        | 0.52.14-11.1                     | Not Erik's Windowing Toolkit - text mode windowing with slang
| libnfnetlink0                      | 1.0.0-1.1                        | Netfilter netlink library
| libp11-kit0:amd64                  | 0.12-3                           | Library for loading and coordinating access to PKCS#11 modules - runtime
| libpam-modules:amd64               | 1.1.3-7.1                        | Pluggable Authentication Modules for PAM
| libpam-modules-bin                 | 1.1.3-7.1                        | Pluggable Authentication Modules for PAM - helper binaries
| libpam-runtime                     | 1.1.3-7.1                        | Runtime support for the PAM library
| libpam0g:amd64                     | 1.1.3-7.1                        | Pluggable Authentication Modules library
| libpci3:amd64                      | 1:3.1.9-6                        | Linux PCI Utilities (shared library)
| libpipeline1:amd64                 | 1.2.1-1                          | pipeline manipulation library
| libpopt0:amd64                     | 1.16-7                           | lib for parsing cmdline parameters
| libprocps0:amd64                   | 1:3.3.3-3                        | library for accessing process information from /proc
| libquadmath0:amd64                 | 4.7.2-5                          | GCC Quad-Precision Math Library
| libreadline6:amd64                 | 6.2+dfsg-0.1                     | GNU readline and history libraries, run-time libraries
| libselinux1:amd64                  | 2.1.9-5                          | SELinux runtime shared libraries
| libsemanage-common                 | 2.1.6-6                          | Common files for SELinux policy management libraries
| libsemanage1:amd64                 | 2.1.6-6                          | SELinux policy management library
| libsepol1:amd64                    | 2.1.4-3                          | SELinux library for manipulating binary security policies
| libsigc++-2.0-0c2a:amd64           | 2.2.10-0.2                       | type-safe Signal Framework for C++ - runtime
| libsigsegv2                        | 2.9-4                            | Library for handling page faults in a portable way
| libslang2:amd64                    | 2.2.4-15                         | S-Lang programming library - runtime version
| libsqlite3-0:amd64                 | 3.7.13-1+deb7u2                  | SQLite 3 shared library
| libss2:amd64                       | 1.42.5-1.1+deb7u1                | command-line interface parsing library
| libssl1.0.0:amd64                  | 1.0.1e-2+deb7u20                 | SSL shared libraries
| libstdc++6:amd64                   | 4.7.2-5                          | GNU Standard C++ Library v3
| libstdc++6-4.7-dev                 | 4.7.2-5                          | GNU Standard C++ Library v3 (development files)
| libtasn1-3:amd64                   | 2.13-2+deb7u2                    | Manage ASN.1 structures (runtime)
| libtext-charwidth-perl             | 0.04-7+b1                        | get display widths of characters on the terminal
| libtext-iconv-perl                 | 1.7-5                            | converts between character sets in Perl
| libtext-wrapi18n-perl              | 0.06-7                           | internationalized substitute of Text::Wrap
| libtinfo5:amd64                    | 5.9-10                           | shared low-level terminfo library for terminal handling
| libudev0:amd64                     | 175-7.2                          | libudev shared library
| libusb-0.1-4:amd64                 | 2:0.1.12-20+nmu1                 | userspace USB programming library
| libustr-1.0-1:amd64                | 1.0.4-3                          | Micro string library: shared library
| libuuid-perl                       | 0.02-5                           | Perl extension for using UUID interfaces as defined in e2fsprogs
| libuuid1:amd64                     | 2.20.1-5.3                       | Universally Unique ID library
| libwrap0:amd64                     | 7.6.q-24                         | Wietse Venema's TCP wrappers library
| libx11-6:amd64                     | 2:1.5.0-1+deb7u2                 | X11 client-side library
| libx11-data                        | 2:1.5.0-1+deb7u2                 | X11 client-side library
| libxapian22                        | 1.2.12-2+deb7u1                  | Search engine library
| libxau6:amd64                      | 1:1.0.7-1                        | X11 authorisation library
| libxcb1:amd64                      | 1.8.1-2+deb7u1                   | X C Binding
| libxdmcp6:amd64                    | 1:1.1.1-1                        | X11 Display Manager Control Protocol library
| libxext6:amd64                     | 2:1.3.1-2+deb7u1                 | X11 miscellaneous extension library
| libxmuu1:amd64                     | 2:1.1.1-1                        | X11 miscellaneous micro-utility library
| linux-base                         | 3.5                              | Linux image base package
| linux-image-3.2.0-4-amd64          | 3.2.78-1                         | Linux 3.2 for 64-bit PCs
| linux-image-amd64                  | 3.2+46                           | Linux for 64-bit PCs (meta-package)
| linux-libc-dev:amd64               | 3.2.78-1                         | Linux support headers for userspace development
| locales                            | 2.13-38+deb7u10                  | Embedded GNU C Library: National Language (locale) data [support]
| login                              | 1:4.1.5.1-1                      | system login tools
| logrotate                          | 3.8.1-4                          | Log rotation utility
| lsb-base                           | 4.1+Debian8+deb7u1               | Linux Standard Base 4.1 init script functionality
| m4                                 | 1.4.16-3                         | a macro processing language
| make                               | 3.81-8.2                         | An utility for Directing compilation.
| man-db                             | 2.6.2-1                          | on-line manual pager
| manpages                           | 3.44-1                           | Manual pages about using a GNU/Linux system
| manpages-dev                       | 3.44-1                           | Manual pages about using GNU/Linux for development
| mawk                               | 1.3.3-17                         | a pattern scanning and text processing language
| module-init-tools                  | 9-3                              | transitional dummy package (module-init-tools to kmod)
| mount                              | 2.20.1-5.3                       | Tools for mounting and manipulating filesystems
| multiarch-support                  | 2.13-38+deb7u10                  | Transitional package to ensure multiarch compatibility
| nano                               | 2.2.6-1+b1                       | small, friendly text editor inspired by Pico
| ncurses-base                       | 5.9-10                           | basic terminal type definitions
| ncurses-bin                        | 5.9-10                           | terminal-related programs and man pages
| ncurses-term                       | 5.9-10                           | additional terminal type definitions
| net-tools                          | 1.60-24.2                        | The NET-3 networking toolkit
| netbase                            | 5.0                              | Basic TCP/IP networking system
| netcat-traditional                 | 1.10-40                          | TCP/IP swiss army knife
| openssh-blacklist                  | 0.4.1+nmu1                       | list of default blacklisted OpenSSH RSA and DSA keys
| openssh-blacklist-extra            | 0.4.1+nmu1                       | list of non-default blacklisted OpenSSH RSA and DSA keys
| openssh-client                     | 1:6.0p1-4+deb7u4                 | secure shell (SSH) client, for secure access to remote machines
| openssh-server                     | 1:6.0p1-4+deb7u4                 | secure shell (SSH) server, for secure access from remote machines
| os-prober                          | 1.58                             | utility to detect other OSes on a set of drives
| passwd                             | 1:4.1.5.1-1                      | change and administer password and group data
| pciutils                           | 1:3.1.9-6                        | Linux PCI Utilities
| perl-base                          | 5.14.2-21+deb7u3                 | minimal Perl system
| procps                             | 1:3.3.3-3                        | /proc file system utilities
| readline-common                    | 6.2+dfsg-0.1                     | GNU readline and history libraries, common files
| rsyslog                            | 5.8.11-3+deb7u2                  | reliable system and kernel logging daemon
| sed                                | 4.2.1-10                         | The GNU sed stream editor
| sensible-utils                     | 0.0.7                            | Utilities for sensible alternative selection
| sudo                               | 1.8.5p2-1+nmu3+deb7u1            | Provide limited super user privileges to specific users
| sysv-rc                            | 2.88dsf-41+deb7u1                | System-V-like runlevel change mechanism
| sysvinit                           | 2.88dsf-41+deb7u1                | System-V-like init utilities
| sysvinit-utils                     | 2.88dsf-41+deb7u1                | System-V-like utilities
| tar                                | 1.26+dfsg-0.1                    | GNU version of the tar archiving utility
| tasksel                            | 3.14.1                           | Tool for selecting tasks for installation on Debian systems
| tasksel-data                       | 3.14.1                           | Official tasks used for installation of Debian systems
| tcpd                               | 7.6.q-24                         | Wietse Venema's TCP wrapper utilities
| texinfo                            | 4.13a.dfsg.1-10                  | Documentation system for on-line information and printed output
| traceroute                         | 1:2.0.18-3                       | Traces the route taken by packets over an IPv4/IPv6 network
| tzdata                             | 2016d-0+deb7u1                   | time zone and daylight-saving time data
| ucf                                | 3.0025+nmu3                      | Update Configuration File: preserve user changes to config files.
| udev                               | 175-7.2                          | /dev/ and hotplug management daemon
| util-linux                         | 2.20.1-5.3                       | Miscellaneous system utilities
| vim-common                         | 2:7.3.547-7                      | Vi IMproved - Common files
| vim-tiny                           | 2:7.3.547-7                      | Vi IMproved - enhanced vi editor - compact version
| wget                               | 1.13.4-3+deb7u2                  | retrieves files from the web
| whiptail                           | 0.52.14-11.1                     | Displays user-friendly dialog boxes from shell scripts
| xauth                              | 1:1.0.7-1                        | X authentication utility
| xkb-data                           | 2.5.1-3                          | X Keyboard Extension (XKB) configuration data
| xz-utils                           | 5.1.1alpha+20120614-2            | XZ-format compression utilities
| zlib1g:amd64                       | 1:1.2.7.dfsg-13                  | compression library - runtime
##### Table 1. Packages installed on an amd64 Debian v7 machine before installing kR^X.

</div>

</details>